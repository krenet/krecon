<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: X-PINGOTHER, Content-Type, enctype");
header('content-type: application/json; charset=utf-8');

include '../inc/db_setting.inc';    

$function = $_REQUEST['function'];

if($function=="contents"){

    $query = sprintf("select * from krecon_adviser where company_id = 1 order by id");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}elseif($function=="edit"){

    $query = sprintf("update krecon_adviser set name = '%s', affiliation='%s',
                                                phone ='%s', mail='%s', etc='%s',birthday ='%s'
                    where id= '%s'",$_REQUEST['name'],$_REQUEST['affiliation'] ,
                    $_REQUEST['phone'],$_REQUEST['mail'],$_REQUEST['memo'],$_REQUEST['birth'],
                    $_REQUEST['id']);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}elseif($function=="backup"){
    $id = $_REQUEST['id'];
    $query = sprintf("insert into 
                        krecon_adviser_backup (name, affiliation, part, position, field, phone, mail,company_id,birthday)
                        select name, affiliation,part,position, field, phone, mail,company_id,birthday
                        from krecon_adviser
                        where id ='%s'",$id);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}elseif($function=="delete"){
    $id = $_REQUEST['id'];

    $query = sprintf("delete from krecon_adviser where id ='%s'",$id);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}elseif($function=="addAdviser"){
    $name = $_REQUEST['name'];
    $affiliation = $_REQUEST['affiliation'];
    $field = $_REQUEST['field'];
    $phone = $_REQUEST['phone'];
    $mail = $_REQUEST['mail'];
    $query = sprintf("insert into 
                        krecon_adviser 
                        (name, affiliation,field,phone,mail)
                        values('%s','%s','%s','%s','%s')",$name,$affiliation,$field,$phone,$mail);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}elseif($function=="selectAdviserByCompanyId"){

    $query = sprintf("select * from krecon_adviser where company_id =(select company_id from krecon_construction where con_code = '%s')",$_REQUEST['id']);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}elseif($function=="selectAdviserAll"){

    $query = sprintf("select * from krecon_adviser group by name");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}elseif( $function=="getAffiliation"){
    $query = sprintf("select affiliation from krecon_adviser group by affiliation");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}elseif( $function=="getAdvById"){
    $query = sprintf("select * from krecon_adviser where id = '%s'",$_REQUEST['id']);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}elseif( $function=="birthList"){
    $query = sprintf("
    (SELECT company_id,
                name,
                affiliation,
                birthday,
                phone,
                mail
                 FROM krecon_adviser where company_id =1)
    
    UNION ALL
    
    (SELECT company_id,
                name,
                affiliation,
                birthday,
                phone,
                mail FROM krecon_off where company_id = 1)");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}


mysql_close($connect);

?>