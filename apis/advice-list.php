<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: X-PINGOTHER, Content-Type, enctype");
header('content-type: application/json; charset=utf-8');

include '../inc/db_setting.inc';    

$function = $_REQUEST['function'];

if($function=="contents"){

    $query = sprintf("select id, 
                            con_code, 
                            title, 
                            deadline, 
                            progress, 
                            group_concat(name) as name
                        from (
                                SELECT A.*, C.name as name
                                FROM krecon_advice A,
                                        krecon_advice_type B,
                                        krecon_adviser C 
                                WHERE B.adviser_id=C.id AND A.id=B.advice_id 
                                ) as a
                        group by id
                        order by id desc");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}


mysql_close($connect);

?>
