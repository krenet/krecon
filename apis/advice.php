<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: X-PINGOTHER, Content-Type, enctype");
header('content-type: application/json; charset=utf-8');

include '../inc/db_setting.inc';    

$function = $_REQUEST['function'];

if($function=="contents"){
    $id = $_REQUEST['id'];

    $query = sprintf("select id, 
                            con_code, 
                            title, 
                            deadline, 
                            progress, 
                            group_concat(name) as name
                        from (
                                SELECT A.*, C.name as name
                                FROM krecon_advice A,
                                        krecon_advice_type B,
                                        krecon_adviser C 
                                WHERE B.adviser_id=C.id AND A.id=B.advice_id
                                ) as a
                        group by id
                        order by id desc");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}else if($function=="contentsLimitSix"){
    $id = $_REQUEST['id'];

    $query = sprintf("select id, 
    con_code, 
    title, 
    deadline, 
    progress, 
    con_area_name,
    group_concat(name) as name
from (
        SELECT A.*, 
		  D.con_code as cc,
		  D.con_area_name,
		  C.name as name
        FROM krecon_advice A,
                krecon_advice_type B,
                krecon_adviser C ,
                krecon_construction D
        WHERE B.adviser_id=C.id AND A.id=B.advice_id and A.con_code = D.con_code
        ) as a
group by id
order by id desc Limit 6");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}else if($function=="remote"){

    // $query = sprintf("select kab.* , group_concat(name) as name
    //                   from krecon_advice_type kat, krecon_advice_board kab,
    //                        krecon_adviser ka
    //                   where kab.advice_id = kat.advice_id and kat.adviser_id = ka.id
    //                   group by kab.id desc"
    //                 );
    $id = $_REQUEST['id'];
                    
    // $query = sprintf("select *  from krecon_advice 
    //                     where con_code = '%s' 
    //                     group by id desc ",$id);
    $query = sprintf("select *  from krecon_advice order by id desc");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);


    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
    
}else if($function=="remoteById"){

    $id = $_REQUEST['id'];
                    
    $query = sprintf("select *  from krecon_advice 
                        where con_code = '%s' 
                        group by id desc ",$id);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);


    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
    
}else if($function=="selectConRemoteById"){

    $id = $_REQUEST['id'];
                    
    $query = sprintf("select *  from krecon_construction 
                        where con_code = '%s' ",$id);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);


    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
    
}else if($function=="selectConRemoteByCompanyId"){

    $id = $_REQUEST['id'];
                    
    $query = sprintf("select *  from krecon_construction ");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);


    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
    
}
else if($function=="remoteBoard"){
    $id = $_REQUEST['id'];

    $query = sprintf("select kab.* , group_concat(name) as name
    from krecon_advice_type kat, krecon_advice_board kab,
         krecon_adviser ka
    where kab.advice_id = kat.advice_id and kat.adviser_id = ka.id and kab.advice_id = '%s'
    group by kab.id",$id);
                    
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);


    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}else if($function=="addRemoteAdvice"){
    $con_area_name = $_REQUEST['con_area_name'];
    $title = $_REQUEST['title'];
    $deadline = $_REQUEST['doc_deadline'];
    $date = $_REQUEST['date'];
    $progress = $_REQUEST['progress'];

    $query = sprintf("insert into krecon_advice (con_code, title, deadline,progress,scheduled_open_time,regdate)
                      values((select con_code 
                                from krecon_construction 
                                where con_area_name = '%s'),'%s','%s','%s','%s',now())",
                                $con_area_name,$title,$deadline,$progress,$date);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $insert_id=mysql_insert_id();


    $output = array("result"=>"ok","contents"=>$contents,"insert"=>$insert_id);
    echo json_encode($output);

}else if($function=="addRemoteAdviserToAdvise"){
    $adviser_id = $_REQUEST['adviser_id'];
    $advice_id = $_REQUEST['advice_id'];

    $query = sprintf("insert into krecon_advice_type (advice_id,adviser_id,regdate)
                      values('%s','%s',now())",$advice_id,$adviser_id);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}else if($function=="addAdvicetoAdviceBoard"){
    $advice_id = $_REQUEST['advice_id'];
    $on_offline = $_REQUEST['on_offline'];
    $doc_deadline = $_REQUEST['doc_deadline'];
    $scheduled_open_time = $_REQUEST['scheduled_open_time'];
    $title = $_REQUEST['title'];
    $query = sprintf("insert into krecon_advice_board (advice_id,on_offline,scheduled_open_time,doc_deadline,title,regdate)
                      values('%s','%s','%s','%s',now())",$advice_id,$on_offline,$doc_deadline,$title);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}else if($function=="addScheduleAdvModal"){
    $advice_id = $_REQUEST['advice_id'];
    $on_offline = $_REQUEST['on_offline'];
    $title = $_REQUEST['title'];
    $doc_deadline = $_REQUEST['doc_deadline'];
    $query = sprintf("insert into krecon_advice_board 
                    (advice_id,on_offline,doc_deadline,regdate,title)
                      values('%s','%s','%s',now(),'%s')",
                      $advice_id,$on_offline,$doc_deadline,$title);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function=="getMailLink"){
    $query = sprintf("select mail,name from krecon_adviser where id = any(
        select adviser_id 
        from (select * from krecon_advice_type where advice_id = '%s')as a)",
                      $_REQUEST['id']);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function=="adviceBackUp"){
    $query = sprintf("insert into 
             krecon_advice_backup (con_code, 
                          title, 
                          regdate, 
                          scheduled_open_time, 
                          scheduled_close_time, 
                          deadline,
                          progress,delete_date)
            select con_code, 
                    title, 
                    regdate, 
                    scheduled_open_time, 
                    scheduled_close_time, 
                    deadline,
                    progress, now()
            from krecon_advice
            where id ='%s'", $_REQUEST['id']);
    $result = mysql_query($query);

    $query = sprintf("insert into 
    krecon_advice_board_backup(advice_id,
                                on_offline,
                                doc_deadline,
                                etc,
                                advice_date,
                                title,
                                session_status,
                                scheduled_open_time,
                                close_date,
                                scheduled_close_time,
                                classification,
                                regdate,backup_date)
    select advice_id, on_offline, doc_deadline,
    etc, advice_date,title, session_status,
    scheduled_open_time, close_date, scheduled_close_time,
    classification, regdate,now() from krecon_advice_board
    where advice_id = '%s'",$_REQUEST['id']);
    $result = mysql_query($query);

    $query = sprintf("insert into 
    krecon_advice_board_detail_backup(board_id,
                                    classification,
                                    title,
                                    date,
                                    file_path,
                                    backup_date)
    select board_id, classification,title,
    date, file_path, now()
    from krecon_advice_board_detail
    where board_id = (select id from krecon_advice_board 
    where advice_id = '%s')",$_REQUEST['id']);
  $result = mysql_query($query);
  $contents = array();
  while($row = mysql_fetch_assoc($result)){
      $contents[] = $row;
  }
  mysql_free_result($result);

  $output = array("result"=>"ok","contents"=>$contents);
  echo json_encode($output);
}else if($function=="deleteAdvice"){
    $query = sprintf("delete from krecon_advice_board_detail where board_id =(select id 
    from krecon_advice_board where advice_id = '%s')",$_REQUEST['id']);
    $result = mysql_query($query);
    
    $query = sprintf("delete from krecon_advice_board where advice_id ='%s'",
                      $_REQUEST['id']);
    $result = mysql_query($query);

    $query = sprintf("delete from krecon_advice where id ='%s'",
                      $_REQUEST['id']);
    $result = mysql_query($query);
}else if($function=="clickDeleteBoardInModal"){
    $query = sprintf("insert into 
    krecon_advice_board_backup(advice_id,
                                on_offline,
                                doc_deadline,
                                etc,
                                advice_date,
                                title,
                                session_status,
                                scheduled_open_time,
                                close_date,
                                scheduled_close_time,
                                classification,
                                regdate,backup_date)
    select advice_id, on_offline, doc_deadline,
    etc, advice_date,title, session_status,
    scheduled_open_time, close_date, scheduled_close_time,
    classification, regdate,now() from krecon_advice_board
    where advice_id = '%s'",$_REQUEST['id']);
    $result = mysql_query($query);

    $query = sprintf("insert into 
    krecon_advice_board_detail_backup(board_id,
                                    classification,
                                    title,
                                    date,
                                    file_path,
                                    backup_date)
    select board_id, classification,title,
    date, file_path, now()
    from krecon_advice_board_detail
    where board_id = (select id from krecon_advice_board 
    where advice_id = '%s')",$_REQUEST['id']);
  $result = mysql_query($query);
  $contents = array();
  while($row = mysql_fetch_assoc($result)){
      $contents[] = $row;
  }
  mysql_free_result($result);

  $output = array("result"=>"ok","contents"=>$contents);
  echo json_encode($output);
}else if($function=="clickDeleteBoardInModalInCommunity"){
    $query = sprintf("insert into 
    krecon_advice_board_backup(advice_id,
                                on_offline,
                                doc_deadline,
                                etc,
                                advice_date,
                                title,
                                session_status,
                                scheduled_open_time,
                                close_date,
                                scheduled_close_time,
                                classification,
                                regdate,backup_date)
    select advice_id, on_offline, doc_deadline,
    etc, advice_date,title, session_status,
    scheduled_open_time, close_date, scheduled_close_time,
    classification, regdate,now() from krecon_advice_board
    where id = '%s'",$_REQUEST['id']);
    $result = mysql_query($query);

    $query = sprintf("insert into 
    krecon_advice_board_detail_backup(board_id,
                                    classification,
                                    title,
                                    date,
                                    file_path,
                                    backup_date)
    select board_id, classification,title,
    date, file_path, now()
    from krecon_advice_board_detail
    where board_id ='%s'",$_REQUEST['id']);
  $result = mysql_query($query);
  $contents = array();
  while($row = mysql_fetch_assoc($result)){
      $contents[] = $row;
  }
  mysql_free_result($result);

  $output = array("result"=>"ok","contents"=>$contents);
  echo json_encode($output);
}else if($function=="deleteAdviceInModal"){
    $query = sprintf("delete from krecon_advice_board_detail where board_id ='%s'",$_REQUEST['id']);
    $result = mysql_query($query);
    
    $query = sprintf("delete from krecon_advice_board where id ='%s'",
                      $_REQUEST['id']);
    $result = mysql_query($query);

}


mysql_close($connect);

?>

