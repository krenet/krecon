<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: X-PINGOTHER, Content-Type, enctype");
header('content-type: application/json; charset=utf-8');

include '../inc/util.php';
include '../inc/db_setting.inc';    

$function = $_REQUEST['function'];

if($function=="specifications"){
    $query = sprintf("select * from krecon_document where doc_category = '시공'");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function=="completions"){
    $query = sprintf("select * from krecon_document where doc_category = '준공'");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function=="laws"){
    $query = sprintf("select * from krecon_law");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function=="lawsByYear"){
    $year = $_REQUEST['year'];

    $query = sprintf("select * from krecon_law where year = '%s'",$year);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
    
}else if($function=="addDocuments"){
    $file = $_FILES['upfile'];
    $query = sprintf("insert into krecon_document (doc_category,doc_title, 
                        doc_upload_date, doc_file_name,con_code)
                        values('%s','%s',now(),'%s','%s')",
                        $_REQUEST['category'],$_REQUEST['title'],
                        $_REQUEST['fileName'],$_REQUEST['id']);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function=="conList"){
    $query = sprintf("select * from krecon_construction");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function=="getSpecificationListbyConcode"){
    $con_code = $_REQUEST['con_code'];
    $query = sprintf("select * from krecon_document 
                        where doc_category = '시공' 
                        and con_code = '%s'",$con_code);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function=="getCompletionListbyConcode"){
    $con_code = $_REQUEST['con_code'];
    $query = sprintf("select * from krecon_document 
                      where con_code = '%s'
                      and doc_category = '준공' ",$con_code);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
         $contents[] = $row;
    }
    mysql_free_result($result);
                  
    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function=="getConstructionbyConcode"){
    $con_code = $_REQUEST['con_code'];
    $query = sprintf("select * from krecon_construction
                      where con_code = '%s'", $con_code);
    $result = mysql_query($query);
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}

mysql_close($connect);

?>