<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: X-PINGOTHER, Content-Type, enctype");
header('content-type: application/json; charset=utf-8');

include '../inc/db_setting.inc';    

$function = $_REQUEST['function'];

if($function=="contents"){
    // $room = $_REQUEST['room'];

    $query = sprintf("select con_code, con_area_name, con_field_name,
                             con_goal_start_date,
                             con_actuality_end_date,
                             con_field_manager,
                             is_live,
                             con_lat,
                             con_long
                      from krecon_construction");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}else if($function=="forTitle"){
    // $room = $_REQUEST['room'];

    $query = sprintf("select con_code, con_area_name, con_field_name,
                             con_goal_start_date,
                             con_actuality_end_date,
                             con_field_manager,
                             is_live,
                             con_lat,
                             con_long
                      from krecon_construction where con_code ='%s'",$_REQUEST['id']);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}else if($function=="contentIsLive"){
    // $room = $_REQUEST['room'];

    $query = sprintf("select con_code, 
                            con_area_name, con_field_name,
                             con_goal_start_date,
                             con_actuality_end_date,
                             con_field_manager,
                             is_live,
                             con_lat,
                             con_long
                      from krecon_construction
                      where is_live = 'Y'");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}
else if($function=="siteDetail"){
    $id = $_REQUEST['id'];
    $query = sprintf("select klc.*,
                          kc. con_field_name,
                          kc. con_area_name
                     from krecon_live_scene klc,
                          krecon_construction kc
                     where kc.con_code = klc.con_code and klc.con_code ='%s'
                     order by regdate desc limit 1",$id);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}else if($function=="getSiteDetailGps"){
    $id = $_REQUEST['id'];
    $query = sprintf("select con_lat, con_long
                        from krecon_construction where con_code = '%s'",$id);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}else if($function=="equipment"){

    $query = sprintf("insert into 
                                krecon_live_scene (con_code, equipment_id, session_id, level, surface_thermal, 
                                                    equip_location_longitude, equip_location_latitude, 
                                                    surface_temp,regdate)
                        values('%s','%s',(select id from krecon_live_session where cond_code = '%s' and equipment_id = '%s'and status = 'live'),'%s','%s','%s','%s','%s',now());",
                        $_REQUEST['con_code'], 
                        $_REQUEST['equipment_id'],
                        $_REQUEST['con_code'], 
                        $_REQUEST['equipment_id'],
                        $_REQUEST['session_id'],
                        $_REQUEST['level'],
                        $_REQUEST['surface_thermal'],
                        $_REQUEST['equip_location_longitude'],
                        $_REQUEST['equip_location_latitude'],
                        $_REQUEST['surface_temp']);
    $result = mysql_query($query);    

    $output = array("result"=>"ok","inserted"=>$result);
    echo json_encode($output);

}else if($function=="selectSession"){

$query = sprintf("select id from krecon_live_session where con_code ='%s' and equipment_id ='%s' and status = null",$_REQUEST['con_code'], $_REQUEST['equipment_id']);
$result = mysql_query($query);    

$output = array("result"=>"ok","contents"=>$contents);
echo json_encode($output);

}else if($function =="editInspection"){
    
    $query = sprintf("update krecon_field_inspection set %s = '%s'
                        where con_code = '%s'",
                        $_REQUEST['editField'],
                        $_REQUEST['editValue'],
                        $_REQUEST['id']);
    $result = mysql_query($query);    

    $output = array("result"=>"ok","inserted"=>$result);
    echo json_encode($output);
}else if($function =="taskOrder"){
    
    $query = sprintf("select kto.con_code,
                            kto.goal,
                            kto.stage,
                        kto.layer_id,
                        kto.id,
                        ktop.task_order_id,
                        ktop.progress,
                        kl.layer_name
                        from krecon_task_order_process ktop,
                        krecon_task_order kto,
                        krecon_layer kl
                        where kto.con_code = '%s' and kto.layer_id = kl.id
                        and ktop.task_order_id = kto.id
                        group by layer_id",$_REQUEST['id']);

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function =="makeEquipmentBtn"){
    
    $query = sprintf("select kls.id as session_id, 
                            kls.con_code, 
                            kls.equipment_id, 
                            kls.`status`, 
                            kls.start_time, 
                            kls.converting_time,
                            kls.converted_time, 
                            kls.demon_id,
                            ke.type,
                            ke.id
                                FROM krecon_live_session AS kls,
                        krecon_equipment AS ke
                        WHERE con_code = '%s' AND kls.equipment_id = ke.id 
                        order by TYPE, start_time desc",$_REQUEST['id']);

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}
// else if($function =="makeEquipmentBtn"){
    
//     $query = sprintf("select * 
//     from
//             (select ke.type, 
//                     ke.id as b, 
//                     kls.id as c,
//                     kls.equipment_id,
//                     kls.con_code
//             from krecon_equipment ke,
//                  krecon_live_scene kls
//             where kls.equipment_id = ke.id and kls.con_code  = '%s'
//             order by kls.id desc) as a
//     group by equipment_id ",$_REQUEST['id']);

//     $result = mysql_query($query);
//     $contents = array();
//     while($row = mysql_fetch_assoc($result)){
//         $contents[] = $row;
//     }
//     mysql_free_result($result);

//     $output = array("result"=>"ok","contents"=>$contents);
//     echo json_encode($output);
// }
else if($function =="siteDetailDocument"){
    $query = sprintf("select * from krecon_document
                        where con_code = '%s'",$_REQUEST['id']);

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function =="siteDetailDocumentSigong"){
    $query = sprintf("select * from krecon_document
                        where con_code = '%s' and doc_category='시공'",$_REQUEST['id']);

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function =="siteDetailDocumentJungong"){
    $query = sprintf("select * from krecon_document
                        where con_code = '%s' and doc_category='준공'",$_REQUEST['id']);

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function =="getMSXImage"){
    $query = sprintf("select * from krecon_live_scene where `type`='msx' order by id");

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function =="progressAll"){

    $query = sprintf("select ksp.*, kl.layer_name, ksep.real_date_count, ksep.real_date_unit, ksep.real_week_num
from krecon_schedule_process ksp,
		krecon_layer kl,
		krecon_schedule_execution_process ksep
where con_code = '%s' and ksp.layer_id = kl.id and ksep.schedule_id = ksp.id",$_REQUEST['id']);

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function =="selectProgrssWeekMonth"){

    $query = sprintf("select distinct week,month from krecon_schedule_process where con_code='%s'",$_REQUEST['id']);

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function =="selectProgrssWeekMonthInDash"){

    $query = sprintf("select
    kc.con_area_name,
    ksp.id, 
    ksp.con_code, 
    sum(ksp.date_count) as date, 
    sum(ksep.real_date_count) as real_date
        from krecon_schedule_process ksp,
                krecon_layer kl,
                krecon_schedule_execution_process ksep,
                krecon_construction kc
          where ksp.layer_id = kl.id 
         and ksep.schedule_id = ksp.id 
         and ksp.con_code = kc.con_code
         and ksp.con_code = any(select con_code from krecon_construction where is_live = 'Y')
         group by ksp.con_code");

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function =="getLayerName"){

    $query = sprintf("select ksp.*, kl.layer_name, ksep.real_date_count, ksep.real_date_unit
    from krecon_schedule_process ksp,
            krecon_layer kl,
            krecon_schedule_execution_process ksep
    where con_code = '%s' and ksp.layer_id = kl.id and ksep.schedule_id = ksp.id
    group by layer_id",$_REQUEST['id']);

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}else if($function =="getProcessByLayerName"){

    $query = sprintf("select ksp.*, kl.layer_name, ksep.real_date_count, ksep.real_date_unit, ksp.process_type
    from krecon_schedule_process ksp,
            krecon_layer kl,
            krecon_schedule_execution_process ksep
    where con_code = '%s' and ksp.layer_id = kl.id and ksep.schedule_id = ksp.id and layer_name='%s'",$_REQUEST['id'],$_REQUEST['layer_name']);

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function =="selectEquipmentBtn"){

    $query = sprintf("select * from
    (select *   from krecon_live_scene 
    where con_code='%s'
    and session_id = '%s'
order by id desc) a
group by con_code",$_REQUEST['id'],$_REQUEST['name']);

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function =="getVideoFile"){

    $query = sprintf("select * from krecon_video 
                    where con_code= '%s' and session_id = '%s' 
                    order by id desc limit 2",$_REQUEST['id'],$_REQUEST['session_id']);

    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function == "getWeather"){
    $query = sprintf("select id,con_code,equipment_id,session_id, regdate, ambient_temp,con_area_name from (
        select KLS.*, KC.con_area_name from 
        krecon_live_scene KLS,
        krecon_construction KC
        where KLS.con_code = any(select krecon_construction.con_code
                from krecon_construction 
                where is_live = 'Y') and KLS.con_code = KC.con_code
        order by id desc
        ) as aa
        group by con_code");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[]= $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function == "getEndDate"){
    $query = sprintf("select * from krecon_construction where is_live = 'Y'");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[]= $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}else if($function == "checkStatus"){
    $query = sprintf("select * from krecon_live_session where id = '%s'",$_REQUEST['session_id']);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[]= $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}



mysql_close($connect);

?>


