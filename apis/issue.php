<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: X-PINGOTHER, Content-Type, enctype");
header('content-type: application/json; charset=utf-8');

include '../inc/db_setting.inc';    

$function = $_REQUEST['function'];

if($function=="contents"){
    $id = $_REQUEST['id'];

    $query = sprintf("select a.id,
                             b.con_field_name,
                             b.con_code,
                             b.con_lat,
                             b.con_long,
                             b.is_live,
                             b.con_area_name,a.title 
                        from krecon_issue_scene a, 
                             krecon_construction b
                        where b.con_code=a.con_code 
                        and a.con_code= '%s'
                        order by a.id",$id);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}elseif($function=="list"){
    $query = sprintf("select a.id,
                             b.con_field_name,
                             b.con_code,
                             b.con_lat,
                             b.con_long,
                             b.is_live,
                             b.con_area_name,a.title 
                        from krecon_issue_scene a, 
                             krecon_construction b
                        where b.con_code=a.con_code 
                        order by a.id");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}elseif($function=="listLimitFour"){
    $query = sprintf("select a.id,
                             b.con_field_name,
                             b.con_code,
                             b.con_lat,
                             b.con_long,
                             b.is_live,
                             b.con_area_name,a.title 
                        from krecon_issue_scene a, 
                             krecon_construction b
                        where b.con_code=a.con_code 
                        order by a.id desc limit 4");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}elseif($function=="listIsLive"){
    $query = sprintf("select a.id,
    b.con_field_name,
    b.con_area_name,a.title 
from krecon_issue_scene a, 
    krecon_construction b
where b.con_code=a.con_code and b.is_live = 'Y'
order by a.id");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}elseif($function=="edit"){
    $id = $_REQUEST['dbID'];
    $title = $_REQUEST['이슈'];
    $query = sprintf("update krecon_issue_scene 
                             set title = '%s'
                             where id= '%s'",$title,$id);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);

}elseif($function=="loadAddIssue"){
    $query = sprintf("select con_area_name from krecon_construction");
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
    
}elseif($function=="addIssueOnModal"){
    $con_area_name = $_REQUEST['con_area_name'];
    $title = $_REQUEST['title'];
    $contents = $_REQUEST['contents'];

    $query = sprintf("insert into krecon_issue_scene (con_code,title,contents)
                    values((select kc.con_code 
                    from krecon_construction kc 
                     where kc.con_area_name = '%s'),'%s','%s')",$con_area_name,$title,$contents);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}elseif($function=="showDetailIssueModal"){
    $query = sprintf("select * from krecon_issue_scene where id = '%s'",$_REQUEST['id']);
    $result = mysql_query($query);
    $contents = array();
    while($row = mysql_fetch_assoc($result)){
        $contents[] = $row;
    }
    mysql_free_result($result);

    $output = array("result"=>"ok","contents"=>$contents);
    echo json_encode($output);
}



mysql_close($connect);

?>