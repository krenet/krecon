<style>
  .d-none2{
      display:block !important;
      flex: 1 1 auto !important;
      width:1%;
      position:relative;
      padding-right: 1.25rem;
  }


</style>

<!-- Header -->
<header class="main-header " id="header">
    <nav class="navbar navbar-static-top navbar-expand-lg">
        <!-- Sidebar toggle button -->
        <button id="sidebar-toggler" class="sidebar-toggle">
            <span class="sr-only">Toggle navigation</span>
        </button>
        <!-- search form -->
        <div class="search-form d-none2 d-lg-inline-block">
            <div class="input-group">
             
                <input type="select" name="query" id="search-input" class="form-control" placeholder="현장명, 위치"
                    autofocus autocomplete="off" />
                    <!-- <select class="myselect form-control" id = "searchSelect" > -->
               
                    <!-- </select> -->

                    <!-- <button type="button" name="search" id="search-btn" class="btn btn-flat">
                    <i class="mdi mdi-magnify"></i>
                </button> -->
            </div>
            <div id="search-results-container">
                <ul id="search-results"></ul>
            </div>
        </div>

        <div class="navbar-right ">
            <ul class="nav navbar-nav">
                <!-- Github Link Button -->
                <li class="github-link mr-3">
                    <button class="btn btn-outline-secondary btn-sm" id="searchBtn">
                        <span class="d-none d-md-inline-block mr-2">검색</span>
                        <i class="mdi mdi-file-find"></i>
                    </button>

                </li>
                <li class="dropdown notifications-menu">
                    <button class="dropdown-toggle" data-toggle="dropdown">
                        <i class="mdi mdi-bell-outline"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li class="dropdown-header">5개의 알림이 있습니다.</li>
                        <li>
                            <a href="#">
                                <i class="mdi mdi-account-plus"></i> New user registered
                                <span class=" font-size-12 d-inline-block float-right"><i
                                        class="mdi mdi-clock-outline"></i> 10 AM</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="mdi mdi-account-remove"></i> User deleted
                                <span class=" font-size-12 d-inline-block float-right"><i
                                        class="mdi mdi-clock-outline"></i> 07 AM</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="mdi mdi-chart-areaspline"></i> Sales report is ready
                                <span class=" font-size-12 d-inline-block float-right"><i
                                        class="mdi mdi-clock-outline"></i> 12 PM</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="mdi mdi-account-supervisor"></i> New client
                                <span class=" font-size-12 d-inline-block float-right"><i
                                        class="mdi mdi-clock-outline"></i> 10 AM</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="mdi mdi-server-network-off"></i> Server overloaded
                                <span class=" font-size-12 d-inline-block float-right"><i
                                        class="mdi mdi-clock-outline"></i> 05 AM</span>
                            </a>
                        </li>
                        <li class="dropdown-footer">
                            <a class="text-center" href="#"> View All </a>
                        </li>
                    </ul>
                </li>
                <!-- User Account -->
                <li class="dropdown user-menu">
                    <button href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                        <img src="assets/img/user/user.png" class="user-image" alt="User Image" />
                        <span class="d-none d-lg-inline-block" id="user_nick">김그래</span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <!-- User image -->
                        <li class="dropdown-header">
                            <img src="assets/img/user/user.png" class="img-circle" alt="User Image" />
                            <div class="d-inline-block">
                                김그래 <small class="pt-1">krekim@krenet.co.kr</small>
                            </div>
                        </li>

                        <li>
                            <a href="?cat=documents&page=profile">
                                <i class="mdi mdi-account"></i> 내 프로필
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="mdi mdi-email"></i> 메시지
                            </a>
                        </li>
                        <li>
                            <a href="#"> <i class="mdi mdi-diamond-stone"></i> 현장 </a>
                        </li>
                        <li>
                            <a href="#"> <i class="mdi mdi-settings"></i> 계정 설정 </a>
                        </li>

                        <li class="dropdown-footer">
                            <a href="signin.html"> <i class="mdi mdi-logout"></i> 로그아웃 </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>


</header>

<script type="text/javascript">

$(document).ready(function () {
//       $(".myselect").select2();

//       rpc("documents", "conList", null, function (data) {
//             if (data.result != "ok") {
//                 alert("통신에 문제가 있습니다.");
//                 return;
//             }
//             var contents = data.contents;
//             var item;
//             var options = "";

// // $('.select2-container--open').val('현장명, 위치');
//             // options += "<option value='all'>전체</option>";

//             for (inx = 0; inx < contents.length; ++inx) {
//                 item = contents[inx];
//                 options += "<option value=" + item.con_code + ">" + item.con_area_name + "</option>";

//             }
//             $(".myselect").html(options);
            
//         //   console.log($('.select2-selection__rendered'));
//         //     // $('.select2-selection__rendered')[0].text('현장명, 위치');
//         //     // $('.select2-selection__rendered')[0].val('현장명, 위치');
//         //     $('.select2-selection__rendered')[0].attr('innerText', '현장명, 위치');
            
//         });
});


//select2 쓸경우s
// $('#searchBtn').on("click",function(){
// console.log($('.select2-selection__rendered')[0].innerText);
// console.log($('.select2-selection__rendered')[0]);
// console.log($('#searchSelect')[0].selectedOptions[0].value);
// searchWord = $('#searchSelect')[0].selectedOptions[0].value;
// location.href=".?cat=site&page=site_detail&id="+searchWord;
// });
    //박혜원 수정 할 예정
    $('#searchBtn').on("click",function(){
        // console.log();
        location.href=".?cat=site&page=site_search&text="+$('#search-input')[0].value;
    });
    //박혜원 수정 할 예정

</script>