<footer class="footer mt-auto">
            <div class="copyright bg-white">
              <p>
                &copy; <span id="copy-year">2019</span> Copyright Krecon (KREnet CONstruction manager) by
                <a
                  class="text-primary"
                  href="http://www.krenet.com/"
                  target="_blank"
                  >KRENET</a
                >.
              </p>
            </div>
            <script>
                var d = new Date();
                var year = d.getFullYear();
                document.getElementById("copy-year").innerHTML = year;
            </script>
          </footer>