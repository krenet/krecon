 <!--
      ====================================
      ——— LEFT SIDEBAR WITH FOOTER
      =====================================
    -->
    <aside class="left-sidebar bg-sidebar">
      <div id="sidebar" class="sidebar sidebar-with-footer">
        <!-- Aplication Brand -->
        <div class="app-brand">
          <a href="/">
            <svg
              class="brand-icon"
              xmlns="http://www.w3.org/2000/svg"
              preserveAspectRatio="xMidYMid"
              width="30"
              height="33"
              viewBox="0 0 30 33"
            >
              <g fill="none" fill-rule="evenodd">
                <path
                  class="logo-fill-blue"
                  fill="#7DBCFF"
                  d="M0 4v25l8 4V0zM22 4v25l8 4V0z"
                />
                <path class="logo-fill-white" fill="#FFF" d="M11 4v25l8 4V0z" />
              </g>
            </svg>
            <span class="brand-name">KRECON</span>
          </a>
        </div>
        <!-- begin sidebar scrollbar -->
        <div class="sidebar-scrollbar">

          <!-- sidebar menu -->
          <ul class="nav sidebar-inner" id="sidebar-menu">       
              <!-- <li  class="has-sub <?php echo $sel_cat=='preConst' ? 'active' : '' ?>">
                <a class="sidenav-item-link" href="?cat=preConst&page=preConst_list" data-target="#preConst"
                  aria-expanded="false" aria-controls="preConst">                      
                  <i class="mdi mdi-clipboard-text"></i>
                  <span class="nav-text">사전조사</span> <b class="caret"></b>
                </a>
              </li> -->

              <li  class="has-sub <?php echo $sel_cat=='site' ? 'active expand' : '' ?>" >
                <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#site"
                  aria-expanded="false" aria-controls="site">
                  <i class="mdi mdi-road-variant"></i>
                  <span class="nav-text">현장관리</span> <b class="caret"></b>
                </a>
                <ul  class="collapse  <?php echo $sel_cat=='site' ? 'show' : '' ?>"  id="site"
                  data-parent="#sidebar-menu">
                  <div class="sub-menu">                                         
                      
                        <li  <?php echo $sel_page=='site_list' ? 'class="active"' : '' ?> >
                          <a class="sidenav-item-link" href="?cat=site&page=site_list">
                            <span class="nav-text">시공리스트</span>
                            
                          </a>
                        </li>
                        <li  <?php echo $sel_page=='site_list_all' ? 'class="active"' : '' ?> >
                          <a class="sidenav-item-link" href="?cat=site&page=site_list_all">
                            <span class="nav-text">전체시공리스트</span>
                            
                          </a>
                        </li>
<!--
                            <li  <?php echo $sel_page=='site_now' ? 'class="active"' : '' ?>>
                              <a class="sidenav-item-link" href="?cat=site&page=site_now">
                                <span class="nav-text">시공현장</span>
                                
                                <span class="badge badge-success">now!</span>
                                
                              </a>
                            </li>
-->                            

                  </div>
                </ul>
              </li>
            

            

              <li  class="has-sub  <?php echo $sel_cat=='charts' ? 'active expand' : '' ?>" >
                <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#charts"
                  aria-expanded="false" aria-controls="charts">
                  <i class="mdi mdi-chart-pie"></i>
                  <span class="nav-text">통계</span> <b class="caret"></b>
                </a>
                <ul  class="collapse  <?php echo $sel_cat=='charts' ? 'show' : '' ?>"  id="charts"
                  data-parent="#sidebar-menu">
                  <div class="sub-menu">
           
                        <li   <?php echo $sel_page=='charts_total' ? 'class="active"' : '' ?> >
                          <a class="sidenav-item-link" href="?cat=charts&page=charts_total">
                            <span class="nav-text">수익통계</span>
                          </a>
                        </li>

                        <li <?php echo $sel_page=='charts_regions' ? 'class="active"' : '' ?>>
                            <a class="sidenav-item-link" href="?cat=charts&page=charts_regions">
                              <span class="nav-text">지역별통계</span>                                
                            </a>
                        </li>

                        <li <?php echo $sel_page=='charts_terms' ? 'class="active"' : '' ?>>
                            <a class="sidenav-item-link" href="?cat=charts&page=charts_terms">
                              <span class="nav-text">운영시간통계</span>
                            </a>
                        </li>

                        <li <?php echo $sel_page=='charts_plan' ? 'class="active"' : '' ?>>
                            <a class="sidenav-item-link" href="?cat=charts&page=charts_plan">
                              <span class="nav-text">공사일정통계</span>
                            </a>
                        </li>                            
                  </div>
                </ul>
              </li>
            
<!--
                
                  <li  class="has-sub <?php echo $sel_cat=='resources' ? 'active expand' : '' ?>" >
                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#resources"
                      aria-expanded="false" aria-controls="resources">
                      <i class="mdi mdi-oil"></i>
                      <span class="nav-text">자원관리</span> <b class="caret"></b>
                    </a>
                    <ul  class="collapse <?php echo $sel_cat=='resources' ? 'show' : '' ?>"  id="resources"
                      data-parent="#sidebar-menu">
                      <div class="sub-menu">
                        
                            <li class="section-title">
                              장비관리
                            </li>
                          
                            <li  <?php echo $sel_page=='resources_roller' ? 'class="active"' : '' ?>>
                              <a class="sidenav-item-link" href="introduction.html">
                                <span class="nav-text">롤러</span>
                              </a>
                            </li>
                                                    
                            <li  <?php echo $sel_page=='resources_finisher' ? 'class="active"' : '' ?>>
                              <a class="sidenav-item-link" href="setup.html">
                                <span class="nav-text">피니셔</span>
                              </a>                              
                            </li>               
                          
                            <li class="section-title">
                              인력관리
                            </li>

                            <li  <?php echo $sel_page=='resources_place' ? 'class="active"' : '' ?>>
                                <a class="sidenav-item-link" href="introduction.html">
                                  <span class="nav-text">본사</span>
                                  
                                </a>
                            </li>
                            <li  <?php echo $sel_page=='resources_site' ? 'class="active"' : '' ?>>
                                <a class="sidenav-item-link" href="introduction.html">
                                  <span class="nav-text">현장</span>                                  
                                </a>
                            </li>

                          
                            <li class="section-title">
                                재료관리
                            </li>
                            <li  <?php echo $sel_page=='resources_plants' ? 'class="active"' : '' ?>>
                                <a class="sidenav-item-link" href="introduction.html">
                                  <span class="nav-text">플랜트</span>
                                </a>
                            </li>
                            <li  <?php echo $sel_page=='resources_aggregate' ? 'class="active"' : '' ?>>
                                <a class="sidenav-item-link" href="introduction.html">
                                  <span class="nav-text">골재</span>
                                </a>
                            </li>

                      </div>
                    </ul>
                  </li>
-->

              <li  class="has-sub <?php echo $sel_cat=='community' ? 'active expand' : '' ?>" >
                <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#community"
                  aria-expanded="false" aria-controls="community">                      
                  <i class="mdi mdi-forum"></i>
                  <span class="nav-text">커뮤니티</span> <b class="caret"></b>
                </a>
                <ul  class="collapse <?php echo $sel_cat=='community' ? 'show' : '' ?>"  id="community"
                  data-parent="#sidebar-menu">
                  <div class="sub-menu"> 
                    <li <?php echo $sel_page=='community_advice' ? 'class="active"' : '' ?>>
                        <a class="sidenav-item-link" href="?cat=community&page=community_advice">
                            <span class="nav-text">원격자문</span>
                        </a>
                    </li>
                    <li <?php echo $sel_page=='community_adviser' ? 'class="active"' : '' ?>>
                        <a class="sidenav-item-link" href="?cat=community&page=community_adviser">
                            <span class="nav-text">자문위원 관리</span>
                        </a>
                    </li>        
                    <li <?php echo $sel_page=='community_advice_list' ? 'class="active"' : '' ?>>
                        <a class="sidenav-item-link" href="?cat=community&page=community_advice_list">
                            <span class="nav-text">자문 리스트</span>
                        </a>
                    </li>                    
                    <li  <?php echo $sel_page=='community_site_issue' ? 'class="active"' : '' ?> >
                        <a class="sidenav-item-link" href="?cat=community&page=community_site_issue">
                            <span class="nav-text">현장 이슈 관리</span>
                        </a>
                    </li>   
                    <li  <?php echo $sel_page=='community_official' ? 'class="active"' : '' ?>>
                        <a class="sidenav-item-link" href="?cat=community&page=community_official">
                            <span class="nav-text">공무원 정보</span>
                        </a>
                      </li>                   
                  </div>
                </ul>
              </li>
            

              <li  class="has-sub <?php echo $sel_cat=='documents' ? 'active expand' : '' ?>" >
                <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#documents"
                  aria-expanded="false" aria-controls="documents">                      
                  <i class="mdi mdi-book-open-page-variant"></i>
                  <span class="nav-text <?php echo $sel_cat=='documents' ? 'show' : '' ?>">자료관리</span> <b class="caret"></b>
                </a>
                <ul  class="collapse <?php echo $sel_cat=='documents' ? 'show' : '' ?>"  id="documents"
                  data-parent="#sidebar-menu">
                  <div class="sub-menu"> 
                    <li <?php echo $sel_page=='documents_specification' ? 'class="active"' : '' ?>>
                        <a class="sidenav-item-link"  href="?cat=documents&page=documents_specification">
                            <span class="nav-text">시방서 리스트</span>
                        </a>
                    </li>
                    <li <?php echo $sel_page=='documents_preConst' ? 'class="active"' : '' ?>>
                        <a class="sidenav-item-link"  href="?cat=documents&page=documents_preConst">
                            <span class="nav-text">사전조사 리스트</span>
                        </a>
                    </li>
                    <li <?php echo $sel_page=='documents_completion' ? 'class="active"' : '' ?>>
                        <a class="sidenav-item-link"  href="?cat=documents&page=documents_completion">
                            <span class="nav-text">준공서류 리스트</span>
                        </a>
                    </li>                            
                    <li   <?php echo $sel_page=='documents_laws' ? 'class="active"' : '' ?>>
                    <a class="sidenav-item-link" href="?cat=documents&page=documents_laws">
                        <span class="nav-text">관련법률</span>
                      </a>
                    </li>                     
                  </div>
                </ul>
              </li>

              <!-- <li  class="has-sub <?php echo $sel_cat=='preConst' ? 'active expand' : '' ?>" >
                <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#preConst"
                  aria-expanded="false" aria-controls="preConst">                      
                  <i class="mdi mdi-clipboard-text"></i>
                  <span class="nav-text <?php echo $sel_cat=='preConst' ? 'show' : '' ?>">시공참여여부</span> <b class="caret"></b>
                </a>
                <ul  class="collapse <?php echo $sel_cat=='preConst' ? 'show' : '' ?>"  id="preConst"
                  data-parent="#sidebar-menu">
                  <div class="sub-menu"> 
                    <li <?php echo $sel_page=='preConst_fieldSurvey' ? 'class="active"' : '' ?>>
                      <a class="sidenav-item-link"  href="?cat=preConst&page=preConst_fieldSurvey">
                        <span class="nav-text">현장조사</span>
                      </a>
                    </li>
                    <li <?php echo $sel_page=='preConst_history' ? 'class="active"' : '' ?>>
                      <a class="sidenav-item-link"  href="?cat=preConst&page=preConst_history">
                        <span class="nav-text">이력정보</span>
                      </a>
                    </li>                            
                    <li   <?php echo $sel_page=='preConst_issues' ? 'class="active"' : '' ?>>
                      <a class="sidenav-item-link" href="?cat=preConst&page=preConst_issues">
                        <span class="nav-text">이슈사항</span>
                      </a>
                    </li>                     
                  </div>
                </ul>
              </li> -->
          </ul>

        </div>

        <hr class="separator" />

        <div class="sidebar-footer">
          <div class="sidebar-footer-content">
            <h6 class="text-uppercase">
              Cpu Uses <span class="float-right">40%</span>
            </h6>
            <div class="progress progress-xs">
              <div
                class="progress-bar active"
                style="width: 40%;"
                role="progressbar"
              ></div>
            </div>
            <h6 class="text-uppercase">
              Memory Uses <span class="float-right">65%</span>
            </h6>
            <div class="progress progress-xs">
              <div
                class="progress-bar progress-bar-warning"
                style="width: 65%;"
                role="progressbar"
              ></div>
            </div>
          </div>
        </div>
      </div>
    </aside>