const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffprobePath = require('@ffprobe-installer/ffprobe').path;
const ffmpeg = require('fluent-ffmpeg');
const videoshow = require('videoshow');
var FormData = require('form-data');
var request = require('request');
ffmpeg.setFfmpegPath(ffmpegPath);
ffmpeg.setFfprobePath(ffprobePath);

var sleep = require('sleep-promise');
const Fs = require('fs');
const Path = require('path');
const Axios = require('axios');
var liveData = new Array();
var msx_arrImages = new Array();
var visible_arrImages = new Array();
var arrayCount = 0;
var rows_id = 0;
var statusCheck = 0;
var msx_filename = new Array();
var visible_filename = new Array();     
var regdate = 0;
var con_code = 0;
var equipment_id = 0;
var rows;
var msx_count = 0;
var visible_count = 0;

function request_php_function($service_name, $function, $param, $callback) {
  var service_file = "http://localhost/" + $service_name + ".php";
  var url = service_file + "?function=" + $function;
  Axios({
    method: "POST",
    url: url,
    // responseType: 'stream',
    // dataType:"json",
    data: $param
  }).then(function (res) {
    // console.log($service_name + "." + $function + " 서버 응답 : ", res);
    if ($callback) {
      $callback.call(this, res);
    }
  }).catch(function (err) {

    // console.log(err);
  });
}


request_php_function("session_live_check", "check_converting", null, function (res) {
  var statusCount = res.data.contents.sc;
  console.log(statusCount);

  if (statusCount == 0) {
    statusCheck = 0;

    request_php_function("session_live_check", "type_check", null, function(res){

      console.log(res.data.contents[0].type);


      for(var i=0; i<res.data.contents.length; i++){

      }

  

    });



    // request_php_function("session_live_check", "convert_ready_images_encoding", null, function (res) {
    //   rows = res.data.contents;

    //   if (statusCheck == 0) {
    //     arrayCount = rows.length;
    //     rows_id = rows[0].session_id;
    //     regdate = rows[0].regdate;
    //     con_code = rows[0].con_code;
    //     equipment_id = rows[0].equipment_id;

    //     regdate = regdate.substring(0, 10);

    //     for (var i = 0; i < arrayCount; i++) {
    //       if (rows[i].type == "msx") {
    //         msx_filename.push(rows[i].filename);
    //       } else if (rows[i].type == "visible") {
    //         visible_filename.push(rows[i].filename);
    //       }
    //     }

    //     request_php_function("session_live_check", "update_session_to_converting", "id=" + rows_id, function (res, err) {


    //     });

    //     for (var i = 0; i < arrayCount; i++) {

    //       downloadImage(i);

    //       if (i == 0 || i % 2 == 0) {
    //         liveData.push({
    //           regdate: rows[i].regdate,
    //           regtime: rows[i].regtime,
    //           mixture_temp: rows[i].mixture_temp,
    //           surface_temp: rows[i].surcate_temp,
    //           ambient_temp: rows[i].ambient_temp,
    //           inst_width: rows[i].inst_width,
    //           inst_level: rows[i].inst_level
    //         });
    //       }

    //     }


    //     let data = JSON.stringify(liveData);
    //     Fs.writeFileSync(rows_id + '.json', data);


    //   } else if (statusCheck == 1) {

    //     console.log("aaaaaaaaaaaaaa");
    //   }


    // });




  } else {
    statusCheck = 1;
    sleep(5000).then(function () {
      // console.log('5 seconds later …');
    });
  }
});


var videoOptions = {
  fps: 30,
  loop: 1, // seconds
  transition: false,
  // transitionDuration: 1, // seconds
  videoBitrate: 1024,
  // size: '640x?',
  // audioBitrate: '128k',
  // audioChannels: 2,
  format: 'mp4',
  pixelFormat: 'yuv420p'

};


async function downloadImage(num) {

  if (rows[num].type == "msx") {

    const url = 'http://localhost/live_img_copy/' + msx_filename[msx_count];
    msx_arrImages.push(__dirname + '/msx_images/' + msx_count + '.jpg');
    const path = Path.resolve(__dirname, 'msx_images', msx_count + '.jpg');
    const writer = Fs.createWriteStream(path);

    msx_count++;


    const response = await Axios({
      url,
      method: 'GET',
      responseType: 'stream'
    });

    response.data.pipe(writer);

    //arrayCount == 12 num == 11;
    if (num == arrayCount - 2) {

      setTimeout(function () {
        var videoName = "msx-" + rows_id + '-' + regdate + '.mp4';
        videoshow(msx_arrImages, videoOptions)
          .save('videos/' + videoName)
          .on('start', function (command) {
            // console.log('ffmpeg process started:', command)
          })
          .on('error', function (err, stdout, stderr) {
            // console.error('Error:', err)
            // console.error('ffmpeg stderr:', stderr)
          })
          .on('end', function (output) {
            // console.error('Video created in:', output)

            // request_php_function("session_complete", "delete_img_from_server", "id=" + rows_id, function (res, err) {
            //   if (err) throw err;
            //   console.log('dddddddddddddddddddddddddddddd', res.data.contents);
            request_php_function("session_complete", "update_session_to_converted", "id=" + rows_id, function (res, err) {
              //     if (err) throw err;
              //     request_php_function("session_complete", "delete_img_from_db", "id=" + rows_id, function (res, err) {
              //       if (err) throw err;
              //     });

            });
            // });


            for (var i = 0; i < arrayCount; i++) {
              const path = './msx_images/' + i + '.jpg'
              Fs.unlink(path, (err) => {
                if (err) {
                  console.error("errrrrrrrrrrrrrrrrrrrrrrrrrr", err)
                  return
                }
                //file removed

              });
            }


            // 파일 DB에 올리기 시작
            var sendData = {
              con_code: con_code,
              session_id: rows_id,
              equipment_id: equipment_id,
              regdate: regdate,
              filename: "msx-" + rows_id + "-" + regdate + ".mp4"
            };
            request.post({ url: 'http://localhost/session_complete.php?function=upload_video_to_db', formData: sendData }, function (err, httpResponse, body) {
              if (err) {
                return console.error('upload failed:', err);
              }
            });


            // request_php_function("session_complete", "upload_video_to_db", sendData, function(err, res){
            //   console.log(res.data);
            //   if (err) throw err;
            // });
            // 파일 DB에 올리기 종료

            // 파일 서버에 올리기 시작
            var formData = {
              newFile: Fs.createReadStream(__dirname + '\\videos\\' + videoName),
              // newFile: Fs.createReadStream(__dirname + '\\videos\\132-2019-09-17.mp4'),
            };

            request.post({ url: 'http://localhost/session_complete.php?function=upload_video_to_server', formData: formData }, function (err, httpResponse, body) {
              if (err) {
                return console.error('upload failed:', err);
              }
              console.log('Upload successful!  Server responded with:', body);
            });
            // 파일 서버에 올리기 종료

          });

      }, 2000);
    }

    return new Promise((resolve, reject) => {
      writer.on('finish', resolve)
      writer.on('error', reject)

    });





  } else if (rows[num].type == "visible") {

    const url = 'http://localhost/live_img_copy/' + visible_filename[visible_count];
    visible_arrImages.push(__dirname + '/visible_images/' + visible_count + '.jpg');
    const path = Path.resolve(__dirname, 'visible_images', visible_count + '.jpg');
    const writer = Fs.createWriteStream(path);

    visible_count++;

    const response = await Axios({
      url,
      method: 'GET',
      responseType: 'stream'
    });

    response.data.pipe(writer);


    if (num == arrayCount - 1) {
      setTimeout(function () {
        var videoName = "visible-" + rows_id + '-' + regdate + '.mp4';
        videoshow(visible_arrImages, videoOptions)
          .save('videos/' + videoName)
          .on('start', function (command) {
            // console.log('ffmpeg process started:', command)
          })
          .on('error', function (err, stdout, stderr) {
            // console.error('Error:', err)
            // console.error('ffmpeg stderr:', stderr)
          })
          .on('end', function (output) {
            // console.error('Video created in:', output)

            // request_php_function("session_complete", "delete_img_from_server", "id=" + rows_id, function (res, err) {
            //   if (err) throw err;
            //   console.log('dddddddddddddddddddddddddddddd', res.data.contents);
            request_php_function("session_complete", "update_session_to_converted", "id=" + rows_id, function (res, err) {
              //     if (err) throw err;
              //     request_php_function("session_complete", "delete_img_from_db", "id=" + rows_id, function (res, err) {
              //       if (err) throw err;
              //     });

            });
            // });


            for (var i = 0; i < arrayCount; i++) {
              const path = './visible_images/' + i + '.jpg'
              Fs.unlink(path, (err) => {
                if (err) {
                  console.error(err)
                  return
                }
                //file removed

              });
            }


            // 파일 DB에 올리기 시작
            var sendData = {
              con_code: con_code,
              session_id: rows_id,
              equipment_id: equipment_id,
              regdate: regdate,
              filename: "visible-" + rows_id + "-" + regdate + ".mp4"
            };
            request.post({ url: 'http://localhost/session_complete.php?function=upload_video_to_db', formData: sendData }, function (err, httpResponse, body) {
              if (err) {
                return console.error('upload failed:', err);
              }
            });


            // request_php_function("session_complete", "upload_video_to_db", sendData, function(err, res){
            //   console.log(res.data);
            //   if (err) throw err;
            // });
            // 파일 DB에 올리기 종료

            // 파일 서버에 올리기 시작
            var formData = {
              newFile: Fs.createReadStream(__dirname + '\\videos\\' + videoName),
              // newFile: Fs.createReadStream(__dirname + '\\videos\\132-2019-09-17.mp4'),
            };

            request.post({ url: 'http://localhost/session_complete.php?function=upload_video_to_server', formData: formData }, function (err, httpResponse, body) {
              if (err) {
                return console.error('upload failed:', err);
              }
              console.log('Upload successful!  Server responded with:', body);
            });
            // 파일 서버에 올리기 종료

          });

      }, 2000);
    }

    return new Promise((resolve, reject) => {
      writer.on('finish', resolve)
      writer.on('error', reject)

    });
  }

}
