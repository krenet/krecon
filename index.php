<?php
header("Access-Control-Allow-Origin", "*");
header("Access-Control-Allow-Credentials", "true");
header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
  //카테고리와 페이지 세팅
  if(isset($_GET['cat'])){
    $sel_cat = $_GET['cat'];
  }else{
    $sel_cat = 'site';
  }
    
  if(isset($_GET['page'])){
    if(strpos($_GET['page'], '.html') !== FALSE) {
        $sel_page = $_GET['page'];
    } else {
        $sel_page = $_GET['page'].".php";
    }
    
  }else{
    //지은수정
    // $sel_page = 'site_list';
    $sel_page = 'site_dashboard.php';
  }

    
    
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <?php   include("inc/head.php"); ?>
</head>

<body class="sidebar-fixed sidebar-dark header-light header-fixed" id="body">
    <script>
      
        NProgress.configure({ showSpinner: false });
        NProgress.start();
    </script>
    <style>
        .today_status p {
            display: inline-block;
        }

        .dash-1st-row .card-body {
            min-height: 248px;
            max-height: 248px
        }
    </style>
    <div class="mobile-sticky-body-overlay"></div>

    <div class="wrapper">
        <?php   include("inc/sidemenu.php"); ?>

        <div class="page-wrapper">
            <?php   include("inc/header.php"); ?>

            <div class="content-wrapper">
                <div class="content">
                    <?php   include("pages/".$sel_page); ?>
                </div>
            </div>

            <?php   include("inc/footer.php"); ?>
        </div>
    </div>

    <?php   include("inc/bottom.php"); ?>
</body>

</html>