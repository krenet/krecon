function rpc($service_name,$function, $param, $callback){
    var service_file = "apis/"+$service_name+".php";
    var url = service_file+"?function="+$function;
    $.ajax({
        method:"POST",
        url:url,
        dataType:"json",
        data: $param
    }).done(function(res){
        console.log($service_name+"."+$function+" 서버 응답 : ",res);
        if($callback){
            $callback.call(this,res);
        }
    });

}
