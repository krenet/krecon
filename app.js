const express = require('express');
const socket = require('socket.io');
const http = require('http');
const app = express();
const mysql = require('mysql');
const server = http.createServer(app);
const io = socket(server);
const multer = require('multer');
const crypto = require('crypto');
var db_saved_file_name = "";

const connection = mysql.createConnection({
  host: 'sps.3core.co.kr',
  port: 3306,
  user: 'krecon',
  password: 'krecon',
  database: 'krecon'
})
connection.connect();


const chat = io.sockets.on('connection', function (socket) {
  socket.on('sMsg', function (data) {
    var room = data.room;
    var _s = new Array();
    _s = data.file.split('\\');
    data.file = _s[_s.length - 1];

    _s = data.file.split('.');
    var format_s = _s[_s.length - 1];
    db_saved_file_name = 'kreconDoc-' + Date.now() + '.' + format_s;

    const cipher = crypto.createCipher('aes-256-cbc', 'hiddenKey');
    var en_result = cipher.update(data.msg, 'utf8', 'base64');
    en_result += cipher.final('base64');
    console.log('암호화된 암호', en_result);

    const decipher = crypto.createDecipher('aes-256-cbc', 'hiddenKey');
    var de_result = decipher.update(en_result, 'base64', 'utf8');
    de_result += decipher.final('utf8');
    console.log('복호화된 평문: ', de_result);

       connection.query("INSERT INTO chat1 (room, uname, msg, date, file, msg_key, saved_file_name) VALUES (?, ?, ?, now(), ?, ?, ?)", [
      data.room, data.name, data.msg, data.file, en_result, db_saved_file_name
    ], function () {
    });
    socket.join(room);
    chat.to(room).emit('rMsg', data);
  });
});

const storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './uploads');
  },
  filename: function (req, file, callback) {
    callback(null, db_saved_file_name);
  }
});
var upload = multer({ storage: storage }).array('kreconDoc', 2);

app.get('/', function (req, res) {
  res.sendFile(__dirname + "/index.html");
});

app.get('/download/', function (req, res) {
  var file = __dirname + '/uploads/' + db_saved_file_name;
  res.download(file); // Set disposition and send it.
});

app.post('/api/photo', function (req, res) {
  upload(req, res, function (err) {
    if (err) {
      return res.end("Error uploading file.");
    }
    // res.end("File is uploaded");
  });
});

server.listen(8000, function () {
  console.log('서버 실행중~~~~~~');
})



