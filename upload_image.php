<?php
include 'inc/util.php';

// 설정
$uploads_dir = '/datas/krecon/live_img_copy';
$min_img_dir = '/datas/krecon/live_min_img_copy';
$allowed_ext = array('jpg','jpeg','png','gif');
 
// 변수 정리
$msx_error = $_FILES['msxFile']['error'];
$msx_name = $_FILES['msxFile']['name'];

$visible_error = $_FILES['visibleFile']['error'];
$visible_name = $_FILES['visibleFile']['name'];

$type = $_POST['type'];
$ambient_temp = $_POST['ambient_temp'];
$mixture_temp = $_POST['mixture_temp'];
$surface_temp = $_POST['surface_temp'];
$inst_width = $_POST['inst_width'];
$equip_location_longitude = $_POST['equip_location_longitude'];
$equip_location_latitude = $_POST['equip_location_latitude'];

$inst_level = $_POST['inst_level'];
$con_code = $_POST['con_code'];
$equipment_id = $_POST['equipment_id'];

$ext = array_pop(explode('.', $msx_name));
 
// 오류 확인
if( $error != UPLOAD_ERR_OK ) {
	switch( $error ) {
		case UPLOAD_ERR_INI_SIZE:
		case UPLOAD_ERR_FORM_SIZE:
			echo "파일이 너무 큽니다. ($error)";
			break;
		case UPLOAD_ERR_NO_FILE:
			echo "파일이 첨부되지 않았습니다. ($error)";
			break;
		default:
			echo "파일이 제대로 업로드되지 않았습니다. ($error)";
	}
	
	exit;
}
 
// 확장자 확인
if( !in_array($ext, $allowed_ext) ) {
	echo $ext."은 허용되지 않는 확장자입니다.";
	

	exit;
}
 
// msx 파일 이동

do{
	$msx_filename = makeName(10,'msx-').'.'.$ext;
	
	if(!file_exists($msx_filename)) {
		
		break;
	}
	
}while(true);

move_uploaded_file( $_FILES['msxFile']['tmp_name'], "$uploads_dir/$msx_filename");

//이미지 소스 얻기
$source = imagecreatefromjpeg("$uploads_dir/$msx_filename");

//작은 이미지 만들기&회전
list($width, $height) = getimagesize("$uploads_dir/$msx_filename");
$newwidth = $width/2;
$newheight = $height/2;
$destination = imagecreatetruecolor($newwidth, $newheight);
imagecopyresampled($destination, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
$rotation = imagerotate ( $destination, 90 , 0); 

imagejpeg($rotation, "$min_img_dir/$msx_filename", 60);

//이미지 리소스 메모리 반환
imagedestroy($source);
imagedestroy($destination);
imagedestroy($rotation);

//visible 파일 이동

do{
	$visible_filename = makeName(10,'visible-').'.'.$ext;
	// $visible_name = $visible_filename;
	if(!file_exists($visible_filename)) {
		
		break;
	}
	
}while(true);

move_uploaded_file( $_FILES['visibleFile']['tmp_name'], "$uploads_dir/$visible_filename");
//이미지 소스 얻기
$source = imagecreatefromjpeg("$uploads_dir/$visible_filename");

//작은 이미지 만들기&회전
list($width, $height) = getimagesize("$uploads_dir/$visible_filename");
$newwidth = $width/2;
$newheight = $height/2;
$destination = imagecreatetruecolor($newwidth, $newheight);
imagecopyresampled($destination, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
$rotation = imagerotate ( $destination, 90 , 0); 
imagejpeg($rotation, "$min_img_dir/$visible_filename", 60);

//이미지 리소스 메모리 반환
imagedestroy($source);
imagedestroy($destination);
imagedestroy($rotation);

//DB접속
include 'inc/db_setting.inc';
// 파일 이동 성공시 DB에 파일 정보 입력

//insert session

$query = sprintf("insert into krecon_live_scene (con_code,
											session_id,
											equipment_id,
											equip_location_longitude, 
											equip_location_latitude, 
											`type`, 
											filename, 
											regtime, 
											regdate, 
											ambient_temp, 
											mixture_temp, 
											surface_temp, 
											inst_width, 
											inst_level) 
				values ('%s',(select id from krecon_live_session where con_code = '%s' and equipment_id = '%s' and status = 'live'),'%s','%s','%s','%s','%s',now(),now(),%s,%s,%s,%s,%s)",
				$con_code,$con_code, $equipment_id,$equipment_id,$equip_location_longitude,$equip_location_latitude,'msx',$msx_filename, $ambient_temp, $mixture_temp, $surface_temp, $inst_width, $inst_level, $con_code, $equipment_id);
mysql_query($query);

$insert_id=mysql_insert_id();
echo json_encode($insert_id);

//select id from krecon_live_session where con_code = %d and equipment_id = %d and status = 'live'

//select IFNULL (id, (SELECT MAX(id) FROM krecon_live_session id))  from krecon_live_session where con_code = %d and equipment_id = %d and status = 'live'

//(SELECT IFNULL (session_id, (SELECT MAX(id) FROM krecon_live_session id)) FROM krecon_live_scene)

//SELECT id FROM krecon_live_session WHERE id = (SELECT MAX(id) FROM krecon_live_session id)

//SELECT IFNULL (session_id, 0) FROM krecon_live_scene

//SELECT id,

//  IFNULL(id , SELECT * FROM krecon_live_session WHERE id = (SELECT MAX(id) FROM krecon_live_session id))
 
//  from krecon_live_session 
//  where con_code = 3176 and equipment_id = 4 and status = 'live'

$query = sprintf("insert into krecon_live_scene (con_code,
												session_id,
												equipment_id,
												equip_location_longitude, 
												equip_location_latitude, 
												`type`, 
												filename, 
												regtime, 
												regdate, 
												ambient_temp, 
												mixture_temp, 
												surface_temp, 
												inst_width, 
												inst_level) 
				values ('%s',(select id from krecon_live_session where con_code = %d and equipment_id = %d and status = 'live'),'%s','%s','%s','%s','%s',now(),now(),%s,%s,%s,%s,%s)",
				$con_code,$con_code, $equipment_id,$equipment_id,$equip_location_longitude,$equip_location_latitude,'visible',$visible_filename, $ambient_temp, $mixture_temp, $surface_temp, $inst_width, $inst_level, $con_code, $equipment_id);
mysql_query($query);

mysql_close($connect);

// 파일 정보 출력
echo "<h2>파일 정보</h2>
<ul>
	<li>파일명: $name</li>
	<li>확장자: $ext</li>
	<li>파일형식: {$_FILES['myfile']['type']}</li>
	<li>파일크기: {$_FILES['myfile']['size']} 바이트</li>
</ul>";



?>