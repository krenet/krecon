<style>
    .progress-table {
        width: 100%;
    }

    .progress-table th {
        width: 120px;
    }

    .progress-table th span {
        width: 120px;
    }

    .progress-table tr {
        height: 50px;
    }

    .progress-bar {
        font-size: 0.7rem;
        padding-left: 5px;
        color: white;
    }

    .progress-table .progress {
        height: 26px;
        margin: 10px;
    }

    .progress-info-table .complete {
        color: green;
        font-weight: 1000;

    }

    /* .progressMonth {
        background-color: #F2DCDB;
        
    }

    .progressWeek {
        background-color: #a8beef;
    } */
    .progressMonth,
    .progressWeek {
        background-color: #FDE9D9;
    }

    .progress-info-table th {
        background-color: gray;
        color: white;
        font-weight: 1000;
        text-align: center;
    }

    .progress-info-table td {
        text-align: center;
    }

    #total_progress input {
        border: unset;
        text-align: center;
        background-color: inherit;
        display: none;
    }

    .planCSum {
        background-color: #FDE9D9;
    }

    .actionCSum {
        background-color: #FCD5B4;
    }

    .planWeek,
    .actionWeek {
        background-color: #DA9694;
    }

    .planTotal,
    .actionTotal {
        background-color: #92CDDC;
    }

    .tableHead {
        background-color: #FDE9D9;
    }

    .nowProgress {
        background-color: green;
    }

    .table thead th,
    .table td {
        vertical-align: middle;
    }
</style>


<div class="row">
    <div class="card col-md-6" style="display:inline-block">
        <div class="card-header">
            <h6 class="col-md-6">시공 진행률</h6>
            <div class="col-md-6" style="text-align: right;">
            </div>
        </div>
        <div class="card-body">
            <table class="progress-table">
                <tbody id="progress-table-sigong">

                    <tr>
                        <th colspan="2">
                            <span class="btn btn-danger">시공완료</span>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-header">
            <div style="margin-right:0.5rem;">
                <h6>상세 진행률</h6>
            </div>
            <div>
                <div class="btn-group btn-group-md btn-group-toggle " data-toggle="buttons" id="btn-group-toggle-div">

                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered progress-each table-responsive">
                <tbody>
                    <tr id="progress-each-body">
                        <th>포설</th>
                        <th>1차 다짐</th>
                        <th>2차 다짐</th>
                        <th>3차 다짐</th>
                        <th>이음부분</th>
                        <th>마무리</th>
                    </tr>
                    <tr id="progress-each-percent">

                    </tr>

                </tbody>
            </table>
        </div>
    </div>
    <div class="card col-md-6" style="display:inline-block">
        <div class="card-header">
            <h6>일정표</h6>
        </div>
        <div class="card-body">
            <?php   include("community_site_issue_calendar.php"); ?>
        </div>
    </div>
</div>

<div class="row card progress-table">
    <div class="card-header">
        <h6>전체공정률</h6>
    </div>
    <div class="card-body">
        <div class="tg-wrap">
            <table id="table-total-progress" class="table table-bordered table-responsive"
                style="text-align:center;color:black;">
                <thead id="site-detail-progress-head" style="vertical-align: middle;">

                </thead>
                <tbody id="site-detail-progress-body" style="vertical-align: middle;">

                </tbody>
            </table>
        </div>
    </div>

    <div class="card-body">
        <div class="tg-wrap">
            <table id="total_progress" class="table table-bordered table-responsive"
                style="text-align:center;color:black;">
                <thead id="pro-table-head" style="vertical-align: middle;">

                </thead>
                <tbody id="pro-table-body" style="vertical-align: middle;">

                </tbody>
            </table>
        </div>
        <script
            charset="utf-8">var TGSort = window.TGSort || function (n) { "use strict"; function r(n) { return n.length } function t(n, t) { if (n) for (var e = 0, a = r(n); a > e; ++e)t(n[e], e) } function e(n) { return n.split("").reverse().join("") } function a(n) { var e = n[0]; return t(n, function (n) { for (; !n.startsWith(e);)e = e.substring(0, r(e) - 1) }), r(e) } function o(n, r) { return -1 != n.map(r).indexOf(!0) } function u(n, r) { return function (t) { var e = ""; return t.replace(n, function (n, t, a) { return e = t.replace(r, "") + "." + (a || "").substring(1) }), l(e) } } function i(n) { var t = l(n); return !isNaN(t) && r("" + t) + 1 >= r(n) ? t : NaN } function s(n) { var e = []; return t([i, m, g], function (t) { var a; r(e) || o(a = n.map(t), isNaN) || (e = a) }), e } function c(n) { var t = s(n); if (!r(t)) { var o = a(n), u = a(n.map(e)), i = n.map(function (n) { return n.substring(o, r(n) - u) }); t = s(i) } return t } function f(n) { var r = n.map(Date.parse); return o(r, isNaN) ? [] : r } function v(n, r) { r(n), t(n.childNodes, function (n) { v(n, r) }) } function d(n) { var r, t = [], e = []; return v(n, function (n) { var a = n.nodeName; "TR" == a ? (r = [], t.push(r), e.push(n)) : ("TD" == a || "TH" == a) && r.push(n) }), [t, e] } function p(n) { if ("TABLE" == n.nodeName) { for (var e = d(n), a = e[0], o = e[1], u = r(a), i = u > 1 && r(a[0]) < r(a[1]) ? 1 : 0, s = i + 1, v = a[i], p = r(v), l = [], m = [], g = [], h = s; u > h; ++h) { for (var N = 0; p > N; ++N) { r(m) < p && m.push([]); var T = a[h][N], C = T.textContent || T.innerText || ""; m[N].push(C.trim()) } g.push(h - s) } var L = "tg-sort-asc", E = "tg-sort-desc", b = function () { for (var n = 0; p > n; ++n) { var r = v[n].classList; r.remove(L), r.remove(E), l[n] = 0 } }; t(v, function (n, t) { l[t] = 0; var e = n.classList; e.add("tg-sort-header"), n.addEventListener("click", function () { function n(n, r) { var t = d[n], e = d[r]; return t > e ? a : e > t ? -a : a * (n - r) } var a = l[t]; b(), a = 1 == a ? -1 : +!a, a && e.add(a > 0 ? L : E), l[t] = a; var i = m[t], v = function (n, r) { return a * i[n].localeCompare(i[r]) || a * (n - r) }, d = c(i); (r(d) || r(d = f(i))) && (v = n); var p = g.slice(); p.sort(v); for (var h = null, N = s; u > N; ++N)h = o[N].parentNode, h.removeChild(o[N]); for (var N = s; u > N; ++N)h.appendChild(o[s + p[N - s]]) }) }) } } var l = parseFloat, m = u(/^(?:\s*)([+-]?(?:\d+)(?:,\d{3})*)(\.\d*)?$/g, /,/g), g = u(/^(?:\s*)([+-]?(?:\d+)(?:\.\d{3})*)(,\d*)?$/g, /\./g); n.addEventListener("DOMContentLoaded", function () { for (var t = n.getElementsByClassName("tg"), e = 0; e < r(t); ++e)try { p(t[e]) } catch (a) { } }) }(document);</script>
    </div>
</div>


<!-- Form Modal -->
<div class="modal fade" id="addProgressModal" tabindex="-1" role="dialog" aria-labelledby="addProgressModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addProgressModalTitle"> 추가 </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group"><label for="options1"><input type="radio" name="options"
                                id="options1">보조기층</label></div>
                    <div class="form-group"><label for="options2"><input type="radio" name="options"
                                id="options2">기층</label></div>
                    <div class="form-group"><label for="options3"><input type="radio" name="options"
                                id="options3">프라임코트</label></div>
                    <div class="form-group"><label for="options4"><input type="radio" name="options"
                                id="options4">택코트</label></div>
                    <div class="form-group"><label for="options5"><input type="radio" name="options"
                                id="options5">중간층</label></div>
                    <div class="form-group"><label for="options6"><input type="radio" name="options"
                                id="options6">표층</label></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

<script>
    //api_server 테스트
    var weekLength;
    var planTdNumWeek;
    var planTdNumMonth;
    var layerCount = '0';
    var title_temp;

    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }
    var id = $.urlParam('id');


    $(document).ready(function () {

        rpc("con-list-all", "selectProgrssWeekMonth", { 'id': id }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var progHMonth;
            var proHWeek;
            var progHead;
            progHMonth += "<td colspan='5' rowspan='2'>공정표</td>";
            progHead += " <tr class='progHMonth'>";
            progHead += "<td colspan='5' rowspan='2'>공정표</td><td></td>";
            progHead += "</tr><tr class='proHWeek'></tr><tr><td rowspan='5'>공정율</td>";
            progHead += "<td rowspan='2' style='background-color: #F2DCDB'>계획</td>";
            progHead += "<td colspan='2' style='background-color: #DA9694'>주간</td>";
            progHead += "<td rowspan='2'  class='actionDevPlan'>%</td>";
            for (inx = 0; inx < 10; ++inx) {
                progHead += "<td id='planWeek" + inx + "' class='planWeek'></td>";
            }
            progHead += "</tr><tr>";
            progHead += "<td colspan='2' style='background-color: #92CDDC'>누계</td>";
            for (inx = 0; inx < 10; ++inx) {
                progHead += "<td id='planTotal" + inx + "' class='planTotal'></td>";
            }
            progHead += "</tr><tr><td rowspan='2' style='background-color: #B8CCE4'>실시</td>";
            progHead += "<td colspan='2' style='background-color: #DA9694'>주간</td><td rowspan='2'  class='actionDevPlan'>%</td>";
            for (inx = 0; inx < 10; ++inx) {
                progHead += "<td id='actionWeek" + inx + "' class='actionWeek'></td>";
            }
            progHead += "</tr><tr><td colspan='2' style='background-color: #92CDDC'>누계</td>";
            for (inx = 0; inx < 10; ++inx) {
                progHead += "<td id='actionTotal" + inx + "' class='actionTotal'></td>";
            }
            progHead += "</tr><tr><td colspan='4'>현재공정</td>";
            for (inx = 0; inx < 10; ++inx) {
                progHead += "<td id='nowProgress" + inx + "'></td>";
            }
            progHead += "</tr>";
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                progHMonth += "<td class='progressMonth' id='monthValue" + [inx] + "' value='" + item.month + "'>" + item.month + "월</td>";
                proHWeek += "<td class='progressWeek' id='weekValue" + [inx] + "' value='" + item.week + "'>" + item.week + "주차</td>";
            }
            // progHead+="<tr><td colspan='5'>현장확인</td></tr>";

            $("#site-detail-progress-head").html(progHead);
            $(".progHMonth").html(progHMonth);
            $(".proHWeek").html(proHWeek);

            weekLength = $('.progressWeek').length;
            getProgressBody();


        });

        rpc("con-list-all", "getLayerName", { 'id': id }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var sPercent;
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                sPercent += "<tr><th><span class='btn btn-primary layer" + inx + "'>" + item.layer_name + "</span></th>";
                sPercent += "<td class='progress' ><div class='progress-bar' id='progress-bar" + (inx + 1) + "'>";

                sPercent += "</div></td>";
                sPercent += "</tr>";
            }

            $("#progress-table-sigong").html(sPercent);
            startProgress(contents[0].layer_name);
        });

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;

        weekNumberByMonth(today);

    });

    function getProgressBody() {
        rpc("con-list-all", "progressAll", { 'id': id }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var prog;
            for (i = 0; i < weekLength; ++i) {
                var str = $('#monthValue' + i)[0].innerText;
                str.slice(0, -1);
            }

            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                //body
                prog += "<tr id='" + item.id + "' class='layer_id" + item.layer_id + "'>";
                prog += "<td rowspan='2' class='procate " + item.layer_name + " merge-target' data-name='" + item.layer_name + "' id='procate" + inx + "'>" + item.layer_name + "</td>";
                prog += "<td rowspan='2' class='protype'>" + item.process_type + "</td>";
                prog += "<td class='proplan'>계획</td>";
                prog += "<td>" + item.date_count + "</td>";
                prog += "<td rowspan='2' class='propercent " + item.layer_name + "' id='propercent" + inx + "'>" + ((item.real_date_count / item.date_count) * 100).toFixed(1) + "%</td>";

                var progressMonthLength = $('.progressMonth').length;

                for (i = 1; i <= progressMonthLength; ++i) {
                    prog += "<td class='planTdNum" + i + " " + ((parseInt(item.week_num) == i) ? "planCSum" : "") + "'>" + ((parseInt(item.week_num) == i) ? item.date_unit : "") + "</td>";
                }

                prog += "</tr>";
                prog += "<tr>";
                prog += "<td>실시</td>";
                prog += "<td>" + item.real_date_count + "</td>";

                for (i = 1; i <= progressMonthLength; ++i) {
                    prog += "<td class='actionTdNum" + i + " " + ((parseInt(item.week_num) == i) ? "actionCSum" : "") + "'>" + ((parseInt(item.real_week_num) == i) ? item.real_date_unit : "") + "</td>";
                }

                prog += "</tr>";

                layerCount = item.layer_id;
                title_temp = item.layer_name;
            }

            $("#site-detail-progress-body").html(prog);


            //공정률-계획-주간
            for (inx = 0; inx < 10; ++inx) {
                var planTdNum = $('#table-total-progress .planTdNum' + (inx + 1) + '').text();
                var pp = new Array;
                var addAllTd = 0;
                for (i = 0; i < planTdNum.length; ++i) {
                    pp = Number(planTdNum[i]);

                    addAllTd += pp;
                }
                $('#table-total-progress #planWeek' + inx + '').html(addAllTd);
            }
            //공정률-계획-누계
            var addPlanNum = 0;
            for (var i = 0; i < weekLength; ++i) {
                addPlanNum += Number($('#planWeek' + i).html());
                $("#planTotal" + i).html(addPlanNum);
            }
            //공정률-실시-주간
            for (inx = 0; inx < 10; ++inx) {
                var actionTdNum = $('#table-total-progress .actionTdNum' + (inx + 1) + '').text();
                var ap = new Array;
                var addAllTd = 0;
                for (i = 0; i < actionTdNum.length; ++i) {
                    ap = Number(actionTdNum[i]);
                    addAllTd += ap;
                }
                $('#table-total-progress #actionWeek' + inx + '').html(addAllTd);
            }
            //공정률-실시-누계
            var addActionNum = 0;
            for (var i = 0; i < weekLength; ++i) {
                addActionNum += Number($('#actionWeek' + i).html());
                $("#actionTotal" + i).html(addActionNum);
            }

            //현재공정 nowProgress
            var nowProgressEndIndex = 0;
            for (var i = 0; i < weekLength; i++) {
                planTotal = ($('#planTotal' + i)[0]).innerText;
                actionTotal = $('#actionTotal' + i)[0].innerText;
                if (planTotal == actionTotal) {
                    $("#nowProgress" + i).addClass('nowProgress');
                    nowProgressEndIndex = i;
                } else {
                }
            }

            var planTotalArray = new Array();
            planTotalArray = $("#site-detail-progress-head .planTotal");
            planTotalArray2 = parseFloat(planTotalArray[planTotalArray.length - 1].textContent);
            planTotalArray3 = parseFloat(planTotalArray[nowProgressEndIndex].textContent);
            actionDevPlan = parseInt((planTotalArray3 / planTotalArray2) * 100) + '%';
            $(".actionDevPlan").html(actionDevPlan);

            //상세진행률 퍼센티지 채우기
            var sum = 0;
            var avgLayerP = 0;
            var sPercent;
            var tPercent = 0;
            for (i = 1; i <= layerCount; ++i) {
                sum = 0;
                avgLayerP = [];
                var trLayer = $('.layer_id' + i);
                for (inx = 0; inx < trLayer.length; ++inx) {
                    parseInt((trLayer[inx].cells[4].innerText).slice(0, -3));
                    sum += parseFloat((trLayer[inx].cells[4].innerText).slice(0, -3));
                }
                avgLayerP = sum / trLayer.length;
                sPercent += "<div class='progress-bar' role='progressbar' aria-valuenow='0'";
                sPercent += "aria-valuemin='0' aria-valuemax='100' style='" + avgLayerP + "%'>" + avgLayerP + "</div>";
                $('#progress-bar' + i).css('width', avgLayerP + '%');
                if (avgLayerP == 100) {
                    $('#progress-bar' + i).css('background-color', 'green');
                } else {
                    $('#progress-bar' + i).css('background-color', 'rgb(236, 34, 13)');
                }
                parseInt(avgLayerP);
                avgLayerP = avgLayerP.toFixed(2);

                $('#progress-bar' + i).html(avgLayerP + '%');
                $('#taskOrder #summary' + i + ' td:nth-child(2)').html(avgLayerP + '%');
                tPercent += parseInt(avgLayerP)
            }
            var divide = $('#taskOrder tr').length - 1;
            tPercent = tPercent / divide;
            tPercent = tPercent.toFixed(2);

            $('#total_summary').html(tPercent + '%');
            mergeRows();
        });

    }

    function startProgress(layer_name) {
        rpc("con-list-all", "getLayerName", { 'id': id }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var progress_each_title = '';
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                progress_each_title += "<label onclick='detailProgressRadioGroup(this);' value='" + item.layer_name + "' class='btn btn-primary detail-progress-radio-group' id='" + item.layer_id + "'>";
                progress_each_title += "<input type='radio' name='options' id='" + item.layer_name + "' autocomplete='off' checked>";
                progress_each_title += "" + item.layer_name + "</label>";
            }
            $("#btn-group-toggle-div").html(progress_each_title);
        });

        rpc("con-list-all", "getProcessByLayerName", { 'id': id, 'layer_name': layer_name }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var progress_each_th = '';
            var progress_each_td = '';

            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                progress_each_th += "<th>" + item.process_type + "</th>";

                var properA = $('.' + item.layer_name + '')[inx + 1].innerText;
                progress_each_td += "<td id='properA" + inx + "'>" + properA + "</td>";
            }
            $("#progress-each-body").html(progress_each_th);
            $("#progress-each-percent").html(progress_each_td);
        });
    }

    function detailProgressRadioGroup(obj) {
        var layer_name = $(obj)[0].innerText;
        startProgress(layer_name);

    }

    function mergeRows() {
        var names = new Set();
        $(".merge-target").each(function () {
            names.add($(this).text());
        })

        for (var name of names) {
            var rows = $(".merge-target[data-name=" + name + "]");
            var len = 0;
            rows.each(function () {
                len += parseInt($(this).attr('rowspan'));
            })

            if (len > 1) {
                rows.eq(0).attr("rowspan", len);
                rows.not(":eq(0)").remove();
            }
        }
    }

    function weekNumberByMonth(dateFormat) {
        console.log(dateFormat);
        var inputDate = new Date(dateFormat);

        // 인풋의 년, 월
        let year = inputDate.getFullYear();
        let month = inputDate.getMonth() + 1;

        // 목요일 기준 주차 구하기
        var weekNumberByThurFnc = (paramDate) => {
            var year = paramDate.getFullYear();
            var month = paramDate.getMonth();
            var date = paramDate.getDate();

            // 인풋한 달의 첫 날과 마지막 날의 요일
            var firstDate = new Date(year, month, 1);
            var lastDate = new Date(year, month + 1, 0);
            var firstDayOfWeek = firstDate.getDay() === 0 ? 7 : firstDate.getDay();
            var lastDayOfweek = lastDate.getDay();

            // 인풋한 달의 마지막 일
            var lastDay = lastDate.getDate();

            // 첫 날의 요일이 금, 토, 일요일 이라면 true
            var firstWeekCheck = firstDayOfWeek === 5 || firstDayOfWeek === 6 || firstDayOfWeek === 7;
            // 마지막 날의 요일이 월, 화, 수라면 true
            var lastWeekCheck = lastDayOfweek === 1 || lastDayOfweek === 2 || lastDayOfweek === 3;

            // 해당 달이 총 몇주까지 있는지
            var lastWeekNo = Math.ceil((firstDayOfWeek - 1 + lastDay) / 7);

            // 날짜 기준으로 몇주차 인지
            let weekNo = Math.ceil((firstDayOfWeek - 1 + date) / 7);

            // 인풋한 날짜가 첫 주에 있고 첫 날이 월, 화, 수로 시작한다면 'prev'(전달 마지막 주)
            if (weekNo === 1 && firstWeekCheck) weekNo = 'prev';
            // 인풋한 날짜가 마지막 주에 있고 마지막 날이 월, 화, 수로 끝난다면 'next'(다음달 첫 주)
            else if (weekNo === lastWeekNo && lastWeekCheck) weekNo = 'next';
            // 인풋한 날짜의 첫 주는 아니지만 첫날이 월, 화 수로 시작하면 -1;
            else if (firstWeekCheck) weekNo = weekNo - 1;

            console.log(weekNo);
            // // return weekNo;
            // // for 
            // var monthV = weekNo-1;
            // console.log(monthV);

            // console.log($('#weekValue3'+monthV));
            // $('#weekValue3'+monthV).css('background-color','red');

        };

        // 목요일 기준의 주차
        let weekNo = weekNumberByThurFnc(inputDate);

        // 이전달의 마지막 주차일 떄
        if (weekNo === 'prev') {
            // 이전 달의 마지막날
            var afterDate = new Date(year, month - 1, 0);
            year = month === 1 ? year - 1 : year;
            month = month === 1 ? 12 : month - 1;
            weekNo = weekNumberByThurFnc(afterDate);
        }
        // 다음달의 첫 주차일 때
        if (weekNo === 'next') {
            year = month === 12 ? year + 1 : year;
            month = month === 12 ? 1 : month + 1;
            weekNo = 1;
        }

        return { year, month, weekNo };
    }
</script>