<style>
    .document-preview-frame {
        width: 100%;
        height: 500px;
    }

    #td-history-num, #td-history-title, #td-history-content {
        vertical-align: middle;
    }

    .button-td {
        width: 70px;
    }
</style>

<div class="row">
    <table class="table table-bordered site-docu-all table-responsive">
        <thead>
            <tr class="bg-light text-center">
            <th class="preConst-list-num">번호</th>
                <th class="preConst-list-title">제목</th>
                <th class="preConst-list-content">요약</th>
                <th class="preConst-list-fileName">첨부파일명</th>
                <th class="preConst-list-preview">미리보기</th>
                <th class="preConst-list-download">다운로드</th>
            </tr>
        </thead>
        <tbody id="krecon_document" style="text-align:center;">
            <tr class="text-center">
                <td>1</td>
                <td>설계도면</td>
                <td>설계도면(도로대장)</td>
                <td>설계도면.pdf</td>
                <td><button class='btn btn-primary btn-sm document-preview-btn' data-file='/pages/uploads/설계도면.pdf' onclick='clickPreview(this);'>미리보기</td>
                <td class='button-td'><a class='btn btn-success btn-sm document-download-btn' onclick='clickDownload(this);'data-file='/pages/uploads/설계도면.pdf'>다운로드</a></td>
            </tr>
            <tr class="text-center">
                <td>2</td>
                <td>도로확장</td>
                <td>도로확장공사이력정보</td>
                <td>도로확장공사이력.pdf</td>
                <td><button class='btn btn-primary btn-sm document-preview-btn' data-file='/pages/uploads/도로확장공사이력.pdf' onclick='clickPreview(this);'>미리보기</td>
                <td class='button-td'><a class='btn btn-success btn-sm document-download-btn' onclick='clickDownload(this);'data-file='/pages/uploads/도로확장공사이력.pdf'>다운로드</a></td>
            </tr>
            <tr class="text-center">
                <td id="td-history-num" rowspan="2">3</td>
                <td id="td-history-title" rowspan="2">기타 도면</td>
                <td id="td-history-content" rowspan="2">현황측량도 및 현황실측평면도</td>
                <td class="td-fileName">현황측량도.jpg</td>
                <td><button class='btn btn-primary btn-sm document-preview-btn' onclick='clickPreview("현황측량도.jpg");'>미리보기</td>
                <td><a class='btn btn-success btn-sm document-download-btn' href="/pages/uploads/현황측량도.jpg" download>다운로드</td>
            </tr>
            <tr class="text-center">
                <td class="td-fileName">현황실측평면도.jpg</td>
                <td><button class='btn btn-primary btn-sm document-preview-btn' onclick='clickPreview("현황실측평면도.jpg");'>미리보기</td>
                <td><a class='btn btn-success btn-sm document-download-btn' href="/pages/uploads/현황실측평면도.jpg" download>다운로드</td>
            </tr>
        </tbody>
    </table>
</div>
<!-- <div id="document-preview-img-div">
</div>
<div id="document-preview-div">
</div> -->