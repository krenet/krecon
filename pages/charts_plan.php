<script src="assets/plugins/charts/Chart.min.js"></script>  

    <div id="container" style="width: 100%;">    
		<canvas id="progress-canvas"></canvas>
	</div>

	<script>        		
		var color = Chart.helpers.color;
        
		var chartData = {
			labels: ['1월', '2월', '3월', '4월', '5월', '6월','7월','8월','9월','10월','11월','12월'],
			datasets: [{
				label: '계약',
				backgroundColor: "rgba(0,255,0,0.5)",
				borderColor: color.green,
				borderWidth: 1,
				data: [
					2000,
					4000,
					2500,
					1680,
					1400,
                    2800,
                    0,
                    0,
                    0,
                    1800,
                    1000,
                    0,
				]
			}, {
				label: '시공',
				backgroundColor: "rgba(0,0,255,0.5)",
				borderColor: "rgba(0,0,255,1)",
				data: [
					0,
					0,
					530,
					1480,
					3210,
                    4000,
                    1500,
                    1200,
                    3500,
                    2500,
                    1000,
                    0
				]
			},{
				label: '준공',
				backgroundColor: "rgba(255,0,0,0.5)",
				borderColor: "rgba(255,0,0,1)",
				data: [
					0,
					0,
					0,
					530,
					1210,
                    2000,
                    2200,
                    3200,
                    3000,
                    2000,
                    2800,
                    1900,                    
				]
			}]

		};

		window.onload = function() {
			var ctx = document.getElementById('progress-canvas').getContext('2d');
			window.myHorizontalBar = new Chart(ctx, {
				type: 'bar',
				data: chartData,
				options: {
					// Elements options apply to all of the options unless overridden in a dataset
					// In this case, we are setting the border of each horizontal bar to be 2px wide
					elements: {
						rectangle: {
							borderWidth: 2,
						}
					},
					responsive: true,
					legend: {
						position: 'right',
					},
					title: {
						display: true,
						text: '최근 5년 월단위 수익 통계'
					},
					scales: {
            yAxes: [{
                ticks: {
                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                        return  numberWithCommas(value)+" 백만원";
                    }
                }
            }]
        	},
					tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    label += numberWithCommas(Math.round(tooltipItem.yLabel * 100) / 100);
                    return label;
                }
						}
					}
				}
			});

		};

	
		function numberWithCommas(n) {
			var parts=n.toString().split(".");
			return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
		}
	</script>