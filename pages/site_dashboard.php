<style>
    .table-dark-head th {
        color: white;
        padding: 0.2rem;
    }

    .table td {
        padding: 0.5rem;
    }

    .dash-1st-row .card-body {
        min-height: 270px;
        max-height: 270px;
    }

    .card-default {
        margin-bottom: unset !important;
    }
</style>
<script>
    $(document).ready(function () {
        var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = yyyy + '-' + mm + '-' + dd;
        rpc("issue", "listLimitFour", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            // console.log(data);
            var contents = data.contents;
            var item;
            var issue;
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                issue += "<tr  onclick='moveIssueDetail("+item.con_code+")' style='cursor:pointer'>";
                issue += "<td>" + item.con_area_name + "</td>";
                issue += "<td>" + item.title + "</td>";
                issue += "</tr>";
            }
            $("#cons-history-table-body").html(issue);

        });

        rpc("advice", "contentsLimitSix", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var adviseList;
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                adviseList += "<tr>";
                adviseList += "<td>" + item.con_area_name + "</td>";
                adviseList += "<td>" + item.title + "</td>";
                adviseList += "<td>" + item.name + "</td>";
                adviseList += "<td class='button-td'><button class='btn btn-success btn-sm' onclick='remote_advice(" + item.id + ");'>자세히</button></td>";
                adviseList += "</tr>";
            }
            $("#advice-list").html(adviseList);

        });
        rpc("adviser", "birthList", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var birthList;
            var birthListNone;
            var birthM;
            var birthMD;
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                birthM = item.birthday;
                birthMD = birthM.substring(5,7)+'월 '+birthM.substring(8,10)+'일';
                birthM = birthM.substring(5, 7); 
                
                if (birthM == mm) {
                    birthList += "<tr>";
                    birthList += "<td>" + item.affiliation + "</td>";
                    birthList += "<td>" + item.name + "</td>";
                    birthList += "<td>" + birthMD + "</td>";
                    birthList += "<td>" + item.mail + "</td>";
                    birthList += "<td>" + item.phone + "</td>";
                    birthList += "</tr>";
                } 
                // else {
                //     birthListNone += "<tr><td colspan='4'>이달의 생일자가 없습니다.</td></tr>"
                // }
            }
            $("#birth-list").html(birthList);
            // $("#birth-list").html(birthListNone);

        });
        rpc("con-list-all", "getWeather", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var liveScene;
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                liveScene += "<tr id='"+item.con_code+"' onclick='moveDetail("+item.con_code+")' style='cursor:pointer'>";
                liveScene += "<td>"+item.con_area_name+"</td>";
                liveScene += "<td>"+item.ambient_temp+"&deg;C</td>";
                liveScene += "</tr>";
            }
           
            $("#cons-weather").html(liveScene);
            
        });
        rpc("con-list-all", "getEndDate", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var dday;
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                var strDate1 = today;
                var strDate2 = item.con_goal_end_date;
                // console.log(strDate1);
                // console.log(strDate2);
                var arr1 = strDate1.split('-');
                var arr2 = strDate2.split('-');
                // console.log(arr1);
                // console.log(arr2);
                var dat1 = new Date (arr1[0], arr1[1], arr1[2]);
                var dat2 = new Date (arr2[0], arr2[1], arr2[2]);
                // console.log(dat1);
                // console.log(dat2);
                var diff = dat2 - dat1;
                var currDay = 24 * 60 * 60 * 1000;// 시 * 분 * 초 * 밀리세컨
                var currMonth = currDay * 30;// 월 만듬
                var currYear = currMonth * 12; // 년 만듬
                // document.write("* 날짜 두개 : " + strDate1 + ", " + strDate2 + "<br/>");
                // document.write("* 일수 차이 : " + parseInt(diff/currDay) + " 일<br/>");
                // document.write("* 월수 차이 : " + parseInt(diff/currMonth) + " 월<br/>");
                // document.write("* 년수 차이 : " + parseInt(diff/currYear) + " 년<br/><br/>");

                dday += "<tr onclick='moveDetail("+item.con_code+")' style='cursor:pointer'>";
                dday += "<td>"+item.con_area_name+"</td>";
                dday += "<td>"+item.con_goal_end_date+"</td>";
                dday += "<td> -"+parseInt(diff/currDay)+"</td>";
                dday += "</tr>";
            }
            $("#cons-dday").html(dday);
        });

        rpc("con-list-all", "selectProgrssWeekMonthInDash", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var progressList;
            console.log(contents);
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                var progressP = 0;
                progressP = Number(item.real_date)/Number(item.date);

                progressP = progressP.toFixed(4);
                console.log(progressP);
                progressList += "<tr id='"+item.con_code+"' onclick='moveDetail("+item.con_code+")' style='cursor:pointer'>";
                progressList += "<td>"+item.con_area_name+"</td>";
                progressList += "<td>"+(progressP)*100+"%</td>";
                progressList += "</tr>";
            }
            $("#cons-progress").html(progressList);
        });
    });
    function remote_advice(id) {
        location.href = "http://localhost/?cat=community&page=community_advice_board&id=" + id;

    }
    function moveDetail(id){
        // location.href=".?cat=site&page=site_detail&id="+id;
        location.href=".?cat=site&page=site_detail&id=3169";

    }
    function moveIssueDetail(id){
        location.href=".?cat=site&page=site_detail&id="+id+"#icon-issue";
    }
</script>


<div class="row dash-1st-row">
    <div class="col-xl-3 col-sm-6">
        <div class="card card-mini mb-4">
            <div class="card-body">
                <h2 class="mb-1">현재 현장 날씨</h2>
                <table class="table table-bordered noselect table-responsive">
                    <thead class="table-dark table-dark-head">
                        <tr>
                            <!-- <th scope="col">지역명</th> -->
                            <th scope="col">현장명</th>
                            <th scope="col">날씨</th>
                        </tr>
                    </thead>
                    <tbody id="cons-weather">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-sm-6">
        <div class="card card-mini mb-4">
            <div class="card-body">
            <h2 class="mb-1">현장진행률</h2>
               <table class="table table-bordered noselect table-responsive">
                    <thead class="table-dark table-dark-head">
                        <tr>
                            <!-- <th scope="col">지역명</th> -->
                            <th scope="col">현장명</th>
                            <!-- <th scope="col">계획률</th> -->
                            <th scope="col">진행률</th>
                        </tr>
                    </thead>
                    <tbody id="cons-progress">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-sm-6">
        <div class="card card-mini mb-4">
            <div class="card-body">
                <h2 class="mb-1">D-day</h2>
               <table class="table table-bordered noselect table-responsive">
                    <thead class="table-dark table-dark-head">
                        <tr>
                            <!-- <th scope="col">지역명</th> -->
                            <th scope="col">현장명</th>
                            <th scope="col">목표</th>
                            <th scope="col">D-day</th>
                        </tr>
                    </thead>
                    <tbody id="cons-dday">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-sm-6">
        <div class="card card-mini  mb-4">
            <div class="card-body">
                <h2 class="mb-1">이슈 발생 리스트</h2>
                <table class="table table-bordered noselect  table-responsive">
                    <thead class="table-dark table-dark-head">
                        <tr>
                            <!-- <th scope="col">지역명</th> -->
                            <th scope="col">현장명</th>
                            <th scope="col">이슈</th>
                        </tr>
                    </thead>
                    <tbody id="cons-history-table-body">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <!-- <div class="col-xl-8 col-md-12">
                    <div class="card card-default" data-scroll-height="500">
                        <div class="card-body" style="padding-top: 1rem;">
                        <h2 class="mb-1">전체 일정</h2>
                            <?php  //include("dashboard_calendar.php"); ?>
                            
                        </div>
                    </div>
                </div> -->
    <div class="col-xl-6 col-md-12">
        <div class="card card-default">
            <div class="card-body" style="padding-top: 1rem;">
                <h2 class="mb-1">자문 리스트</h2>
                <table class="table table-bordered table-responsive">
                    <thead class="table-dark table-dark-head">
                        <tr>
                            <th>공사명</th>
                            <th>자문제목</th>
                            <th>참여위원</th>
                            <th>게시판</th>
                        </tr>
                    </thead>
                    <tbody id="advice-list">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-md-12">
        <div class="card card-default" style='height: 100%;'>
            <div class="card-body" style="padding-top: 1rem;">
                <h2 class="mb-1">이달의 생일</h2>
                <table class="table table-bordered table-responsive">
                    <thead class="table-dark table-dark-head">
                        <tr>
                            <!-- <th>번호</th> -->
                            <th>소속</th>
                            <th>이름</th>
                            <th>생일</th>
                            <th>메일</th>
                            <th>번호</th>
                        </tr>
                    </thead>
                    <tbody id="birth-list">

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>