<style>
    .table-dark-head th {
        color: white;
        text-align: center;
    }

    #cons-history-table-is-live td {
        text-align: center;
    }

    #cons-history-table-is-live tr:hover,
    .cons-history-table-is-live-hovered {
        background: #7DBCFF;
        color: white;
        cursor: pointer;
    }

    .noselect {
        -webkit-touch-callout: none;
        /* iOS Safari */
        -webkit-user-select: none;
        /* Safari */
        -khtml-user-select: none;
        /* Konqueror HTML */
        -moz-user-select: none;
        /* Firefox */
        -ms-user-select: none;
        /* Internet Explorer/Edge */
        user-select: none;
        /* Non-prefixed version, currently
                                                    supported by Chrome and Opera */
    }
</style>
<!-- <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=9160d0178961d9909b326b6f75f2031e"></script> -->
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=9160d0178961d9909b326b6f75f2031e"></script>
<script>
    // //<![CDATA[
        var defaultImageSrc = "http://t1.daumcdn.net/localimg/localimages/07/mapjsapi/default_marker.png";
    var liveImageSrc = "http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/markerStar.png";
    var selectedImageSrc = "img/pin-selected.gif";
    // 마커 이미지의 이미지 크기 입니다
    var defaultImageSize = new daum.maps.Size(35,36);
    var liveImageSize = new daum.maps.Size(24, 35);     
    var selectedImageSize = new daum.maps.Size(35, 35);    
    // 마커 이미지를 생성합니다    
    var defaultMarkerImage = new daum.maps.MarkerImage(defaultImageSrc,defaultImageSize);
    var liveMarkerImage = new daum.maps.MarkerImage(liveImageSrc, liveImageSize); 
    var selectedMarkerImage = new daum.maps.MarkerImage(selectedImageSrc, selectedImageSize); 

    //상세 페이지로 이동
    function moveDetail(id) {
        // function moveDetail(id,isLive) {
        console.log('이건뭐야');
        location.href = ".?cat=site&page=site_detail&id=" + id;

    }
    var marker;
            // //        ]]>
</script>


<div class="row">
    <div class="col-xl-6 col-md-12">
        <div class="card card-default" data-scroll-height="675">
            <div class="card-header">
                <h2>시공 이력 지도</h2>
            </div>
            <div class="card-body">
                <div id="mapSiteListAll" style="width:100%;height:400px;"></div>
            </div>
            <div class="card-footer d-flex flex-wrap bg-white p-0">
                <div class="col-6 px-0">
                    <div class="text-center p-4">
                        <h4 class="total-cons-count"></h4>
                        <p class="mt-2">총 시공 이력</p>
                    </div>
                </div>
                <div class="col-6 px-0">
                    <div class="col-12 px-0">
                        <div class="text-center p-4 border-left">
                            <h4 class="live-cons-count"></h4>
                            <p class="mt-2">현재 시공 중</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-6 col-md-12">
        <div class="card card-default" data-scroll-height="675">
            <div class="card-header">
                <h2>시공 이력 리스트</h2>
            </div>
            <div class="card-body" style="height:500px;overflow:auto;">
                <table class="table table-bordered table-hover table-responsive" style="text-align:center">
                    <thead class="table-dark table-dark-head">
                        <tr>
                            <th scope="col">지역명</th>
                            <th scope="col">현장명</th>
                            <th scope="col">시공일</th>
                            <th scope="col">준공일</th>
                            <th scope="col">현장소장</th>
                        </tr>
                    </thead>
                    <tbody id="cons-history-table-body">

                    </tbody>
                </table>
            </div>
            <div class="card-footer d-flex flex-wrap bg-white p-0">
                <div class="col-6 px-0">
                    <div class="text-center p-4">
                        <h4 class="total-cons-count"></h4>
                        <p class="mt-2">총 시공 이력</p>
                    </div>
                </div>
                <div class="col-6 px-0">
                    <div class="text-center p-4 border-left">
                        <h4 class="live-cons-count"></h4>
                        <p class="mt-2">현재 시공 중</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        // //지도 객체를 담을 변수
        var map;
        var conGps = new Array();
        var MarkerId = new Array();
        var bounds = new daum.maps.LatLngBounds();
        rpc("con-list-all", "contents", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var cons_tr_list;
            var liveCount = 0;
            conGps = [];
            var con_lat;
            var con_long;
            var marker;
            var selectedMarker = null;
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                cons_tr_list += "<tr class='cons-history-table-row' onmouseenter='mouseoverTr(this);' data-cons-id=" + item.con_code + " onclick='moveDetail(" + item.con_code + ");'><td>" + item.con_field_name + "</td>";
                cons_tr_list += "<td>" + item.con_area_name + "</td>";
                cons_tr_list += "<td>" + item.con_goal_start_date + "</td>";
                cons_tr_list += "<td>" + (item.con_actuality_end_date == null ? '-' : item.con_actuality_end_date) + "</td>";
                cons_tr_list += "<td>" + item.con_field_manager + "</td></tr>";
                cons_tr_list += "<input class='con_gps' type='hidden' value= new daum.maps.LatLng(" + item.con_lat + "," + item.con_long + ")/>";
                cons_tr_list += "<input class='islive' type='hidden' value=" + item.is_live + "/>";
                // cons_list_db[inx] = ({ id: item.con_code, sdate: item.con_goal_start_date, edate: item.con_actuality_end_date, region: item.con_field_name, name: item.con_area_name, latlng: new daum.maps.LatLng(item.con_lat, item.con_long), manager: item.con_field_manager, islive: item.is_live });

                conGps.push({ title: item.con_area_name, latlng: new kakao.maps.LatLng(item.con_lat, item.con_long) });
                MarkerId.push({ id: item.con_code });
            }
            // cons_list = cons_list_db;
            console.log(conGps);
            // console.log(cons_list);
            $(".total-cons-count").text(contents.length);
            $("#cons-history-table-body").html(cons_tr_list);

            var mapContainer = document.getElementById('mapSiteListAll'), // 지도를 표시할 div 
                mapOption = {
                    center: new daum.maps.LatLng(37.552715, 126.891457), // 지도의 중심좌표
                    level: 7 // 지도의 확대 레벨
                };
            var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

            var positions = conGps;
            // console.log(positions);

            for (var i = 0; i < positions.length; i++) {
                item = positions[inx];
                console.log(MarkerId);

                // 마커를 생성합니다
                marker = new kakao.maps.Marker({
                    map: map, // 마커를 표시할 지도
                    position: positions[i].latlng, // 마커를 표시할 위치
                    title: positions[i].title, // 마커의 타이틀, 마커에 마우스를 올리면 타이틀이 표시됩니다
                    // image: markerImage // 마커 이미지 
                });
                marker.id = MarkerId[i].id;
                id=marker.id;
                // console.log(marker.id);
                marker.setMap(map);

                addMarkerActions(marker,id);
                bounds.extend(positions[i].latlng);
                // console.log(positions[i].latlng);
                console.log(data.contents[i].is_live,'dddddddddddddddddddddddddddd')

                if (data.contents[i].is_live=="Y") {
                    console.log('라이브----------------------------');
                    marker.setImage(liveMarkerImage);
                    ++liveCount;
                } else {
                    console.log('라이브아닐경우---------------------------');
                    marker.setImage(defaultMarkerImage);
                }

                kakao.maps.event.addListener(marker, 'mouseover', function() {

                // 클릭된 마커가 없고, mouseover된 마커가 클릭된 마커가 아니면
                // 마커의 이미지를 오버 이미지로 변경합니다
                if (!selectedMarker || selectedMarker !== marker) {
                    marker.setImage(overImage);
                }
                });

                // 마커에 mouseout 이벤트를 등록합니다
                    kakao.maps.event.addListener(marker, 'mouseout', function() {

                // 클릭된 마커가 없고, mouseout된 마커가 클릭된 마커가 아니면
                // 마커의 이미지를 기본 이미지로 변경합니다
                if (!selectedMarker || selectedMarker !== marker) {
                    marker.setImage(normalImage);
                }
                });
            }
            map.setBounds(bounds);

            

        });

        //테이블에 마우스 오버시 해당 마커 강조

        // $(".cons-history-table-row").mouseout(function () {
        //     var s_item = cons_id_list[$(this).data('cons-id')];
        //     s_item.marker.setMap(null);
        //     if (s_item.islive) {
        //         s_item.marker.setImage(liveMarkerImage);
        //     } else {
        //         s_item.marker.setImage(defaultMarkerImage);
        //     }

        //     s_item.marker.setMap(map);
        // });



    });

    var cons_id="";

    function mouseoverTr(obj) {
        marker.getMap();
        console.log(marker.getMap());
        cons_id=$(obj).data('consId');
        console.log(cons_id);

        cons_id.marker.setMap(null);
        cons_id.marker.setImage(selectedMarkerImage);
        cons_id.marker.setMap(map);
    }

    //마커에 마우스 이벤트 등록                    
    function addMarkerActions(marker,id){
        
        daum.maps.event.addListener(marker, 'mouseover', function() {  
            console.log('ddddddd',marker,id);
            $(".cons-history-table-row[data-cons-id="+id+"]").addClass("cons-history-table-body-hovered");
        });
        
        daum.maps.event.addListener(marker, 'mouseout', function() {                        
            $(".cons-history-table-row[data-cons-id="+id+"]").removeClass("cons-history-table-body-hovered");
        });

        daum.maps.event.addListener(marker, 'click', function() {                        
            moveDetail(id);
        });
        }
    rpc("con-list-all", "contentIsLive", null, function (data) {
        if (data.result != "ok") {
            alert("통신에 문제가 있습니다.");
            return;
        }
        var contents = data.contents;
        $(".live-cons-count").text(contents.length);
    });




</script>