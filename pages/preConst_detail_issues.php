<style>
    .document-preview-frame {
        width: 100%;
        height: 500px;
    }

    .button-td {
        width: 70px;
    }
</style>

<div class="row">
    <table class="table table-bordered site-docu-all table-responsive">
        <thead>
            <tr class="bg-light text-center">
                <th class="preConst-list-num">번호</th>
                <th class="preConst-list-title">제목</th>
                <th class="preConst-list-content">요약</th>
                <th class="preConst-list-fileName">첨부파일명</th>
                <th class="preConst-list-preview">미리보기</th>
                <th class="preConst-list-download">다운로드</th>
            </tr>
        </thead>
        <tbody id="krecon_document" style="text-align:center;">
            <tr class="text-center">
                <td>1</td>
                <td>민원발생</td>
                <td>인근 지역 주민들의 소음 민원 발생</td>
                <td>-</td>
                <td></td>
                <td></td>
                <!-- <td><button class='btn btn-primary btn-sm document-preview-btn' onclick='clickPreview(this);'>미리보기</td>
                <td><a class='btn btn-success btn-sm document-download-btn' onclick='clickDownload(this);'>다운로드</td> -->
            </tr>
            <tr class="text-center">
                <td>2</td>
                <td>인근환경주의</td>
                <td>어린이보호구역으로 공사 진행 시 각별한 주의 요망</td>
                <td>-</td>
                <td></td>
                <td></td>
                <!-- <td><button class='btn btn-primary btn-sm document-preview-btn' onclick='clickPreview(this);'>미리보기</td>
                <td><a class='btn btn-success btn-sm document-download-btn' onclick='clickDownload(this);'>다운로드</td> -->
            </tr>
        </tbody>
    </table>


</div>
<div id="document-preview-div">
</div>

<script>
      function clickPreview(obj) {
        // var file = $(obj)[0].dataset.file;
        // $("#document-preview-div").html('<iframe class="document-preview-frame" src="pdf_viewer/web/viewer.html?file=' + file + '" ></iframe>');
    }

    function clickDownload(obj) {
        // var file = $(obj)[0].dataset.file;
        // $(obj).attr({ target: '_blank', href: file });
        // $('#download-frame').attr("src", file);
    }
</script>