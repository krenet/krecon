<style>
    .card-list{
        width: 100%;
        /* height: 250px; */
        border: 1px solid #dad9d9;
        border-radius: 25px;
        margin: 10px;
        padding: 30px;
        display: flex;
        flex-direction: row;
        overflow: auto;
    }
    /* .search-label{
        font-size: 17pt;
        color: #585757;
        display: block;
        margin-left: 40px;
        font-weight: 600;
        flex: 3;
        margin-bottom: unset;
    }
    .result-line{
        display: flex;
        align-items: center;
        margin: 5px 0;
    }
    .search-value{
        flex:10;
    } */
    #siteSearchTitle{
        font-size: 15pt;
        font-weight: 600;
        font-family: 'Nanum Square', sans-serif;
    }
    .font-color-red{
        color: #E50150;
    }
    .search-result-title{
        font-size: 17pt;
        font-weight: 600;
        color: #18448e;
        /* flex: 7; */
    }
    .menu-summary{
        display: flex;
        flex-direction: row;
        align-items: center;
        flex: 9;
        padding: 10px;
    }
    .search-badge{
        font-weight: 600;
        margin-left: 10px;
        /* flex: 1; */
    }
    .show-site-detail{
        flex: 1;
        align-self: center;
    }
    .search-detail-content{
        margin: 5px;
        font-size: large;
        flex: 1;
    }
    .search-detail{
        display: flex;
        flex-direction: row;
        width: 90%;
        padding: 5px;
        color: black;
    }
    .font-bold{
        font-weight: bold;
    }
    .search-result-list-item{
        display: flex;
        flex-direction: column;
        flex: 9;
    }
    .detail-content-btn{
        height: 85px;
        font-size: 11pt;
        /* display: flex;
        flex-direction: column; */
    }
    .site-icon{
        display: flex;
        align-items: center;
    }
</style>
<div class="c">
    <div class="card card-default">
        <div class="card-header card-header-border-bottom">
            <h2>
                <div id="siteSearchTitle" style="display:inline-block">
                </div>
            </h2>

        </div>
        <div class="card-body" style="overflow: auto; max-height:637px;">
            <div class="card-list">
                <div class="search-result-list-item">

                    <div class="menu-summary">
                        <span class="search-result-title">성암로 포장 보수 공사</span>
                        <span class="badge badge-warning search-badge">시공 진행중</span>
                        <!-- badge badge-warning -->
                        
                    </div>
                    
                    <div class="search-detail">
                            <span class="search-detail-content"><span class="font-bold">지역명 :</span> 서울시 마포구 성암로</span>
                            <span class="search-detail-content"><span class="font-bold">공사개요 :</span> 3,500m구간 도로 보수</span>
                            <span class="search-detail-content"><span class="font-bold">금액 :</span> 37,730,000원</span>
                            <span class="search-detail-content"><span class="font-bold">일자 :</span> 2021-04-12 ~ 2021-05-01</span>
                    </div>
                </div>
                <div class="show-site-detail">
                    <button class="md-1 btn btn-primary detail-content-btn" onclick="moveDetail();"> 
                        <span class="site-icon"><i class="mdi mdi-information-outline mr-1" style="font-size:large"></i><span><span class="site-icon">시공상세보기</span>
                        <!-- 상세내용확인 -->
                    </button>
                </div>
            </div>
            
            <div class="card-list">
                <div class="search-result-list-item">

                    <div class="menu-summary">
                        <span class="search-result-title">증산로 포장 보수 공사</span>
                        <span class="badge badge-danger search-badge">시공 중지</span>
                        
                    </div>

                    <div class="search-detail">
                            <span class="search-detail-content"><span class="font-bold">지역명 :</span> 서울시 은평구</span>
                            <span class="search-detail-content"><span class="font-bold">공사개요 :</span> 2,570m구간 도로 보수</span>
                            <span class="search-detail-content"><span class="font-bold">금액 :</span> 30,150,000원</span>
                            <span class="search-detail-content"><span class="font-bold">일자 :</span> 2019-08-23 ~ 2020-09-08</span>
                    </div>
                </div>
                <div class="show-site-detail">
                    <button class="md-1 btn btn-primary detail-content-btn" onclick="moveDetail();">
                        <span class="site-icon"><i class="mdi mdi-information-outline mr-1" style="font-size:large"></i><span><span class="site-icon">시공상세보기</span>
                    </button>
                </div>
            </div>

            <div class="card-list">
                <div class="search-result-list-item">

                    <div class="menu-summary">
                        <span class="search-result-title">양화대교 포장 보수 공사</span>
                        <span class="badge badge-success search-badge">시공 완료</span>
                        <!-- badge badge-warning -->
                        
                    </div>

                    <div class="search-detail">
                            <span class="search-detail-content"><span class="font-bold">지역명 :</span> 서울시 영등포구</span>
                            <span class="search-detail-content"><span class="font-bold">공사개요 :</span> 1,850m구간 도로 보수</span>
                            <span class="search-detail-content"><span class="font-bold">금액 :</span> 25,740,000원</span>
                            <span class="search-detail-content"><span class="font-bold">일자 :</span> 2017-04-23 ~ 2019-12-25</span>
                    </div>
                </div>
                <div class="show-site-detail">
                    <button class="md-1 btn btn-primary  detail-content-btn" onclick="moveDetail();">
                        <span class="site-icon"><i class="mdi mdi-information-outline mr-1" style="font-size:large"></i><span><span class="site-icon">시공상세보기</span>
                        <!-- 상세내용확인 -->
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        var text = decodeURI($.urlParam('text'));
        var htmlStr = '\"<span class="font-color-red">'+text+'</span>\"';
        htmlStr += "에 대한 검색결과는 총 <span class='font-color-red'>3</span>건 입니다.";
        $('#siteSearchTitle').html(htmlStr);
    });

    checkNumber = (event) => {
        if(event.key === '.' 
            || event.key === '-'
            || event.key >= 0 && event.key <= 9) {
            return true;
        }
        return false;
    }

    moveDetail = () => {
        location.href=".?cat=site&page=site_detail&id=3169";
    }

    

</script>



