<div class="card card-default">
    <div class="card-header card-header-border-bottom">
        <h2>자문위원 리스트</h2>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <tbody>
                <tr class="bg-light text-center">
                    <th>번호</th>
                    <th>위원명</th>
                    <th>소속</th>
                    <th>부서</th>
                    <th>직급</th>
                    <th>분야</th>
                    <th>전화</th>
                    <th>메일</th>
                    <th>수정</th>
                    <th>삭제</th>
                </tr>
                <tr class="text-center">
                    <td>1</td>
                    <td>권수안</td>
                    <td>한국건설기술연구원</td>
                    <td>산업혁신부원장실</td>
                    <td>부원장</td>
                    <td>도로공학</td>
                    <td>000-0000-0000</td>
                    <td>aaaa@kict.re.kr</td>                    
                    <td class="button-td"><button class="btn btn-success btn-sm modify-btn" data-id="0001">수정</button></td>
                    <td class="button-td"><button class="btn btn-danger btn-sm remove-btn" data-id="0001">삭제</button>
                    </td>
                </tr>
                <tr class="text-center">
                    <td>2</td>
                    <td>최준성</td>
                    <td>인덕대학교</td>
                    <td>-</td>
                    <td>교수</td>
                    <td>지반및 도로공학</td>
                    <td>02-950-7565</td>
                    <td>soilpave@induk.ac.kr</td>                    
                    <td class="button-td"><button class="btn btn-success btn-sm modify-btn" data-id="0001">수정</button></td>
                    <td class="button-td"><button class="btn btn-danger btn-sm remove-btn" data-id="0001">삭제</button>
                    </td>
                </tr> 
                <tr class="text-center">
                    <td>3</td>
                    <td>이상염</td>
                    <td>인덕대학교</td>
                    <td>건설안전공학과</td>
                    <td>교수</td>
                    <td>도로공학</td>
                    <td>02-950-7587</td>
                    <td>-</td>                    
                    <td class="button-td"><button class="btn btn-success btn-sm modify-btn" data-id="0001">수정</button></td>
                    <td class="button-td"><button class="btn btn-danger btn-sm remove-btn" data-id="0001">삭제</button>
                    </td>
                </tr>    
                <tr class="text-center">
                    <td>4</td>
                    <td>이준휘</td>
                    <td>크레넷</td>
                    <td>개발팀</td>
                    <td>팀장</td>
                    <td>컴퓨터공학</td>
                    <td>010-8643-9391</td>
                    <td>eroica@krenet.co.kr</td>                    
                    <td class="button-td"><button class="btn btn-success btn-sm modify-btn" data-id="0001">수정</button></td>
                    <td class="button-td"><button class="btn btn-danger btn-sm remove-btn" data-id="0001">삭제</button>
                    </td>
                </tr>             
            </tbody>
        </table>
    </div>
    <div class="card-text" style="text-align:right; padding: 20px; padding-right: 60px;">
        <button class="btn btn-primary btn-sm add-btn">추가</button>
    </div>
</div>

<div id="chat-contents"></div>

<script>
    //api_server 테스트
    $(document).ready(function(){
        room_no = 1;
        rpc("chat","contents",{'room':room_no},function(data){
            if(data.result!="ok"){
                alert("통신에 문제가 있습니다.");
                return;
            }

            var contents = data.contents;
            var item;
            var chats = "<h2>"+room_no+"번 채팅 내용</h2><br /> \n";
            for(inx = 0;inx<contents.length;++inx){
                item = contents[inx];                
                chats += "["+item.uname+"] : "+item.msg+"<br />\n";
            }
            $("#chat-contents").html(chats);
        });
    });

</script>