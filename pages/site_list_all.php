<style>
    .table-dark-head th{
        color: white;
        text-align: center;
    }

    #cons-history-table-body td{
        text-align: center;
    }

    #cons-history-table-body tr:hover, .cons-history-table-body-hovered {
        background: #7DBCFF;
        color: white;
        cursor: pointer;
    }

    .noselect {
        -webkit-touch-callout: none; /* iOS Safari */
        -webkit-user-select: none; /* Safari */
        -khtml-user-select: none; /* Konqueror HTML */
        -moz-user-select: none; /* Firefox */
            -ms-user-select: none; /* Internet Explorer/Edge */
                user-select: none; /* Non-prefixed version, currently
                                    supported by Chrome and Opera */
    }

    .add-cons-btn{
        position: absolute;
        right: 30px;
        font-size: 25px;
        background: #7dbcff;
        color: white;
        border-radius: 50%;
        height: 35px;
        width: 35px;
    }
</style>
<!-- <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=8635eb5a7465928c8d747aa7e5f3d6b6"></script> -->
<!-- <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=fb33fb2a39c5c870948bbe64f31deb8e"></script> -->
<script>          
//<![CDATA[     
    //공사중 마커 이미지
    var defaultImageSrc = "http://t1.daumcdn.net/localimg/localimages/07/mapjsapi/default_marker.png";
    var liveImageSrc = "http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/markerStar.png";
    var selectedImageSrc = "img/pin-selected.gif";
    // 마커 이미지의 이미지 크기 입니다
    var defaultImageSize = new daum.maps.Size(35,36);
    var liveImageSize = new daum.maps.Size(24, 35);     
    var selectedImageSize = new daum.maps.Size(35, 35);    
    // 마커 이미지를 생성합니다    
    var defaultMarkerImage = new daum.maps.MarkerImage(defaultImageSrc,defaultImageSize);
    var liveMarkerImage = new daum.maps.MarkerImage(liveImageSrc, liveImageSize); 
    var selectedMarkerImage = new daum.maps.MarkerImage(selectedImageSrc, selectedImageSize); 

    //지도 객체를 담을 변수
    var map;

    //표시 데이터
    var cons_id_list = new Array();
    var cons_list = new Array();                              
                // cons_list.push({id:'c0006',sdate:'2019-04-13',edate:'-',region:'서울시 마포구',name:'성암로(중소기업 DMC 앞) 포장 보수 공사 ',latlng:new daum.maps.LatLng(37.577387, 126.897684),manager:'김그래',mobile:'01086439391',phone:'023033713',islive:true});
                // cons_list.push({id:'c0005',sdate:'2018-09-13',edate:'2018-10-13',region:'서울시 마포구',name:'성산대교 포장 보수 공사',latlng:new daum.maps.LatLng(37.552715, 126.891457),manager:'김그래',mobile:'01086439391',phone:'023033713',islive:false});
                // cons_list.push({id:'c0004',sdate:'2017-04-23',edate:'2017-06-25',region:'서울시 영등포구',name:'양화대교 포장 보수 공사',latlng:new daum.maps.LatLng(37.542678, 126.903465),manager:'오세일',mobile:'01086439391',phone:'023033713',islive:false});
                // cons_list.push({id:'c0003',sdate:'2016-07-07',edate:'2016-09-15',region:'서울시 마포구',name:'서강대교 포장 보수 공사',latlng:new daum.maps.LatLng(37.537102, 126.925111),manager:'오세일',mobile:'01086439391',phone:'023033713',islive:false});
                // cons_list.push({id:'c0002',sdate:'2016-06-02',edate:'2016-09-12',region:'서울시 마포구',name:'가양대교 포장 보수 공사',latlng:new daum.maps.LatLng(37.570082, 126.861502),manager:'오세일',mobile:'01086439391',phone:'023033713',islive:false});
                // cons_list.push({id:'c0001',sdate:'2015-05-03',edate:'2015-08-25',region:'경기도 고양시',name:'고양대교 포장 보수 공사',latlng:new daum.maps.LatLng(37.6526522,126.7179719),manager:'김그래',mobile:'01086439391',phone:'023033713',islive:false});

            $(document).ready(function(){
                rpc("con-list-all", "contents", null, function (data) {
                    if (data.result != "ok") {
                        alert("통신에 문제가 있습니다.");
                        return;
                    }               
                    var contents = data.contents;
                    cons_list = new Array();
                    var item;
                    for(inx = 0;inx<contents.length;++inx){
                        item = contents[inx];
                        cons_list.push({id:item.con_code,sdate:item.con_goal_start_date,edate:item.con_actuality_end_date,
                        region:item.con_field_name,name:item.con_area_name,latlng:new daum.maps.LatLng(item.con_lat, item.con_long),
                        manager:item.con_field_manager,mobile:'-',phone:'-',islive:item.is_live == 'Y'});
                    }

                    renderMap();
                });                
                $(".add-cons-btn").click((evt)=>{
                    console.log('evt : ', evt);
                    location.href=".?cat=site&page=site_add_cons.html";
                });
            });


                //초기화
                function renderMap(){
                    var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
                    mapOption = { 
                        center: new daum.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
                        level: 3 // 지도의 확대 레벨
                    };

                    

                    // 지도를 표시할 div와  지도 옵션으로  지도를 생성합니다
                    map = new daum.maps.Map(mapContainer, mapOption); 

                    //지도 마커 표시 및 지도 범위 설정
                    var bounds = new daum.maps.LatLngBounds(); 
                    var liveCount = 0;
                    var consHistoryTableHTML = "";
                    var item;
                    var marker;
                    
                    // console.log("cons_list",cons_list);
                    for(inx = 0;inx<cons_list.length;++inx){
                        item = cons_list[inx];
                        // console.log(item);
                        marker = new daum.maps.Marker({
                            position: item.latlng,
                            title:item.name,
                            map:map
                        });                                      
                        cons_list[inx].marker = marker;
                        cons_id_list[item.id] = item;                        
                        marker.id = item.id;    
                        addMarkerActions(marker,item.id);                     
                        
                        bounds.extend(item.latlng);
                        if(item.islive){
                            marker.setImage(liveMarkerImage);
                            ++liveCount;
                        }else{
                            marker.setImage(defaultMarkerImage);
                        }                        
                        
                        // consHistoryTableHTML +='<tr class="cons-history-table-row" data-cons-id="'+item.id+'">'+
                        consHistoryTableHTML +='<tr class="cons-history-table-row" data-cons-id="3169">'+
                                '<td scope=\"row\">'+item.region+'</td>'+
                                '<td>'+item.name+'</td>'+
                                '<td>'+item.sdate+'</td>'+
                                '<td>'+item.edate+'</td>'+
                                '<td>'+item.manager+'</td>'+
                            '</tr>';                                    
                            
                    }
                    map.setBounds(bounds);                    

                    //시공 이력및 현재 시공 중 현장 텍스트입력
                    $(".total-cons-count").text(cons_list.length);
                    $(".live-cons-count").text(liveCount);

                    //테이블 완성
                    $("#cons-history-table-body").html(consHistoryTableHTML);

                    //테이블에 마우스 오버시 해당 마커 강조
                    $(".cons-history-table-row").mouseover(function(){
                        var s_item = cons_id_list[$(this).data('cons-id')];
                        s_item.marker.setMap(null);
                        s_item.marker.setImage(selectedMarkerImage);
                        s_item.marker.setMap(map);
                    });
                    $(".cons-history-table-row").mouseout(function(){
                        var s_item = cons_id_list[$(this).data('cons-id')];
                        s_item.marker.setMap(null);
                        if(s_item.islive){
                            s_item.marker.setImage(liveMarkerImage);
                        }else{
                            s_item.marker.setImage(defaultMarkerImage);
                        }
                        
                        s_item.marker.setMap(map);
                    });
                    $(".cons-history-table-row").click(function(){
                        var s_item = cons_id_list[$(this).data('cons-id')];
                        moveDetail(s_item.id);
                    });
                }

                //마커에 마우스 이벤트 등록                    
                function addMarkerActions(marker,id){
                        
                        daum.maps.event.addListener(marker, 'mouseover', function() {                                                
                            $(".cons-history-table-row[data-cons-id="+id+"]").addClass("cons-history-table-body-hovered");
                        });
                        
                        daum.maps.event.addListener(marker, 'mouseout', function() {                        
                            $(".cons-history-table-row[data-cons-id="+id+"]").removeClass("cons-history-table-body-hovered");
                        });

                        daum.maps.event.addListener(marker, 'click', function() {                        
                            moveDetail(id);
                        });
                }

                //상세 페이지로 이동
                function moveDetail(id){

                    location.href=".?cat=site&page=site_detail&id="+id;
                }
//        ]]>
</script>   


<div class="row">
	<div class="col-xl-6 col-md-12">
        <div class="card card-default" data-scroll-height="675">
            <div class="card-header">
                <h2>시공 이력 지도</h2>
            </div>
            <div class="card-body">
                <div id="map" style="width:100%;height:400px;"></div>             
            </div>
            <div class="card-footer d-flex flex-wrap bg-white p-0">
                <div class="col-6 px-0">
                    <div class="text-center p-4">
                        <h4 class="total-cons-count"></h4>
                        <p class="mt-2">총 시공 이력</p>
                    </div>
                </div>
                <div class="col-6 px-0">
                    <div class="text-center p-4 border-left">
                        <h4 class="live-cons-count"></h4>
                        <p class="mt-2">현재 시공 중</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div class="col-xl-6 col-md-12">
        <div class="card card-default" data-scroll-height="675">
            <div class="card-header">
                <h2>시공 이력 리스트</h2>
                <button class="add-cons-btn">+</button>
            </div>
            <div class="card-body" style="overflow:auto;    height: 500px;">
                <table class="table table-bordered noselect table-responsive">
                    <thead class="table-dark table-dark-head">
                        <tr>
                            <th scope="col">지역명</th>
                            <th scope="col">현장명</th>
                            <th scope="col">시공일</th>
                            <th scope="col">준공일</th>                          
                            <th scope="col">현장소장</th>
                        </tr>
                    </thead>
                    <tbody id="cons-history-table-body">
                        
                    </tbody>
                </table>
            </div>
                <div class="card-footer d-flex flex-wrap bg-white p-0">
                    <div class="col-6 px-0">
                        <div class="text-center p-4">
                            <h4 class="total-cons-count"></h4>
                            <p class="mt-2">총 시공 이력</p>
                        </div>
                    </div>
                    <div class="col-6 px-0">
                        <div class="text-center p-4 border-left">
                            <h4 class="live-cons-count"></h4>
                            <p class="mt-2">현재 시공 중</p>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>