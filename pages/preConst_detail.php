<style>
    .document-preview-frame {
        width: 100%;
        height: 800px;
    }

    #document-preview-div-preConst-detail {
        /* width: 100%;
        height: 500px; */
        text-align: center;
    }

    .preConst-list-num{
        width: 5%
    }

    .preConst-list-title{
        width: 20%
    }

    .preConst-list-content{
        width: 40%
    }

    .preConst-list-fileName{
        width: 15%
    }

    .preConst-list-preview{
        width: 10%
    }

    .preConst-list-download{
        width: 10%
    }

    .preConstDetail-header {
        position: relative;
    }

    .btn-preConst-list{
        /* padding-left: 1190px; */
        text-align: right;
        position: absolute;
        left: 1420px;
    }

</style>

<div class="c">
    <div class="card card-default">
        <div class="card-header card-header-border-bottom preConstDetail-header">
            <h2>
                <div id="siteDetailId" style="display:inline-block"></div>
            </h2>
            <div class='btn-preConst-list'>
                <button type='button' class='btn btn-secondary' onclick='goPreConstList(this);'>목록</button>
            </div> 
        </div>
      
        <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" id="icon-fieldSurvey-tab" data-toggle="tab" href="#icon-fieldSurvey" role="tab"
                        aria-controls="icon-fieldSurvey" aria-selected="true">
                        <i class="mdi mdi-ruler mr-1"></i>현장조사</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link " id="icon-history-tab" data-toggle="tab" href="#icon-history" role="tab"
                        aria-controls="icon-history" aria-selected="false">
                        <i class="mdi mdi-history mr-1"></i> 이력정보</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="icon-issues-tab" data-toggle="tab" href="#icon-issues" role="tab"
                        aria-controls="icon-issues" aria-selected="false">
                        <i class="mdi mdi-alarm-light mr-1"></i> 이슈사항</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane pt-3 fade" id="icon-fieldSurvey" role="tabpanel" aria-labelledby="icon-fieldSurvey-tab">
                    <?php include "preConst_detail_fieldSurvey.php"; ?>
                </div>

                <div class="tab-pane pt-3 fade" id="icon-history" role="tabpanel" aria-labelledby="icon-history-tab">
                    <?php include "preConst_detail_history.php"; ?>
                </div>
                <div class="tab-pane pt-3 fade" id="icon-issues" role="tabpanel" aria-labelledby="icon-issues-tab">
                    <?php include "preConst_detail_issues.php"; ?>
                </div>
            </div>
        </div>
    </div> 
</div>
<div id="document-preview-div-preConst-detail">
    <!-- <img src="/pages/uploads/pav1.png" alt="도로현황1"/> -->
</div>
<!-- <div id="document-preview-div">
</div> -->

<script>
    $(document).ready(function(){

        var params = new URLSearchParams(location.search);
        var t_value = params.get('t_value');
        console.log("t_value 확인: ", typeof t_value);

        if(t_value=='1') {
            $('#siteDetailId').text('성암로 포장 보수 공사');
        }else if(t_value=='2'){
            $('#siteDetailId').text('성산대교 포장 보수 공사');
        }else if(t_value=='3'){
            $('#siteDetailId').text('양화대교 포장 보수 공사');
        }else if(t_value=='4'){
            $('#siteDetailId').text('서강대교 포장 보수 공사');
        }else if(t_value=='5'){
            $('#siteDetailId').text('가양대교 포장 보수 공사');
        }else if(t_value=='6'){
            $('#siteDetailId').text('고양대교 포장 보수 공사');
        }else if(t_value=='7'){
            $('#siteDetailId').text('증산로 포장 보수 공사');
        }else if(t_value=='8'){
            $('#siteDetailId').text('수색로 포장 보수 공사');
        }

        $(".nav-link").click(function () {
            $("#document-preview-div-preConst-detail").html('');
            $("#document-preview-div-preConst-detail").css('overflow', 'hidden');
            var stateObj = { tab: $(this).attr("href") };
            history.pushState(stateObj, $(this).text(), $(this).attr("href"));
        });

        // $.urlParam = function (name) {
        //     var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        //     return results[1] || 0;
        // }
        // $.urlParam('id'); 

        var url = window.location.href;
        var hashInx = url.indexOf("#");
        var hash = url.substring(hashInx + 1);
        // console.log('hashhashhashhashhashhashhash',hash);
        // console.log(hashInx);
        if (hashInx == -1) hash = "icon-fieldSurvey";
        // if (hashInx == -1 || hashInx == 61) hash = "icon-live";

        $("#" + hash).addClass('show').addClass('active');
        $("#" + hash + "-tab").addClass("active");
    });

    function clickPreview(obj) {
        console.log("타입: ", typeof(obj));
        if(typeof(obj) == "string") {
            console.log("obj: ", obj);
            $("#document-preview-div-preConst-detail").css('overflow', 'scroll');
            $("#document-preview-div-preConst-detail").html('<img alt="도로현황1" src="/pages/uploads/' + obj + '">');
        }else{
            var file = $(obj)[0].dataset.file;
            console.log('파일명',file);
            $("#document-preview-div-preConst-detail").css('overflow', 'hidden');
            $("#document-preview-div-preConst-detail").html('<iframe class="document-preview-frame" src="pdf_viewer/web/viewer.html?file=' + file + '" ></iframe>');

        }
    }
    
    function clickDownload(obj) {
        var file = $(obj)[0].dataset.file;
        console.log('파일명2',file);
        $(obj).attr({ target: '_blank', href: file });
        $('#download-frame').attr("src", file);
    }

    function goPreConstList(obj) {
        location.href=".?cat=documents&page=documents_preConst";
    }
</script>