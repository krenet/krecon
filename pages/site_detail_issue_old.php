<style>
    .table-dark-head th {
        color: white;
        text-align: center;
    }

    #cons-history-table-body td {
        text-align: center;
    }

    #cons-history-table-body tr:hover,
    .cons-history-table-body-hovered {
        background: #7DBCFF;
        color: white;
        cursor: pointer;
    }

    /* #mapIssue div div div img{
                left:0px !important;
                top:0px !important;
            } */
</style>
<!-- <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=9160d0178961d9909b326b6f75f2031e"></script> -->
<script>



</script>

<div class="row">
    <div class="col-xl-6 col-md-12">
        <div class="card card-default" data-scroll-height="675">
            <div class="card-header">
                <h2>이슈 발생 지도</h2>
            </div>
            <div class="card-body">
                <div id="mapIssue" style="width:100%;height:400px;"></div>

            </div>

        </div>
    </div>

    <div class="col-xl-6 col-md-12">
        <div class="card card-default" data-scroll-height="675">
            <div class="card-header">
                <h2>이슈 발생 리스트</h2>
            </div>
            <div class="card-body" style="height:500px;overflow:auto;">
                <table class="table table-bordered noselect table-responsive" id="issue-table" style="text-align:center;">
                    <thead class="table-dark table-dark-head" id="community-site-issue">
                        <tr>
                            <th scope="col" class="">지역명</th>
                            <th scope="col" class="">현장명</th>
                            <th scope="col" class="">이슈</th>
                            <th scope="col" class="">수정</th>
                        </tr>
                    </thead>
                    <tbody id="cons-history-table-body">
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <div class="col-md-6"></div>
    <div class="col-md-6 card-text" style="text-align:right; padding: 20px;">
        <button class="btn btn-primary add-btn add-issue" data-toggle="modal" data-target="#makeIssueModal">추가</button>
    </div>
</div>
<!-- Form Modal -->
<div class="modal fade" id="makeIssueModal" tabindex="-1" role="dialog" aria-labelledby="makeIssueModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="makeIssueModalTitle">현장 이슈 추가</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group addOffInputName">
                        <label for="addOffInputName">현장명</label>
                        <input class="form-control" id="addOffInputName" value="" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label for="addIssueName">이슈</label>
                        <input type="text" class="form-control" id="addIssueName" placeholder="이슈를 입력해주세요.">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addIssueOnModal()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- 수정모달 -->
<div class="modal fade" id="editIssueModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="editModalLabel">수정</h4>
                <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×
                    </span><span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modalContent">
                    <form role="form" name="editModalForm" id="editModalForm" action="adviser-edit.php" method="post">
                        <div class="form-group">
                            <label for="지역명">지역명</label>
                            <input class="form-control" name="지역명" id="area" value="" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="현장명">현장명</label>
                            <input class="form-control" name="현장명" id="field" value="" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="이슈">이슈</label>
                            <input class="form-control" name="이슈" id="issue" value="">
                        </div>
                        <input type="hidden" name="dbID" id="dbID" value="">
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick='checkEditValue(this);'>Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="showDetailIssueModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titleArea"> </h4>
                <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×
                    </span><span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modalContent">
                        <div class="form-group">
                            <div id="innerContents" value=""></div>
                        </div>
                        <input type="hidden" name="dbID" id="dbID" value="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary" onclick='checkEditValue(this);'>Save changes</button> -->
            </div>
        </div>
    </div>
</div>


<script>
    //api_server 테스트
    $(document).ready(function () {

        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        $.urlParam('id');

        id_no = $.urlParam('id');

        rpc("issue", "contents", { 'id': id_no }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var offs = "";
            var area_name = "";
            var con_code = "";
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                offs += "<tr id='" + item.id + "'style='text-align:center;' value='" + item.id + "' onclick='clickDetailIssueRow(this);' data-toggle='modal' data-target='#showDetailIssueModal'><td>" + item.con_field_name + "</td>";
                offs += "<td>" + item.con_area_name + "</td>";
                offs += "<td>" + item.title + "</td>";
                offs += "<td> <button type='button' value='" + item.id + "' class='btn btn-success modify-btn btn-sm' data-toggle='modal' onclick='clickEdit(this);' data-target='#editIssueModal'>수정</button></td>";
                offs += "</tr>";
            
            }

            con_long = contents[0].con_long;
            con_lat = contents[0].con_lat;

            area_name = contents[0].con_area_name;
            $("#cons-history-table-body").html(offs);
            $('#addOffInputName').val(area_name);
            $('#addOffInputName').html(area_name);
            if (contents.length == 0) {
                var none = '';
                none += '<tr><td colspan=4 style=text-align:center>등록된 현장이슈가 없습니다. </td></tr>';
                $("#cons-history-table-body").html(none);
            }


            var mapContainer = document.getElementById('mapIssue'), // 지도를 표시할 div 
                mapOption = {
                    center: new daum.maps.LatLng(con_lat, con_long), // 지도의 중심좌표
                    level: 3 // 지도의 확대 레벨
                };
            var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다



            // 마커가 표시될 위치입니다 
            var markerPosition = new kakao.maps.LatLng(con_lat, con_long);
            // console.log(markerPosition);

            // 마커를 생성합니다
            var marker = new kakao.maps.Marker({
                position: markerPosition
            });

            // 마커가 지도 위에 표시되도록 설정합니다
            marker.setMap(map);

        });
    });

        // //초록수정 수정모달로 들어가기
        function clickEdit(obj) {
            var tr = $(obj).parent().parent();
            var dbID = $(obj).val();
            var td = tr.children();
            var str = ""
            var tdArr = new Array();
            td.each(function (i) {
                tdArr.push(td.eq(i).text());
            });

            $("#editIssueModal #area").val(tdArr[0]);
            $("#editIssueModal #field").val(tdArr[1]);
            $("#editIssueModal #issue").val(tdArr[2]);
            $('#editIssueModal #dbID').val(dbID);

        }

        //파란수정 수정이 실제로 되는 버튼
        function checkEditValue(obj) {
            var result = confirm("현장이슈를 수정하시겠습니까?");
            if (result) {
                alert('수정되었습니다.');
            } else {
                return;
            }

            var formValue = $(obj).parent().parent();
            var formData = $("#editModalForm").serializeObject();

            rpc("issue", "edit", formData, function (data) {
                if (data.result != "ok") {
                    alert("통신에 문제가 있습니다.");
                    return;
                }
                //수정하고나서의 프로세스 
                location.reload();
            });
        }

        jQuery.fn.serializeObject = function () {
            var obj = null;
            try {
                if (this[0].tagName && this[0].tagName.toUpperCase() == "FORM") {
                    var arr = this.serializeArray();
                    // console.log(arr);
                    if (arr) {
                        obj = {};
                        jQuery.each(arr, function () {
                            obj[this.name] = this.value;
                        });
                    }
                }
            } catch (e) {
                alert(e.message);
            } finally { }
            return obj;
        }


        function addIssueOnModal() {
            var sendData = {
                con_area_name: $('#addOffInputName').val(),
                title: $('#addIssueName').val(),
            };

            rpc("issue", "addIssueOnModal", sendData, function (data) {
                if (data.result != "ok") {
                    alert("통신에 문제가 있습니다.");
                    return;
                }
            });
            location.reload();
        }

        function clickDetailIssueRow(obj){
            // console.log($(obj)[0].attributes.value.value);
            var id = $(obj)[0].attributes.value.value;
            rpc("issue", "showDetailIssueModal", {'id': id}, function (data) {
                if (data.result != "ok") {
                    alert("통신에 문제가 있습니다.");
                    return;
                }
                var contents = data.contents;
                // console.log(contents[0].title);
                // console.log(contents[0].contents);

                var titleArea = contents[0].title;
                var innerContents = contents[0].contents;
            $('#showDetailIssueModal #titleArea').text(titleArea);
            $('#showDetailIssueModal #innerContents').text(innerContents);
            });
            // $('#showDetailIssueModal #titleArea').html(titleArea);
            // $('#showDetailIssueModal #titleArea').val(titleArea);

        }

</script>