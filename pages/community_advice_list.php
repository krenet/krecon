<style>
    .document-preview-frame {
        width: 100%;
        height: 800px;
    }
</style>
<script>
    $(document).ready(function () {
        $(".board-btn").click(function () {
            location.href = "http://localhost/?cat=community&page=community_advice_board";
        });
    });
</script>
<style>

    .btn-close{
background-color: #f5f6fa;
    }
    .sendMail{
        cursor:pointer;
    }
    .chat-ready-btn{
        background-color:#fec400;
    }
  
</style>
<div class="card card-default">
    <div class="card-header card-header-border-bottom">
        <h2>자문 리스트</h2>
    </div>
    <div class="card-body">
        <table class="table table-bordered table-responsive">
        <thead>
        <tr class="bg-light text-center">
                    <th>번호</th>
                    <th>자문제목</th>
                    <th>마감일</th>
                    <th>참여위원</th>
                    <th>게시판</th>
                </tr>
        </thead>
            <tbody id="advice-list">
    
            </tbody>
        </table>
    </div>

</div>



<!-- Modal -->
<div class="modal fade" id="adviceResultModal" tabindex="-1" role="dialog" aria-labelledby="adviceResultModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="adviceResultModalLabel">자문 결과</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-responsive">
                    <tr class="bg-light text-center">
                        <th>자문일자
                        </th>
                        <th>보기
                        </th>
                        <th>다운로드
                        </th>
                        <th>참석 자문</th>
                            <th>기타</th>
                            <th>메일</th>
                            <th>게시판가기</th>
                    </tr>
                 
                    <tr class="text-center">
                        <td>2019-01-30
                        </td>
                        <!-- <td>2
                        </td> -->
                        <td><button class="btn btn-primary btn-sm document-preview-btn"
                                data-file="/documents/준공검사원.pdf">미리보기</button>
                        </td>
                        <td><a class="btn btn-success btn-sm document-download-btn"
                                data-file="/documents/준공검사원.pdf">다운로드</a></td>
                                <td>최준성, 이상염</td>
                            <td>-</td>
                            <td><span class="mdi mdi-email-outline sendMail" data-toggle="modal" data-target="#sendMailAdvModal"></span></span></td>
                            <td><button class="btn btn-success btn-sm board-btn" data-id="0001">자세히</button></td>
                         </tr>
                </table>
    
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    //api_server 테스트
    $(document).ready(function(){
        // $.urlParam = function (name) {
        //     var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        //     return results[1] || 0;
        // }
        // var id = $.urlParam('id');
        rpc("advice","contents",null,function(data){
            if(data.result!="ok"){
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var offs = "";
            for(inx = 0;inx<contents.length;++inx){
                item = contents[inx];
                offs += "<tr id='advice_list_"+item.id+"'style='text-align:center;'><td><input type='hidden'value="+item.id+">"+(inx+1)+"</td>";
                offs += "<td>"+item.title+"</td>";
                offs += "<td>"+item.deadline+"</td>";
                offs += "<td>"+item.name+"</td>";
                offs += "<td><button type='button' class='btn btn-success btn-sm' onclick='goBoard("+item.id+");'>게시판</button></td>";
                offs += "</tr>";
            }

            $("#advice-list").html(offs);
            if (contents.length == 0) {
                var none = '';
                none += '<tr><td colspan=5 style=text-align:center>등록된 자문이 없습니다. </td></tr>';
                $("#advice-list").html(none);
            }
        });
    });
    function goBoard(id){
        location.href = "http://localhost/?cat=community&page=community_advice_board&id=" + id;
    }
</script>  