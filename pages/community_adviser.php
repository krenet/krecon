<script>

</script>
<style>
	#ui-id-1 {
		z-index: 9999;
	}
	
    .select2-container {
        display: block !important;
    }

    .select2-container--default .select2-selection--single {
        border: 1px solid #e5e9f2 !important;

    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding: .5rem 1.06rem !important;
        border-color: #e5e9f2 !important;
        line-height: 28px !important;
        font-size: 0.98rem !important;
        color: #B4B8C0 !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        margin-top: 0.5rem !important;
    }

    .select2-container .select2-selection--single {
        height: unset !important;
    }
</style>
<div class="card card-default">
	<div class="card-header card-header-border-bottom">
		<h2>자문위원 리스트</h2>
	</div>
	<div class="card-body">
		<table class="table table-bordered table-responsive">
			<thead>
				<tr class="bg-light text-center">
					<th class="" name='id'>번호</th>
					<th class="" name='name'>위원명</th>
					<th class="" name='affiliation'>소속</th>
					<!-- <th class="" name='part'>부서</th> -->
					<!-- <th class="" name='position'>직급</th> -->
					<th class="" name='field'>분야</th>
					<th class="" name='phone'>전화</th>
					<th class="" name='mail'>메일</th>
					<th class="" name='mail'>생일</th>
					<th class="">수정</th>
					<th class="">삭제</th>
				</tr>
			</thead>
			<tbody id="adv-contents">
			</tbody>
		</table>
	</div>
	<div class="card-text" style="text-align:right; padding: 20px; padding-right: 60px;">
		<button class="btn btn-primary add-btn add-issue" data-toggle="modal" data-target="#addAdvModal">추가</button>
	</div>
</div>
<!-- Form Modal -->
<div class="modal fade" id="addAdvModal" tabindex="-1" role="dialog" aria-labelledby="addAdvModalTitle"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="addAdvModalTitle">자문위원 추가</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="addModalForm" name="addModalForm" action="adviser.php" method="post">
						<div class="form-group ui-widget">
						<label for="addName">이름</label>
						<input type="text" class="form-control tags" id="addName" placeholder="이름을 입력해주세요.">
					</div>
					<div class="form-group">
                        <label for="addAdvAffiliation">소속</label>
                        <select class="form-control js-example-basic-single" name="affiliation" id="addAdvAffiliation">

                        </select>
                    </div>
					<div class="form-group">
						<label for="addField">분야</label>
						<input type="text" class="form-control" id="addField" placeholder="분야를 입력해주세요.">
					</div>
					<div class="form-group">
						<label for="addPhone">전화</label>
						<input type="text" class="form-control" id="addPhone" placeholder="전화를 입력해주세요.">
					</div>
					<div class="form-group">
						<label for="addMail">메일</label>
						<input type="email" class="form-control" id="addMail" placeholder="메일을 입력해주세요.">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary"onclick="addAdviser()">Save</button>
			</div>
		</div>
	</div>
</div>
<!-- 수정모달 -->
<div class="modal fade" id="editAdviserModal" tabindex="-1" role="dialog" aria-labelledby="editAdviserModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<h4 class="modal-title" id="editAdviserModalLabel">자문위원 수정</h4>
                <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×
                    </span><span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="editAdvicialModal" id="editAdvicialModal">
                    <div class="form-group ui-widget">
                        <label for="editAdvName">이름</label>
                        <input type="text" class="form-control tags" id="editAdvName" placeholder="이름을 입력해주세요.">
                    </div>

                    <div class="form-group">
                        <label for="editAdvAffiliation">소속</label>
                        <select class="form-control js-example-basic-single" name="affiliation" id="editAdvAffiliation">

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="editAdvPhone">전화</label>
                        <input type="text" class="form-control" id="editAdvPhone" placeholder="전화를 입력해주세요.">
                    </div>
                    <div class="form-group">
                        <label for="editAdvMemo">메모</label>
                        <input type="text" class="form-control" id="editAdvMemo" placeholder="메모를 입력해주세요.">
                    </div>
                    <div class="form-group">
                        <label for="editAdvBirth">생일</label>
                        <input type="date" class="form-control" id="editAdvBirth" placeholder="생일을 입력해주세요.">
                    </div>
                    <div class="form-group">
                        <label for="editAdvMail">메일</label>
                        <input type="email" class="form-control" id="editAdvMail" placeholder="메일을 입력해주세요.">
                    </div>
                    <input type="hidden" id="editAdvID" name="dbID" value="" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick='checkEditValue(this)'>Save changes</button>
            </div>
        </div>
    </div>
</div>
    <script>
		//api_server 테스트

		$(document).ready(function () {
            //리스트뽑기
            getAdviserList();

		});

		//초록수정 수정모달로 들어가기
		function clickEdit(obj) {
			var tr = $(obj).parent().parent();
			console.log($(tr[0]).attr('id'));
			id = $(tr[0]).attr('id')
			rpc("adviser", "getAdvById", { 'id': id }, function (data) {
				var contents = data.contents;
				var item;
				item = contents[0];
				console.log(item);
				$('#editAdvName').val(item.name);
				$('#editAdvAffiliation').val(item.affiliation);
				$('#editAdvPhone').val(item.phone);
				$('#editAdvMemo').val(item.etc);
				$('#editAdvBirth').val(item.birthday);
				$('#editAdvMail').val(item.mail);
				$('#editAdvID').val(item.id);
				console.log($('#editAdvAffiliation')[0].selectedOptions[0].innerText);
			});	

		}

	    //파란수정 수정이 실제로 되는 버튼
		function checkEditValue(obj) {
        var result = confirm("자문정보를 수정하시겠습니까?");
        if (result) {
            alert('수정되었습니다.');
        } else {
            return;
        }
        var sendData = {
            id: $('#editAdvID').val(),
            name: $('#editAdvName').val(),
            affiliation: $('#editAdvAffiliation').val(),
            phone: $('#editAdvPhone').val(),
            mail: $('#editAdvMail').val(),
            memo: $('#editAdvMemo').val(),
            birth: $('#editAdvBirth').val()
        };
        console.log(sendData);

        rpc("adviser", "edit", sendData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            //수정하고나서의 프로세스 
            // location.reload();
        });
    }

		//빨간삭제
		function clickDelete(obj) {
			var result = confirm("자문위원을 삭제하시겠습니까?");
			if (result) {
				alert('삭제되었습니다.');
			} else {
				return;
            }
			var tr = $(obj).parent().parent();
			id = $(tr[0]).attr('id');
	
			//백업테이블에 자문 추가 및 자문테이블에서 해당 자문 삭제
			insertAdviser(id);

			//페이지 리로드
			// location.reload();
        }
        
        function getAdviserList(){
            rpc("adviser", "contents", null, function (data) {
				if (data.result != "ok") {
					alert("통신에 문제가 있습니다.");
					return;
				}

				var contents = data.contents;
				var item;
				var offs = "";
				for (inx = 0; inx < contents.length; ++inx) {
					item = contents[inx];
					offs += "<tr id='" + item.id + "'style='text-align:center;'><td >" + (inx + 1) + "</td><td>" + item.name + "</td>";
					offs += "<td>" + item.affiliation + "</td>";
					// offs += "<td>" + item.part + "</td>";
					// offs += "<td>" + item.position + "</td>";
					offs += "<td>" + item.field + "</td>";
					offs += "<td>" + (item.phone==null?'-':item.phone)+ "</td>";
					offs += "<td>" + (item.mail==null?'-':item.mail) + "</td>";
					offs += "<td>" + (item.birthday==null?'-':item.birthday) + "</td>";
					offs += "<td><button class='btn btn-success btn-sm modify-btn'data-toggle='modal' onclick='clickEdit(this);' data-target='#editAdviserModal' contenteditable='true'>수정</button></td>";
					offs += "<td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</button></td></tr>";
				}
				$("#adv-contents").html(offs);
			});
			rpc("adviser", "getAffiliation", null, function (data) {
            var contents = data.contents;
            var item;
            var option;
            option += "<option>소속을 입력해주세요</option>"
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                option += "<option data-value='" + item.affiliation + "'>" + item.affiliation + "</option>"

            }
            $('#addAdvAffiliation').html(option);
            $('#editAdvAffiliation').html(option);

        });

        $('.js-example-basic-single').select2();
		}
		function insertAdviser(obj){
        rpc("adviser", "backup", { 'id': id }, function (data) {
			if (data.result != "ok") {
				alert("통신에 문제가 있습니다.");
				return;
			}
			//호출문제로 인해 삭제함수를 여기다 넣었음
            deleteAdviser(id);
		});
    }

		function deleteAdviser(obj){
			rpc("adviser", "delete",  {'id':id}, function (data) {
				if (data.result != "ok") {
					alert("통신에 문제가 있습니다.");
					return;
				}
			});
			location.reload();

		}

		function addAdviser(){
			var result = confirm("자문위원을 추가하시겠습니까?");
			if (result) {
				alert('추가되었습니다.');
			} else {
				return;
            }
			var sendData = {name:$('#addName').val(), 
							affiliation:$('#addAffiliation').val(),
							field:$('#addField').val(),
							phone:$('#addPhone').val(),
							email:$('#addMail').val()};
			console.log(sendData);

			rpc("adviser", "addAdviser", sendData, function (data) {
			if (data.result != "ok") {
				alert("통신에 문제가 있습니다.");
				return;
			}
		});
		location.reload();

		}
		
	</script>