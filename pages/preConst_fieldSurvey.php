<style>
    .document-preview-frame {
        width: 100%;
        height: 800px;
    }

    .button-td {
        width: 70px;
    }
</style>
<!-- <script>
    $(document).ready(function () {
        getCompletionList();


        rpc("documents", "conList", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var options = "";

            options += "<option value='all'>전체</option>";

            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                options += "<option value=" + item.con_code + ">" + item.con_area_name + "</option>";

            }
            $("#select-button-by-code-com").html(options);

        });

        $("#select-button-by-code-com").change(function () {
            con_code = $(this).val();
            console.log(con_code);
            if (con_code == 'all') {
                getCompletionList();
            } else {
                rpc("documents", "getCompletionListbyConcode", {'con_code' : con_code}, function (data) {
                    if (data.result != "ok") {
                        alert("통신에 문제가 있습니다.");
                        return;
                    }
                    var contents = data.contents;
                    var item;
                    var doc = "";
                    for (inx = 0; inx < contents.length; ++inx) {
                        item = contents[inx];
                        doc += "<tr style='text-align:center'>";
                        doc += "<td>" + (inx + 1) + "</td>";
                        doc += "<td style = 'text-align: left;'>" + item.doc_title + "</td>";
                        // doc += "<td>" + item.doc_classify + "</td>";
                        doc += "<td class='button-td'><button class='btn btn-primary btn-sm' onclick='documentPreviewBtn(this);'data-file='/documents/" + item.doc_file_name + ".pdf'>미리보기</button></td>";
                        doc += "<td class='button-td'><a class='btn btn-success btn-sm' onclick='documentDownloadBtn(this);'data-file='/documents/" + item.doc_file_name + ".pdf'>다운로드</a></td>";
                        doc += "</tr>";
                    }
                    $("#krecon_document_completions").html(doc);
                });
            }
        });


    });

    function getCompletionList() {
        rpc("documents", "completions", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var doc = "";
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                doc += "<tr style='text-align:center'>";
                doc += "<td>" + (inx + 1) + "</td>";
                doc += "<td style = 'text-align: left;'>" + item.doc_title + "</td>";
                // doc += "<td>" + item.doc_category + "</td>";
                doc += "<td class='button-td'><button class='btn btn-primary btn-sm' onclick='documentPreviewBtn(this);'data-file='/documents/" + item.doc_file_name + ".pdf'>미리보기</button></td>";
                doc += "<td class='button-td'><a class='btn btn-success btn-sm' onclick='documentDownloadBtn(this);'data-file='/documents/" + item.doc_file_name + ".pdf'>다운로드</a></td>";
                doc += "</tr>";
            }
            $("#krecon_document_completions").html(doc);
        });
    }


    function documentPreviewBtn(obj) {
        var file = $(obj)[0].dataset.file;
        console.log(file);
        $("#document-preview-div-completion").html('<iframe class="document-preview-frame" src="pdf_viewer/web/viewer.html?file=' + file + '" ></iframe>');
    }

    function documentDownloadBtn(obj) {
        var file = $(obj)[0].dataset.file;
        $(obj).attr({ target: '_blank', href: file });
        $('#download-frame').attr("src", file);
    }


</script> -->
<div class="card card-default" data-scroll-height="675">
    <div class="card-header">
        <h2>준공서류 리스트</h2>
        <!-- <div class="dropdown d-inline-block mb-1" style="margin-left:10px">
            <label for="select-button-by-code-com">공사 선택</label>
            <select class="form-control" id="select-button-by-code-com">
    
            </select>
        </div> -->
    <!-- </div>
    <div class="card-body slim-scroll">
        <table class="table table-bordered table-responsive">
            <thead>
                <tr class="bg-light text-center">
                    <th>번호</th>
                    <th>공사명</th>
                    <th>미리보기</th>
                    <th>다운로드</th>
                </tr>
            </thead>
            <tbody id="krecon_document_completions">


            </tbody>
        </table>
    </div>
</div>
<div id="document-preview-div-completion"> -->
</div>