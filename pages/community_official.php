<style>
    #ui-id-1 {
        z-index: 9999;
    }

    .select2-container {
        display: block !important;
    }

    .select2-container--default .select2-selection--single {
        border: 1px solid #e5e9f2 !important;

    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding: .5rem 1.06rem !important;
        border-color: #e5e9f2 !important;
        line-height: 28px !important;
        font-size: 0.98rem !important;
        color: #B4B8C0 !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        margin-top: 0.5rem !important;
    }

    .select2-container .select2-selection--single {
        height: unset !important;
    }
</style>

<div class="card card-default">
    <div class="card-header card-header-border-bottom">
        <h2>공무원 리스트</h2>
    </div>
    <div class="card-body">
        <table class="table table-bordered table-responsive">
            <thead>
                <tr style="text-align:center;">
                    <th>번호</th>
                    <th class="">이름</th>
                    <th class="">소속</th>
                    <!-- <th class="">부서</th> -->
                    <!-- <th class="">직급</th> -->
                    <th class="">전화</th>
                    <th class="">메일</th>
                    <th class="">생일</th>
                    <th>메모</th>
                    <th>수정</th>
                    <th>삭제</th>
                </tr>
            </thead>
            <tbody style="text-align:center;" id="off-contents">
            </tbody>
        </table>
    </div>
    <div class="card-text" style="text-align:right; padding: 20px; padding-right: 60px;">
        <button class="btn btn-primary add-btn add-issue" data-toggle="modal" data-target="#addOffModal">추가</button>
    </div>
</div>
<!--추가모달 -->
<div class="modal fade" id="addOffModal" tabindex="-1" role="dialog" aria-labelledby="addOffModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addOffModalTitle">공무원 추가</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="addOfficialModal" id="addOfficialModal">
                    <div class="form-group ui-widget">
                        <label for="addOffName">이름</label>
                        <input type="text" class="form-control tags" id="addOffName" placeholder="이름을 입력해주세요.">
                    </div>

                    <div class="form-group">
                        <label for="addOffAffiliation">소속</label>
                        <select class="form-control js-example-basic-single" name="affiliation" id="addOffAffiliation">

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="addOffPhone">전화</label>
                        <input type="text" class="form-control" id="addOffPhone" placeholder="전화를 입력해주세요.">
                    </div>
                    <div class="form-group">
                        <label for="addOffMemo">메모</label>
                        <input type="text" class="form-control" id="addOffMemo" placeholder="메모를 입력해주세요.">
                    </div>
                    <div class="form-group">
                        <label for="addOffBirth">생일</label>
                        <input type="date" class="form-control" id="addOffBirth" placeholder="생일을 입력해주세요.">
                    </div>
                    <div class="form-group">
                        <label for="addOffMail">메일</label>
                        <input type="email" class="form-control" id="addOffMail" placeholder="메일을 입력해주세요.">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addOfficial()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- 메모모달 -->
<div class="modal fade" id="memoModal" tabindex="-1" role="dialog" aria-labelledby="memoModalTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="memoModalTitle">
                    공무원 특이사항</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="off-etc"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

<!-- 수정모달 -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="editModalLabel">수정</h4>
                <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×
                    </span><span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="editOfficialModal" id="editOfficialModal">
                    <div class="form-group ui-widget">
                        <label for="editOffName">이름</label>
                        <input type="text" class="form-control tags" id="editOffName" placeholder="이름을 입력해주세요.">
                    </div>

                    <div class="form-group">
                        <label for="editOffAffiliation">소속</label>
                        <select class="form-control js-example-basic-single" name="affiliation" id="editOffAffiliation">

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="editOffPhone">전화</label>
                        <input type="text" class="form-control" id="editOffPhone" placeholder="전화를 입력해주세요.">
                    </div>
                    <div class="form-group">
                        <label for="editOffMemo">메모</label>
                        <input type="text" class="form-control" id="editOffMemo" placeholder="메모를 입력해주세요.">
                    </div>
                    <div class="form-group">
                        <label for="editOffBirth">생일</label>
                        <input type="date" class="form-control" id="editOffBirth" placeholder="생일을 입력해주세요.">
                    </div>
                    <div class="form-group">
                        <label for="editOffMail">메일</label>
                        <input type="email" class="form-control" id="editOffMail" placeholder="메일을 입력해주세요.">
                    </div>
                    <input type="hidden" id="editOffID" name="dbID" value="" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick='checkEditValue(this)'>Save changes</button>
            </div>
        </div>
    </div>
</div>



<script>
    //api_server 테스트

    $(document).ready(function () {

        rpc("official", "contents", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }

            var contents = data.contents;
            var item;
            var offs = "";
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                offs += "<tr id='" + item.id + "'><td>" + (inx + 1) + "</td><td>" + item.name + "</td>";
                offs += "<td>" + item.affiliation + "</td>";
                // offs += "<td>" + item.part + "</td>";
                // offs += "<td>" + item.position + "</td>";
                offs += "<td>" + item.phone + "</td>";
                offs += "<td>" + item.mail + "</td>";
                offs += "<td>" + (item.birthday == null ? '-' : item.birthday) + "</td>";
                // offs += "<td><input type='hidden' value="+item.etc+"/>";
                offs += "<td><button class='btn btn-success btn-sm modify-btn' onclick='offEtc(this);' data-toggle='modal' data-target='#memoModal'>보기</button></td>";
                // offs += "<button class='btn btn-success btn-sm modify-btn'  data-toggle='modal' data-target='#memoModal'>보기</button></td>";
                offs += "<td><button class='btn btn-success btn-sm modify-btn'data-toggle='modal' data-target='#editModal' onclick='clickEdit(this);' contenteditable='true'>수정</button></td>";
                offs += "<td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</button></td></tr>";
            }
            $("#off-contents").html(offs);
        });
        rpc("official", "getAffiliation", null, function (data) {
            var contents = data.contents;
            var item;
            var option;
            option += "<option>소속을 입력해주세요</option>"
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                option += "<option data-value='" + item.affiliation + "'>" + item.affiliation + "</option>"

            }
            $('#addOffAffiliation').html(option);
            $('#editOffAffiliation').html(option);

        });

        $('.js-example-basic-single').select2();
    });

    function offEtc(obj) {
        var tr = $(obj).parent().parent();
        console.log($(tr[0]).attr('id'));
        id_no = $(tr[0]).attr('id');
        rpc("official", "etc-contents", { "id": id_no }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }

            var contents = data.contents;
            var item;
            var offs = "";
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                offs += "<div>" + item.etc + "</div>";
            }
            $("#off-etc").html(offs);
        });
    }


    //초록수정 수정모달로 들어가기
    function clickEdit(obj) {

        var tr = $(obj).parent().parent();
        console.log($(tr[0]).attr('id'));
        id = $(tr[0]).attr('id')
        rpc("official", "getOffById", { 'id': id }, function (data) {
            var contents = data.contents;
            var item;
            item = contents[0];
            console.log(item);
            $('#editOffName').val(item.name);
            $('#editOffAffiliation').val(item.affiliation);
            $('#editOffPhone').val(item.phone);
            $('#editOffMemo').val(item.etc);
            $('#editOffBirth').val(item.birthday);
            $('#editOffMail').val(item.mail);
            $('#editOffID').val(item.id);
            console.log($('#editOffAffiliation')[0].selectedOptions[0].innerText);
        });

    }

    //파란수정 수정이 실제로 되는 버튼
    function checkEditValue(obj) {
        var result = confirm("공무원정보를 수정하시겠습니까?");
        if (result) {
            alert('수정되었습니다.');
        } else {
            return;
        }
        var sendData = {
            id: $('#editOffID').val(),
            name: $('#editOffName').val(),
            affiliation: $('#editOffAffiliation').val(),
            phone: $('#editOffPhone').val(),
            mail: $('#editOffMail').val(),
            memo: $('#editOffMemo').val(),
            birth: $('#editOffBirth').val()
        };
        console.log(sendData);

        rpc("official", "edit", sendData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            //수정하고나서의 프로세스 
            // location.reload();
        });
    }

    //삭제
    function clickDelete(obj) {
        var result = confirm("해당 공무원을 삭제하시겠습니까?");
        if (result) {
            alert('삭제되었습니다.');
        } else {
            return;
        }
        var tr = $(obj).parent().parent();
        id = $(tr[0]).attr('id');

        //백업테이블에 공무원 추가 및 공무원테이블에서 해당 공무원 삭제
        insertOfficial(id);

        //페이지리로드

    }

    // jQuery.fn.serializeObject = function () {
    //     var obj = null;
    //     try {
    //         if (this[0].tagName && this[0].tagName.toUpperCase() == "FORM") {
    //             var arr = this.serializeArray();
    //             console.log(arr);
    //             if (arr) {
    //                 obj = {};
    //                 jQuery.each(arr, function () {
    //                     obj[this.name] = this.value;
    //                 });
    //             }
    //         }
    //     } catch (e) {
    //         alert(e.message);
    //     } finally { }
    //     return obj;
    // }
    function insertOfficial(obj) {
        rpc("official", "backup", { 'id': id }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            //호출문제로 인해 삭제함수를 여기다 넣었음
            deleteOfficial(id);
        });
    }

    function deleteOfficial(obj) {
        rpc("official", "delete", { 'id': id }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
        });
        location.reload();
    }

    function addOfficial() {
        var result = confirm("공무원을 추가하시겠습니까?");
        if (result) {
            alert('추가되었습니다.');
        } else {
            return;
        }
        var sendData = {
            name: $('#addOffName').val(),
            affiliation: $('#addOffAffiliation').val(),
            phone: $('#addOffPhone').val(),
            memo: $('#addOffMemo').val(),
            birth: $('#addOffBirth').val(),
            email: $('#addOffMail').val()
        };
        console.log(sendData);
        // return;
        rpc("official", "addOfficial", sendData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
        });
        // location.reload();

    }


</script>