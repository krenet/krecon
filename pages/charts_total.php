<script src="assets/plugins/charts/Chart.min.js"></script>  

    <div id="container" style="width: 100%;">    
		<canvas id="progress-canvas"></canvas>
	</div>

	<script>        		
		var color = Chart.helpers.color;
        
		var chartData = {
			labels: ['2014', '2015', '2016', '2017', '2018'],
			datasets: [{
				label: '매출',
				backgroundColor: "rgba(255,0,0,0.5)",
				borderColor: color.blue,
				borderWidth: 1,
				data: [
					10000000000,
					13230000000,
					15460000000,
					20100000000,
					18900000000
				]
			}, {
				label: '영업이익',
				backgroundColor: "rgba(0,255,0,0.5)",
				borderColor: "rgba(0,255,0,1)",
				data: [
					3000400000,
					5230000000,
					6054000000,
					7004300000,
					7000000000
				]
			},{
				label: '순이익',
				backgroundColor: "rgba(0,0,255,0.5)",
				borderColor: "rgba(0,0,255,1)",
				data: [
					384000000,
					923000000,
					1205400000,
					1700430000,
					1480000000
				]
			}]

		};

		window.onload = function() {
			var ctx = document.getElementById('progress-canvas').getContext('2d');
			window.myHorizontalBar = new Chart(ctx, {
				type: 'bar',
				data: chartData,
				options: {
					// Elements options apply to all of the options unless overridden in a dataset
					// In this case, we are setting the border of each horizontal bar to be 2px wide
					elements: {
						rectangle: {
							borderWidth: 2,
						}
					},
					responsive: true,
					legend: {
						position: 'right',
					},
					title: {
						display: true,
						text: '최근 5년 연단위 수익 통계'
					},
					scales: {
            yAxes: [{
                ticks: {
                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                        return  numberWithCommas(value / 1000000)+" 백만원";
                    }
                }
            }]
        	},
					tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    label += numberWithCommas(Math.round(tooltipItem.yLabel * 100) / 100);
                    return label;
                }
						}
					}
				}
			});

		};

	
		function numberWithCommas(n) {
			var parts=n.toString().split(".");
			return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
		}
	</script>