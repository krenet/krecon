<style>
    .document-preview-frame {
        width: 100%;
        height: 800px;
    }

    .button-td {
        width: 70px;
    }

    /* .add-cons-btn{
        position: absolute;
        right: 50px;
        font-size: 25px;
        background: #7dbcff;
        color: white;
        border-radius: 50%;
        height: 35px;
        width: 35px;
    } */
</style>

<div class="card card-default" data-scroll-height="675">
    <div class="card-header">
        <h2>사전조사 리스트</h2>
        <!-- <button class="add-cons-btn">+</button> -->
    </div>
    <div class="card-body">
		<table class="table table-bordered table-responsive">
			<thead>
				<tr class="bg-light text-center">
					<th name='id'>번호</th>
					<th name='title'>현장명</th>
					<th name='address'>지역명</th>
					<th name='type'>공사개요</th>
					<th name='bid'>금액</th>
					<th>내용</th>
					<th>삭제</th>
				</tr>
			</thead>
			<tbody id="adv-contents">
                <tr class="text-center">
                    <td>1</td>
                    <td>성암로 포장 보수 공사</td>
                    <td>서울특별시 마포구 성암로</td>
                    <td>도로보수</td>
                    <td>37,730,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='1' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
                <tr class="text-center">
                    <td>2</td>
                    <td>성산대교 포장보수 공사</td>
                    <td>서울시 마포구</td>
                    <td>교량보수</td>
                    <td>51,804,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='2' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
                <tr class="text-center">
                    <td>3</td>
                    <td>양화대교 포장 보수 공사</td>
                    <td>서울시 영등포구</td>
                    <td>교량보수</td>
                    <td>99,500,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='3' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
                <tr class="text-center">
                    <td>4</td>
                    <td>서강대교 포장 보수 공사</td>
                    <td>서울시 마포구</td>
                    <td>교량보수</td>
                    <td>35,761,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='4' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
                <tr class="text-center">
                    <td>5</td>
                    <td>가양대교 포장 보수 공사</td>
                    <td>서울시 마포구</td>
                    <td>교량보수</td>
                    <td>101,400,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='5' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
                <tr class="text-center">
                    <td>6</td>
                    <td>고양대교 포장 보수 공사</td>
                    <td>경기도 고양시</td>
                    <td>교량보수</td>
                    <td>100,000,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='6' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
                <tr class="text-center">
                    <td>7</td>
                    <td>증산로 포장 보수 공사</td>
                    <td>서울시 은평구 증산로</td>
                    <td>도로유지보수</td>
                    <td>95,300,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='7' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
                <tr class="text-center">
                    <td>8</td>
                    <td>수색로 포장 보수 공사</td>
                    <td>서울시 서대문구 수색로</td>
                    <td>도로 소파보수</td>
                    <td>170,800,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='8' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
			</tbody>
		</table>
	</div>
</div>

    <script>
        
        $(document).ready(function () {
        $(".add-cons-btn").click((evt)=>{
                    console.log('evt : ', evt);
                    location.href=".?cat=preConst&page=preConst_addList";
                });
        });

        function clickDelete(obj) {
            var result = confirm("해당 리스트 정보를 삭제하시겠습니까?");
            if(result) {
                alert('삭제되었습니다.');
            }else{
                return;
            }
        }

        function goDetail(obj){
            // console.log("this: ",obj.value);
            var t_value = obj.value;
            location.href="http://localhost/?cat=preConst&page=preConst_detail&t_value="+t_value;
        }
    </script>