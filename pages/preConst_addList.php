<style>
    /* .add-btn-set{
        position: absolute;
        right: 50px;
    }

    .add-fieldSurvey-tb{
        margin-top: 3rem;
    } */

    .fieldSurveyModal, .historyModal, .issuesModal{
        width: 150%;
    }

    /* .filedSurvey-add-btn, .history-add-btn, .issues-add-btn{
        width: 80px;
    } */

    .add-cons-btn{
        /* position: absolute;
        left: 30px; */
        font-size: 25px;
        background: #7dbcff;
        color: white;
        border-radius: 50%;
        height: 35px;
        width: 35px;
        line-height: 25px;
    }

    #fieldSurvey_content, #history_content, #issues_content{
        height: 250px;
    }
</style>

<div class="c">
    <div class="card card-default">
        <div class="card-header card-header-border-bottom">
            <h2>
                <div id="siteDetailId" style="display:inline-block">사전조사 등록</div>
            </h2>

        </div>
        <div class="card-body">
            <form action="" method="post" enctype="multipart/form-data" name="preConst_add" id="preConst_add">
                
                <!-- <div class ="form-group preConst">
                    <label>현장조사</label>
                    <div class ="sub-group">
                        <label for="fieldSurvey_title">제목</label> </br>
                        <input type="text"  class="form-control tags" id="fieldSurvey_title" placeholder="제목을 입력해주세요">
                
                    </div>
                </div>   -->
            </form>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" id="icon-summary-tab" data-toggle="tab" href="#icon-summary" role="tab"
                        aria-controls="icon-summary" aria-selected="true">
                        <i class="mdi mdi-clipboard-text mr-1"></i>개요</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="icon-fieldSurvey-tab" data-toggle="tab" href="#icon-fieldSurvey" role="tab"
                        aria-controls="icon-fieldSurvey" aria-selected="true">
                        <i class="mdi mdi-ruler mr-1"></i>현장조사</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link " id="icon-history-tab" data-toggle="tab" href="#icon-history" role="tab"
                        aria-controls="icon-history" aria-selected="false">
                        <i class="mdi mdi-history mr-1"></i> 이력정보</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="icon-issues-tab" data-toggle="tab" href="#icon-issues" role="tab"
                        aria-controls="icon-issues" aria-selected="false">
                        <i class="mdi mdi-alarm-light mr-1"></i> 이슈사항</a>
                </li>
            </ul>
          
            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane pt-3 fade" id="icon-summary" role="tabpanel" aria-labelledby="icon-summary-tab">
                    <div class ="form-group preConst">
                        <label for="preConst_add_title">현장명</label>
                        <input type="text"  class="form-control tags" id="preConst_add_title" readonly>
                        <!-- <input type="text"  class="form-control tags" id="preConst_add_title" placeholder="현장명을 입력해주세요"> -->
                    </div>             
                    <div class ="form-group preConst">
                        <label for="preCon_add_address">지역명</label>
                        <input type="text"  class="form-control tags" id="preConst_add_address" readonly>
                        <!-- <input type="text"  class="form-control tags" id="preConst_add_address" placeholder="지역명을 입력해주세요"> -->
                    </div>  
                    <div class ="form-group preConst">
                        <label for="preConst_add_type">공사개요</label>
                        <input type="text"  class="form-control tags" id="preConst_add_type" readonly>
                        <!-- <input type="text"  class="form-control tags" id="preConst_add_type" placeholder="공사개요를 입력해주세요"> -->
                    </div>  
                    <div class ="form-group preConst">
                        <label for="preConst_add_bid">금액(원)</label>
                        <input type="number" min="0" class="form-control tags" id="preConst_add_bid" readonly>
                        <!-- <input type="number" min="0" class="form-control tags" id="preConst_add_bid" placeholder="금액을 입력해주세요"> -->
                    </div>  
                    <div class="form-group preConst" style="text-align:right;">
                        <!-- <button type="button" class="btn btn-success" onclick='editSummary(this)'>수정</button> -->
                        <button class='btn btn-success' onclick='editSummary(this);' data-toggle="modal" data-target="#editSummaryModal">수정</button>

                    </div>
                </div>
                <div class="tab-pane pt-3 fade" id="icon-fieldSurvey" role="tabpanel" aria-labelledby="icon-fieldSurvey-tab">
                    <?php include "preConst_addList_fieldSurvey.php"; ?>
                </div>

                <div class="tab-pane pt-3 fade" id="icon-history" role="tabpanel" aria-labelledby="icon-history-tab">
                    <?php include "preConst_addList_history.php"; ?>
                </div>
                <div class="tab-pane pt-3 fade" id="icon-issues" role="tabpanel" aria-labelledby="icon-issues-tab">
                    <?php include "preConst_addList_issues.php"; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!--수정모달 -->
<div class="modal fade" id="editSummaryModal" tabindex="-1" role="dialog" aria-labelledby="editSummaryModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content SummaryModal">
            <div class="modal-header">
                <h5 class="modal-title" id="editSummaryModalTitle">현장조사 추가</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="editSummModal" id="editSummModal">
                    <div class="form-group ui-widget">
                        <label for="editTitle">현장명</label>
                        <input type="text" class="form-control tags" id="editTitle" value="성암로 포장 보수 공사" readonly>
                    </div>

                    <div class="form-group">
                        <label for="editAddress">지역명</label>
                        <input type="text" class="form-control tags" id="editAddress" value="서울시 마포구">
                    </div>
                    <div class="form-group">
                        <label for="editType">공사개요</label>
                        <input type="text" class="form-control tags" id="editType" value="포장도로 보수">
                    </div>
                    <div class="form-group">
                        <label for="editBid">금액(원)</label>
                        <input type="text" class="form-control tags" id="editBid" value="10000000">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="saveEditSummary()">Save</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $(".nav-link").click(function () {
            var stateObj = { tab: $(this).attr("href") };
            history.pushState(stateObj, $(this).text(), $(this).attr("href"));
        });


        // $.urlParam = function (name) {
        //     var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        //     return results[1] || 0;
        // }
        // $.urlParam('id'); 

        var url = window.location.href;
        var hashInx = url.indexOf("#");
        var hash = url.substring(hashInx + 1);
        // console.log('hashhashhashhashhashhashhash',hash);
        // console.log(hashInx);
        if (hashInx == -1) hash = "icon-summary";
        // if (hashInx == -1 || hashInx == 61) hash = "icon-live";

        $("#" + hash).addClass('show').addClass('active');
        $("#" + hash + "-tab").addClass("active");
    });

    var params = new URLSearchParams(location.search);
        var con_code = params.get('con_code');
       
        rpc("documents", "getConstructionbyConcode", {'con_code' : con_code}, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var area_name = contents[0].con_area_name;
            console.log("뭐가 나오나: ", contents[0]);

            $("#siteDetailId").text("[" + area_name + "] 사전조사 등록");
            $("#preConst_add_title").val(contents[0].con_area_name);
            $("#preConst_add_address").val(contents[0].con_field_name);
            $("#preConst_add_type").val("포장도로 보수");
            $("#preConst_add_bid").val(10000000);
        });


        function editSummary(){

        }

        function saveEditSummary(){
            var address = $("#editAddress").val();
            var type = $("#editType").val();
            var bid = $("#editBid").val();

            var result = confirm("수정된 정보를 저장하시겠습니까?");
            if(result) {
                 if(address==""){
                    alert("지역명을 입력해주세요.");
                 }else if(type==""){
                    alert("공사개요를 입력해주세요.");
                 }else if(bid==""){
                    alert("금액을 입력해주세요.");
                 }else{
                     alert("수정이 완료되었습니다.");
                 }
            }else{
                return;
            }

        }
    // function saveSummary(){
    //     var title = $("#preConst_add_title").val();
    //     var address = $("#preConst_add_address").val();
    //     var type = $("#preConst_add_type").val();
    //     var bid = $("#preConst_add_bid").val();
        
    //     if(title==''){
    //         alert("현장명을 입력해주세요.");
    //     }else if(address==''){
    //         alert('지역명을 입력해주세요.');
    //     }else if(type==''){
    //         alert('공사개요를 입력해주세요.');
    //     }else if(bid==''){
    //         alert('금액을 입력해주세요.');
    //     }else if(title !='' && address !='' && type !='' && bid !=''){
    //         var result = confirm("사전조사 개요정보를 추가하시겠습니까?");
    //         if(result) {
    //             alert("추가되었습니다.");
    //         }else{
    //             return;
    //         }
    //     }     

    // }
    
</script>