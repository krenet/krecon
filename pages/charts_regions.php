<script src="assets/plugins/charts/Chart.min.js"></script>  

    <div id="container" style="width: 100%;">    
		<canvas id="progress-canvas"></canvas>
	</div>

	<script>        		
		var color = Chart.helpers.color;
        
		var chartData = {
			labels: ['서울', '경기', '강원', '충청',"호남","영남"],
			datasets: [{
				label: '매출',
				backgroundColor: "rgba(255,0,0,0)",
				borderColor:  "rgba(255,0,0,0.5)",
				borderWidth: 3,
				data: [
					2200,
					1323,
					1546,
					2010,
					1890,
					1500

				]
			},{
				label: '수익',
				backgroundColor: "rgba(255,0,0,0)",
				borderColor:  "rgba(0,0,255,0.5)",
				borderWidth: 3,
				data: [
					600,
					423,
					546,
					410,
					690,
					300

				]
			}],
			options: {
				scales: {
					yAxes: [{
						ticks: {
							min: 1000,
							max: 3000
						}
					}]
				}
			}

		};

		window.onload = function() {
			var ctx = document.getElementById('progress-canvas').getContext('2d');
			window.myHorizontalBar = new Chart(ctx, {
				type: 'radar',
				data: chartData,
				options: {
					// Elements options apply to all of the options unless overridden in a dataset
					// In this case, we are setting the border of each horizontal bar to be 2px wide
					elements: {
						rectangle: {
							borderWidth: 2,
						}
					},
					responsive: true,
					legend: {
						position: 'right',
					},
					title: {
						display: true,
						text: '지역별 통계'
					},
				
					
					tooltips: {
						callbacks: {
							label: function(tooltipItem, data) {
								var label = data.datasets[tooltipItem.datasetIndex].label || '';

								if (label) {
									label += ': ';
								}
								label += numberWithCommas(Math.round(tooltipItem.yLabel * 100) / 100);
								return label;
							}
						}
					}
				}
			});

		};

	
	
		function numberWithCommas(n) {
			var parts=n.toString().split(".");
			return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
		}
	</script>