<?php        
    if(isset($_GET['id'])){
        $site_id = $_GET['id'];
    }else{
        //해당 ID 없음 페이지로 이동
        return;
    }
    include 'inc/db_setting.inc';    

    //msx 이미지 얻어오기
    $query = "select * from krecon_live_scene where `type`='msx' order by regtime desc limit 1;";            
    // 질의 수행
    $result = mysql_query($query);        
    $msx_row = null;
    if($row = mysql_fetch_assoc($result)){        
        $msx_row = $row;
    }    

    //visible 이미지 얻어오기
    $query = "select * from krecon_live_scene where `type`='visible' order by regtime desc limit 1;";            
    // 질의 수행
    $result = mysql_query($query);        
    $visible_row = null;
    if($row = mysql_fetch_assoc($result)){        
        $visible_row = $row;
    }    

    //krecon_live_scene 얻어오기
    $id=$_GET['id'];
    $query = "select * from krecon_live_scene 
                where con_code = '$id'
                order by regdate desc limit 1";
    
    // 질의 수행
    $result = mysql_query($query);        
    $live_info = null;
    if($row = mysql_fetch_assoc($result)){        
        $live_info = $row;
    }    

    //krecon_construction 얻어오기
    $id=$_GET['id'];
    $query = "select * from krecon_construction
                where con_code = '$id'";
    
    // 질의 수행
    $result = mysql_query($query);        
    $con_info = null;
    if($row = mysql_fetch_assoc($result)){        
        $con_info = $row;
    }
    //krecon_specification 얻어오기
    $id=$_GET['id'];
    $query = "select * from krecon_specification
                where con_code = '$id'";
    
    // 질의 수행
    $result = mysql_query($query);        
    $spec_info = null;
    if($row = mysql_fetch_assoc($result)){        
        $spec_info = $row;
    }

    //krecon_specification 얻어오기
    $id=$_GET['id'];
    $query = "select * from krecon_field_inspection
                where con_code = '$id'";
    
    // 질의 수행
    $result = mysql_query($query);        
    $field_inspec_info = null;
    if($row = mysql_fetch_assoc($result)){        
        $field_inspec_info = $row;
    }

    mysql_free_result($result);
    mysql_close($connect);

?>
<script>
    var latestCheckURL = "api.php?action=latest-live-img";
    var latestFilename = "<?php echo $msx_row['filename'];?>";
    var id = '';
    var gpsisLiveLat;
    var gpsisLiveLong;
    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }
    $.urlParam('id');

    var id_no = $.urlParam('id');
    $(document).ready(function () {
     

        $("#icon-live-tab").click(function () {
            setTimeout(renderLiveMap, 300);
        });
        $("#icon-live-tab").bind('load', function () {
            setTimeout(renderLiveMap, 300);
        });
        $("#icon-live-tab").trigger('load');

        // setTimeout(checkLiveStatus, 300);
        // checkLiveStatus();


        $('#field-check .check-btn').each(function () {
            var checkBtn = $(this).text();
            if (checkBtn == '확인전') {
                $(this).addClass('btn-primary');
            } else if (checkBtn == '확인완료') {
                $(this).addClass('btn-secondary');
                $(this).addClass('cursor-none');
            }
        });

        $('.check-btn').click(function () {
            var checkBtn = $(this).text();
            if (checkBtn == '확인완료') return;

            var result = confirm("현장검침 값을 확인완료하시겠습니까?");
            if (result) {
                alert('확인완료 되었습니다.');
            } else {
                return;
            }
            $.urlParam = function (name) {
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                return results[1] || 0;
            }
            $.urlParam('id');
            id_no = $.urlParam('id');
            var sendData = {
                id: id_no,
                editField: $(this).val(),
                editValue: '확인완료'
            };
            rpc("con-list-all", "editInspection", sendData, function (data) {
                if (data.result != "ok") {
                    alert("통신에 문제가 있습니다.");
                    return;
                }
                location.reload();
            });

        });
    });

    function renderLiveMap() {
        rpc("con-list-all", "getSiteDetailGps", { 'id': id_no }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }

            var contents = data.contents;
            gpsisLiveLat = contents[0].con_lat;
            gpsisLiveLong = contents[0].con_long;

            var mapContainer = document.getElementById('live-map'), // 지도를 표시할 div 
                mapOption = {
                    center: new daum.maps.LatLng(gpsisLiveLat, gpsisLiveLong), // 지도의 중심좌표
                    level: 5 // 지도의 확대 레벨
                };

            // 지도를 표시할 div와  지도 옵션으로  지도를 생성합니다
            map = new daum.maps.Map(mapContainer, mapOption);

            marker = new daum.maps.Marker({
                position: new daum.maps.LatLng(gpsisLiveLat, gpsisLiveLong),
                title: "시공현장",
                map: map
            });
            // console.log(gpsisLiveLat);
            // console.log(gpsisLiveLong);
        });
    }

    function checkLiveStatus() {
        myStopFunction();
        var mixLiveImgDiv = '';
        var consLiveImgDiv = '';
        var surfaceiveImgDiv = '';
        mixLiveImgDiv += '<video id="mixture-live-img" class="image-live card-image-top live-video"></video>';
            // '<div id="video-controls">' +
            // '<input type="button" id="btnPlay" value="Play" onclick="PlayNow()"/>' +
            // '<input type="button" id="btnPause" value="Pause" onclick="PauseNow()"/>' +
            // '<input type="button" id="btnStop" value="Stop" onclick="PlayStop()" />' +
            // '<input type="range" id="seekbar" value="0" onchange="ChangeTheTime()">' +
            // '<label id="lblTime"> 0:00:00</label>';
            // '</div>';
        consLiveImgDiv += ' <video id="cons-live-img" class="image-live card-image-top live-video" controls></video>';
        // '<div id="video-controls">' +
        //     '<input type="button" id="btnPlay" value="Play" onclick="PlayNow()"/>' +
        //     '<input type="button" id="btnPause" value="Pause" onclick="PauseNow()"/>' +
        //     '<input type="button" id="btnStop" value="Stop" onclick="PlayStop()" />' +
        //     '<input type="range" id="seekbar" value="0" onchange="ChangeTheTime()">' +
        //     '<label id="lblTime"> 0:00:00</label>';
        //     '</div>';
        surfaceiveImgDiv += ' <video id="surface-live-img" class="image-live card-image-top live-video" controls></video>';
        // '<div id="video-controls">' +
        //     '<input type="button" id="btnPlay" value="Play" onclick="PlayNow()"/>' +
        //     '<input type="button" id="btnPause" value="Pause" onclick="PauseNow()"/>' +
        //     '<input type="button" id="btnStop" value="Stop" onclick="PlayStop()" />' +
        //     '<input type="range" id="seekbar" value="0" onchange="ChangeTheTime()">' +
        //     '<label id="lblTime"> 0:00:00</label>';
        //     '</div>';
        $('#mixture-live-img').html(mixLiveImgDiv);
        $('#cons-live-img').html(consLiveImgDiv);
        $('#surface-live-img').html(surfaceiveImgDiv);

        $.ajax({
            url: latestCheckURL
        }).done(function (val) {
            var params = val.split(",");
            if (latestFilename != params[0]) {
                console.log("changed!" + latestFilename + " : " + params[0]);
                latestFilename = params[0];
                // console.log(params[1]);
                console.log('value : ', val);
                $("#ambient-temp-td").html(params[1] + "&deg;C");

                // //혼합물 온도 항목 갱신
                var mtet = $("#mixture-temp-error-td").text();
                mtet = mtet.substr(0, mtet.length - 3);
                if (mtet > 10) {
                    $("#mixture-temp-tr").removeClass("bg-success").addClass("bg-danger");
                } else {
                    $("#mixture-temp-tr").removeClass("bg-danger").addClass("bg-success");
                }

                // //표면 온도 항목 갱신
                var stet = $("#surface-temp-error-td").text();
                stet = stet.substr(0, stet.length - 3);
                if (stet > 10) {
                    $("#surface-temp-tr").removeClass("bg-success").addClass("bg-danger");
                } else {
                    $("#surface-temp-tr").removeClass("bg-danger").addClass("bg-success");
                }

                //기울기
                var iwet = $("#inst-width-error-td").text();
                iwet = iwet.substr(0, iwet.length - 1);
                if (iwet > 1) {
                    $("#inst-width-tr").removeClass("bg-success").addClass("bg-danger");
                } else {
                    $("#inst-width-tr").removeClass("bg-danger").addClass("bg-success");
                }

                // //레벨 항목 갱신
                var ilet = $("#inst-level-error-td").text();
                ilet = ilet.substr(0, ilet.length - 3);
                if (ilet > 1) {
                    $("#inst-level-tr").removeClass("bg-success").addClass("bg-danger");
                } else {
                    $("#inst-level-tr").removeClass("bg-danger").addClass("bg-success");
                }

                //시간 항목 갱신
                // $(".captured-time-text").html(params[6]);
            }
            // setTimeout(checkLiveStatus, 200);
        });
    }
    function checkLatestURL() {
        console.log('들어온다라이브');
        console.log($('#mixture-live-div'));
        var mixLiveImgDiv = '';
        var consLiveImgDiv = '';
        var surfaceiveImgDiv = '';
        mixLiveImgDiv += '<img id="mixture-live-img" src="live_min_img_copy/<?php echo $msx_row['filename'];?>"' +
            'class="image-live card-image-top" />';
        consLiveImgDiv += '<img id="cons-live-img" src="live_min_img_copy/<?php echo $visible_row['filename'];?>"' +
            'class="image-live card-image-top" />';
        surfaceiveImgDiv += '<img id="surface-live-img" src="live_min_img_copy/<?php echo $msx_row['filename'];?>"' +
            'class="image-live card-image-top" />';
        $('#mixture-live-div').html(mixLiveImgDiv);
        $('#cons-live-div').html(consLiveImgDiv);
        $('#surface-live-div').html(surfaceiveImgDiv);
        // console.log(liveImgDiv);

        $.ajax({
            url: latestCheckURL
        }).done(function (val) {
            var params = val.split(",");
            if (latestFilename != params[0]) {
                console.log("changed!" + latestFilename + " : " + params[0]);
                latestFilename = params[0];
                $("#mixture-live-img").attr("src", "live_min_img/" + latestFilename.replace('-visible-', '-msx-'));
                $("#cons-live-img").attr("src", "live_min_img_copy/" + latestFilename.replace('-msx-', '-visible-'));
                $("#surface-live-img").attr("src", "live_min_img_copy/" + latestFilename.replace('-visible-', '-msx-'));
                $("#ambient-temp-td").html(params[1] + "&deg;C");

                // //혼합물 온도 항목 갱신
                // var mixture_temp_error = eval("180-(" + params[2] + ")").toFixed(2);
                // $("#mixture-temp-td").html(params[2] + "&deg;C");
                // $("#mixture-temp-error-td").html(mixture_temp_error + "&deg;C");
                var mtet = $("#mixture-temp-error-td").text();
                mtet = mtet.substr(0, mtet.length - 3);
                if (mtet > 10) {
                    $("#mixture-temp-tr").removeClass("bg-success").addClass("bg-danger");
                } else {
                    $("#mixture-temp-tr").removeClass("bg-danger").addClass("bg-success");
                }


                // //표면 온도 항목 갱신
                // var surface_level_error = eval("165-(" + params[3] + ")").toFixed(2);
                // $("#surface-temp-td").html(params[3] + "&deg;C");
                // $("#surface-temp-error-td").html(surface_level_error + "&deg;C");
                var stet = $("#surface-temp-error-td").text();
                stet = stet.substr(0, stet.length - 3);
                if (stet > 10) {
                    $("#surface-temp-tr").removeClass("bg-success").addClass("bg-danger");
                } else {
                    $("#surface-temp-tr").removeClass("bg-danger").addClass("bg-success");
                }

                //기울기
                var iwet = $("#inst-width-error-td").text();
                iwet = iwet.substr(0, iwet.length - 1);
                if (iwet > 1) {
                    $("#inst-width-tr").removeClass("bg-success").addClass("bg-danger");
                } else {
                    $("#inst-width-tr").removeClass("bg-danger").addClass("bg-success");
                }

                // //레벨 항목 갱신
                // var inst_level_error = eval("2.6-(" + params[5] + ")").toFixed(2);
                // $("#inst-width-td").html(params[4] + "m");
                // $("#inst-level-td").html(params[5] + "&deg;");
                // $("#inst-level-error-td").html(inst_level_error + "&deg;");
                var ilet = $("#inst-level-error-td").text();
                ilet = ilet.substr(0, ilet.length - 3);
                if (ilet > 1) {
                    $("#inst-level-tr").removeClass("bg-success").addClass("bg-danger");
                } else {
                    $("#inst-level-tr").removeClass("bg-danger").addClass("bg-success");
                }

                //시간 항목 갱신
                $(".captured-time-text").html(params[6]);
                // $("#date-td").html(params[6].substring(0, 10));
                // $("#time-td").html(params[6].substring(11, 19));
            }

            setTimeOutFunc();
        });
    }
    var myVar;
    function setTimeOutFunc() {

        myVar = setTimeout(checkLatestURL, 300);
    }
    function myStopFunction() {
        console.log('이안으로들어오려나');
        clearTimeout(myVar);
    }

</script>
<script>
    $(document).ready(function () {

        $(".btn[data-target='#editFieldModal']").click(function () {
            var columnValues = $(this).parent().siblings().map(function () {
                return $(this).text();
            }).get();
            var checkCol = $(this).val();
            var checkVal = columnValues[2];

            $('#editFieldModal #editInspection').val(checkVal);
            $('#editFieldModal #editInspectionField').val(checkCol);

        });
        $('.modal-footer .btn-primary').click(function () {
            $('form[name="modalForm"]').submit();
        });


    });


</script>
<style>
    .image-live {
        /* padding: 5px; */
        width: 100%;
        height: auto;
    }

    .live-card-group h6 {
        margin-left: auto;
        margin-right: auto;
    }

    .live-card-group .card-footer {
        text-align: right;
    }

    .cursor-none {
        cursor: auto !important;
    }

    .drop_down_status {
        margin-left: 0.5rem !important;
    }

    /* 이렇게 하는게 아닌가 */
    #video-controls {
        background: #333;
        padding: 10px;
        margin-top : -0.5rem;
    }

    #btnStop {
        display: none;
    }

    #seekbar {
        width: 100%;
        margin-top: 0.5rem;
    }

    #mixture-live-div,
    #cons-live-div,
    #surface-live-div {
        padding: unset;
    }
</style>
<!-- <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=8635eb5a7465928c8d747aa7e5f3d6b6"></script> -->
<!-- <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=fb33fb2a39c5c870948bbe64f31deb8e"></script> -->

<div class="row">
    <div class="col-md-6 col-sm-6" style="margin-bottom:10px;display: inline-block;text-align: left;" id="livebtntest">

    </div>


</div>
<div class="row" style="margin-bottom:10px;">
    <div class="card col-md-6">
        <div class="card-header">
            <h6>현장 위치</h6>
        </div>
        <div class="card-body">
            <div id="live-map" style="width:100%;height:260px;"></div>
        </div>
    </div>
    <div class="card col-md-6">
        <div class="card-header">
            <h6>현장 정보</h6>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-responsive">
                <tbody>
                    <tr>
                        <th scope="row" class="bg-light text-center">날짜</th>
                        <td id="date-td">2019-08-30</td>
                    </tr>
                    <tr>
                        <th scope="row" class="bg-light text-center">시간</th>
                        <td id="time-td">-</td>
                    </tr>
                    <tr>
                        <th scope="row" class="bg-light text-center">차로</th>
                        <td id="lane-td">2</td>
                    </tr>
                    <tr>
                        <th scope="row" class="bg-light text-center">연장</th>
                        <td id="extension-td">20Km</td>
                    </tr>
                    <tr>
                        <th scope="row" class="bg-light text-center">대기 온도</th>
                        <td id="ambient-temp-td"><?php //echo $live_info['ambient_temp']; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row " style="margin-bottom:10px;" id="videoList">
    <!-- <div class="card-group live-card-group" id="finisher-middle-photo-area" style="margin-bottom:10px;"> -->
    <div class="card col-md-4">
        <div class="card-header">
            <h6 class="card-title">아스팔트 혼합물 열화상</h6>
        </div>
        <div class="card-body" id="mixture-live-div">
            <video id="mixture-live-img" class="image-live card-image-top live-video" controls>
                <source src="video/포장_열화상GIF.mp4" type="video/mp4">
            </video>
        </div>

        <div class="card-footer">
            <small class="text-muted captured-time-text"><?php echo $msx_row['regdate'];?></small>
        </div>
    </div>
    <div class="card col-md-4">
        <div class="card-header">
            <h6 class="card-title">시공영상</h6>
        </div>
        <div class="card-body" id="cons-live-div">

            <video id="cons-live-img" class="image-live card-image-top live-video" controls>
                <source src="video/시공영상.mp4" type="video/mp4">
            </video>
            <!-- <img id="cons-live-img" src="live_min_img_copy/<?php echo $visible_row['filename'];?>"
                class="image-live card-image-top" /> -->
        </div>
        <div class="card-footer">
            <small class="text-muted captured-time-text"><?php echo $visible_row['regdate'];?></small>
        </div>
    </div>
    <div class="card col-md-4">
        <div class="card-header">
            <h6 class="card-title">바닥면/대기온도 열화상</h6>
        </div>
        <div class="card-body" id="surface-live-div">
            <video id="surface-live-img" class="image-live card-image-top live-video" controls>
                <source src="video/시공영상_열화상.mp4" type="video/mp4">
            </video>
            <!-- <img id="surface-live-img" src="live_min_img_copy/<?php echo $msx_row['filename'];?>"
                class="image-live card-image-top" /> -->
        </div>
        <div class="card-footer">
            <small class="text-muted captured-time-text"><?php echo $msx_row['regdate'];?></small>
        </div>
    </div>
    <!-- </div> -->
</div>


<div class="row" id="alertDiv" style="margin-bottom:10px"></div>


<div class="row" id="finisher-bottom-row" style="margin-bottom:10px;">
    <div class="card col-md-6">
        <div class="card-header">
            <h6>측정 데이터 DB</h6>
        </div>
        <div class="card-body">
            <table class="table table-bordered" style="max-width:470px;" id="count-data-db">
                <thead class="bg-light text-center">
                    <tr>
                        <th scope="col">항목</th>
                        <th scope="col">설계</th>
                        <th scope="col">시행</th>
                        <th scope="col">오차</th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="mixture-temp-tr" class="text-white">
                        <th scope="row">혼합물 온도</th>
                        <td id="mixture-temp-design-td"><?php echo $spec_info['mixture_temp'] , '&deg C'; ?></td>
                        <td id="mixture-temp-td"><?php echo $live_info['mixture_temp'], '&deg C'; ?></td>
                        <td id="mixture-temp-error-td">
                            <?php echo $spec_info['mixture_temp']- $live_info['mixture_temp'] , '&deg C'; ?></td>
                    </tr>
                    <tr id="surface-temp-tr" class="text-white">
                        <th scope="row">바닥면 온도</th>
                        <td id="surface-temp-design-td"><?php echo $spec_info['surface_temp'], '&deg C'; ?></td>
                        <td id="surface-temp-td"><?php echo $live_info['surface_temp'], '&deg C'; ?></td>
                        <td id="surface-temp-error-td">
                            <?php echo $spec_info['surface_temp']-$live_info['surface_temp'], '&deg C'; ?></td>
                    </tr>
                    <tr id="inst-width-tr" class="text-white">
                        <th scope="row">포설폭</th>
                        <td id="inst-width-design-td"><?php echo $spec_info['inst_width'], 'm'; ?></td>
                        <td id="inst-width-td"><?php echo $live_info['inst_width'], 'm';?></td>
                        <td id="inst-width-error-td">
                            <?php echo $spec_info['inst_width']-$live_info['inst_width'], 'm'; ?></td>
                    </tr>
                    <tr id="inst-level-tr" class="text-white">
                        <th scope="row">레벨</th>
                        <td id="inst-level-design-td"><?php echo $spec_info['inst_level'], '&deg'; ?></td>
                        <td id="inst-level-td"><?php echo $live_info['inst_level'], '&deg'; ?></td>
                        <td id="inst-level-error-td">
                            <?php echo $spec_info['inst_level']-$live_info['inst_level'], '&deg'; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="card col-md-6">
        <div class="card-header">
            <h6>공정율 요약</h6>
        </div>
        <div class="card-body">
            <table class="table table-bordered" style="max-width:400px;" id="progress-rate-sum">
                <thead class="bg-light text-center">
                    <tr>
                        <th scope="col">단계</th>
                        <!-- <th scope="col">목표</th> -->
                        <!-- <th scope="col">시행</th> -->
                        <th scope="col">진행</th>
                    </tr>
                </thead>
                <tbody id="taskOrder">
                    <tr>
                        <th scope="row">보조기층포설</th>
                        <td>500m</td>
                        <td>500m</td>
                        <td>100%</td>
                    </tr>
                    <tr>
                        <th scope="row">기층포설</th>
                        <td>500m</td>
                        <td>500m</td>
                        <td>100%</td>
                    </tr>
                    <tr>
                        <th scope="row">다짐1</th>
                        <td>3회</td>
                        <td>3회</td>
                        <td>100%</td>
                    </tr>
                    <tr>
                        <th scope="row">다짐2</th>
                        <td>3회</td>
                        <td>1회</td>
                        <td>33%</td>
                    </tr>
                    <tr>
                        <th scope="row">총진행율</th>
                        <th colspan="3" style="text-align:right;">85%</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>



<!-- Form Modal -->
<div class="modal fade" id="makeReportModal" tabindex="-1" role="dialog" aria-labelledby="makeReportModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="makeReportModalTitle">보고서 생성</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="makeReportInputTitle">보고서제목</label>
                        <input type="text" class="form-control" id="makeReportInputTitle" placeholder="보고서제목을 입력해 주세요">
                    </div>
                    <div class="form-group">
                        <label for="makeReportInputEmail1">작성자</label>
                        <input type="email" class="form-control" id="makeReportInputName" placeholder="작성자를 입력해주세요">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">보고서 생성</button>
            </div>
        </div>
    </div>
</div>

<script>

    var id_no = '';
    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }
    $.urlParam('id');
    // console.log($.urlParam('id'));

    id_no = $.urlParam('id');
    function editInspection() {

        var result = confirm("현장검침 값을 수정하시겠습니까?");
        if (result) {
            alert('수정되었습니다.');
        } else {
            return;
        }
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        $.urlParam('id');
        // console.log($.urlParam('id'));

        id_no = $.urlParam('id');
        // console.log(id);
        var sendData = {
            id: id_no,
            editField: $('#editInspectionField').val(),
            editValue: $('#editInspection').val()
        };
        // console.log(sendData);

        rpc("con-list-all", "editInspection", sendData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            location.reload();
        });
    }

    function selectEquipmentBtn(obj) {
        // console.log('ddddd세숀확인', id);
        // console.log('ddddd세숀확인', $(obj));
        // console.log($(obj)[0].attributes.data.nodeValue);
        var checkStatus = $(obj)[0].attributes.data.nodeValue;
        console.log(checkStatus);
        if (checkStatus == 'convert_ready') {
            setTimeout(checkLiveStatus, 300);
            var readyDiv;
            readyDiv = "<div class='card' style='width:100%'><div class='card-header'><div class='card-body' style='text-align:center'>시공영상을 변환대기 중 입니다.</div></div></div>"
            $('#alertDiv').css('display','block');
            $('#videoList').css('display','none');
            $('#video-controls').css('display','none');
            $('#alertDiv').html(readyDiv);
            // alert('변환대기중입니다.');
        } else if (checkStatus == 'converting') {
            setTimeout(checkLiveStatus, 300);
            var convertingDiv;
            convertingDiv = "<div class='card' style='width:100%'><div class='card-header'><div class='card-body' style='text-align:center'>시공영상 변환 중 입니다.</div></div></div>"
            $('#alertDiv').css('display','block');
            $('#videoList').css('display','none');
            $('#video-controls').css('display','none');
            $('#alertDiv').html(convertingDiv);
        } else if (checkStatus == 'converted') {
            console.log('converted');
            // setTimeout(checkLiveStatus, 300);
            $('#alertDiv').css('display','none');
            $('#videoList').css('display','flex');
            $('#video-controls').css('display','block');

            // checkLiveStatus();
        } else if (checkStatus == 'live') {
            console.log('live');
            setTimeout(checkLatestURL, 400);
        }
        var sendData = {
            id: id_no,
            name: $(obj)[0].name
        };
        console.log(sendData);
        // console.log($(obj)[0].attributes.data.nodeValue);
        // console.log($(obj)[0].attributes);
        // console.log($(obj)[0].innerText);
        rpc("con-list-all", "selectEquipmentBtn", sendData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var countDataDb;

            //현장정보 시작
            var regdate;
            var nowCrossroad;
            var extension;
            var ambient_temp;
            var equipLocationLat;
            var equipLocationLong;
            regdate = contents[0].regdate;
            if (regdate == null) {
                $('#date-td').html('-');
            } else {
                realRegdate = regdate.substring(0, 10);
                $('#date-td').html(realRegdate);
            }
            regtime = regdate.substring(11,16);
            console.log('regtime',regtime);
            $('#time-td').html(regtime);
            // $('#time-td').html('-');
            nowCrossroad = contents[0].now_crossroad;
            $('#lane-td').html(nowCrossroad);
            extension = contents[0].extension;
            $('#extension-td').html(extension);
            ambient_temp = contents[0].ambient_temp;

            $('#ambient-temp-td').html(ambient_temp);
            equipLocationLat = contents[0].equip_location_latitude;
            equipLocationLong = contents[0].equip_location_longitude;
            //현장정보끝

            //측정DB시작
            var mix;
            var surf;
            var instWidth;
            var instLev;
            mix = contents[0].mixture_temp;
            mix = Number(mix);
            surf = contents[0].surface_temp;
            surf = Number(surf);
            instWidth = contents[0].inst_width;
            instWidth = Number(instWidth);
            instLev = contents[0].inst_level;
            instLev = Number(instLev);
            // 혼합물온도  
            $('#mixture-temp-td').html(mix + "&deg;C");
            // 바닥면온도  
            $('#surface-temp-td').html(surf + "&deg;C");
            // 포설폭  
            $('#inst-width-td').html(instWidth + "m");
            // 레벨  
            $('#inst-level-td').html(instLev + "&deg;");

            var errorMix;
            var errorSurf;
            var errorInstWidth;
            var errorInstLev;

            var designMix = $('#mixture-temp-design-td')[0].innerText;
            designMix = designMix.substr(0, designMix.length - 3);
            designMix = Number(designMix);

            var designSurf = $('#surface-temp-design-td')[0].innerText;
            designSurf = designSurf.substr(0, designSurf.length - 3);
            designSurf = Number(designSurf);

            var designInstWidth = $('#inst-width-design-td')[0].innerText;
            designInstWidth = designInstWidth.substr(0, designInstWidth.length - 1);
            designInstWidth = Number(designInstWidth);

            var designInstLev = $('#inst-level-design-td')[0].innerText;
            designInstLev = designInstLev.substr(0, designInstLev.length - 1);
            designInstLev = Number(designInstLev);

            errorMix = designMix - mix + "&deg;C";
            $('#mixture-temp-error-td').html(errorMix);
            errorSurf = designSurf - surf + "&deg;C";
            $('#surface-temp-error-td').html(errorSurf);
            errorInstWidth = (designInstWidth - instWidth).toFixed(2) + "m";
            $('#inst-width-error-td').html(errorInstWidth);
            errorInstLev = (designInstLev - instLev).toFixed(2) + "&deg;";
            $('#inst-level-error-td').html(errorInstLev);
            //측정DB끝
            console.log('site_detail_live내부 selectEquipFunction들어옴');
            getVideoFile(contents[0].session_id);

        });

    }

    //여기 내가 건드림

    var vid = document.getElementById("mixture-live-img");
    console.log(vid);
    // var playButton = document.getElementById("play-pause")
    var seekBar = document.getElementById("seekbar");

    var nowTime = 0;
    var it;


    window.onload = function () {
        vid.addEventListener('timeupdate', UpdateTheTime, false);
        vid.addEventListener('durationchange', SetSeekBar, false);
    }

    function SetSeekBar() {
        seekbar.min = 0;
        seekbar.max = vid.duration;
        console.log("seekbar.max = ", vid.duration);
    }

    function ChangeTheTime() {
        vid.currentTime = seekbar.value;
        console.log("vid.currentTime = ", vid.currentTime);
    }

    function UpdateTheTime() {
        var sec = vid.currentTime;
        var h = Math.floor(sec / 3600);
        sec = sec % 3600;
        var min = Math.floor(sec / 60);
        sec = Math.floor(sec % 60);
        if (sec.toString().length < 2) sec = "0" + sec;
        if (min.toString().length < 2) min = "0" + min;
        document.getElementById('lblTime').innerHTML = h + ":" + min + ":" + sec;
        seekbar.min = vid.startTime;
        seekbar.max = vid.duration;
        seekbar.value = vid.currentTime;

        nowTime = parseInt(seekbar.value);
        //지금은 세션 아이디지만 다른 방법을 생각해봐야됨. 아니다 세션아이디로 해도됨!!
        var jsonLocation = '/190.json';
        $.getJSON(jsonLocation, function (data) {
            mixTemp = Number(data[nowTime].ambient_temp);
            mixTemp = mixTemp.toFixed(2);
            console.log(mixTemp);
            // data[nowTime].mixture_temp = (data[nowTime].mixture_temp).toFixed(2);
            $('#ambient-temp-td').html(mixTemp);
            
        });

    }

    function PlayNow() {
        console.log('PlayNow들어온다');
        if (vid.paused) {
            vid.play();
        } else if (vid.ended) {
            vid.currentTime = 0;
            vid.play();
        }
    }

    // fires when Pause button is clicked
    function PauseNow() {
        console.log('PauseNow들어온다');
        if (vid.play) {
            vid.pause();
        }
    }

    function PlayStop() {
        console.log('PlayStop');
        vid.pause();
        vid.currentTime = 0;
    }



</script>












<!-- var vid = document.getElementById("mixture-live-img");
    var playButton = document.getElementById("play-pause")
    var seekBar = document.getElementById("seek-bar");
    var currentTime;
    var nowTime = 0;

    //플레이 버튼을 눌러야만 식바가 생기게 하면 될듯
    playButton.addEventListener("click", function () {

        if (vid.paused == true) {
            // Play the video
            vid.play();

            currentTime = vid.currentTime;
            var jsonLocation = '/jsonTest.json';
            var mixture_temp = new Array();
            var regdate = new Array();
            var ambient_temp = new Array();
            var surface_temp = new Array();
            var inst_width = new Array();
            var jsonData = new Array();

            $.getJSON(jsonLocation, function (data) {

            jsonData = data;
                

                // var i = 0;
                var i = nowTime;
                var it = setInterval(function () {
                    if (i++ < data.length) {
                        //반복할 실행문
                        mixture_temp.push(data[i].mixture_temp);
                        regdate.push(data[i].regdate);

                        $('#ambient-temp-td').html(data[i].mixture_temp);
                    } else {
                        clearInterval(it);
                        //이후 실행문
                    }
                }, 1000);

            });
            // Update the button text to 'Pause'
            playButton.innerHTML = "일시정지";
        } else {
            // Pause the video
            vid.pause();

            // Update the button text to 'Play'
            playButton.innerHTML = "재생";
        }


        // 재생바가 있는 위치로 화면 이동
        seekBar.addEventListener("change", function () {
            // Calculate the new time
            var time = vid.duration * (seekBar.value / 100);
            // Update the video time
            vid.currentTime = time;

            nowTime = parseInt(time);
            $('#ambient-temp-td').html(jsonData[nowTime].mixture_temp);
            
            
        });

    }); -->
