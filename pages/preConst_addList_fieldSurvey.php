<style>
    .fs-remove, .fs-modify {
        display: none;
    }

    .fs_num {
        width: 5%;
    }

    .fs_title {
        width: 25%;
    }

    .fs_content {
        width: 35%;
    }
    
    .fs_fileName {
        width: 15%;
    }

    .fs_edit {
        width: 10%;
    }

    .fs_delete {
        width: 10%;
    }

    #btn-addFiles{
        position: absolute;
        right: 16px;
        font-size: 25px;
        background: #7dbcff;
        color: white;
        border-radius: 50%;
        height: 30px;
        width: 30px;
        line-height: 25px;
    }
</style>

<table class="table table-bordered site-docu-all table-responsive">
    <thead>
        <tr class="bg-light text-center">
            <th class="fs_num">번호</th>
            <th class="fs_title">제목</th>
            <th class="fs_content">요약</th>
            <th class="fs_fileName">첨부파일명</th>
            <th class="fs_edit">수정</th>
            <th class="fs_delete">삭제</th>
        </tr>
    </thead>
    <tbody id="krecon_document" style="text-align:center;">
        <tr class="text-center">
            <td id="fs_num">1</td>
            <td id="fs_title"></td>
            <td id="fs_content"></td>
            <td id="fs_fileName"></td>
            <td><button class='btn btn-success btn-sm'  id="fs_editBtn" style="display: none" onclick='modifyFieldSurvey(this);' data-toggle="modal" data-target="#editFieldModal">수정</button></td>
            <td><button class='btn btn-danger btn-sm' id="fs_deleteBtn" style="display: none" onclick='clickDelete(this);'>삭제</td>
        </tr>
    </tbody>
</table>  
<div class="card-text" style="text-align:right;">
    <button class="btn btn-primary " data-toggle="modal" data-target="#addFieldModal">추가</button>
</div>

<!--추가모달 -->
<div class="modal fade" id="addFieldModal" tabindex="-1" role="dialog" aria-labelledby="addFieldModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content fieldSurveyModal">
            <div class="modal-header">
                <h5 class="modal-title" id="addFieldModalTitle">현장조사 추가</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="addFieldSurveyModal" id="addFieldSurveyModal">
                    <div class="form-group ui-widget">
                        <label for="addFieldTitle">제목</label>
                        <input type="text" class="form-control tags" id="addFieldTitle" placeholder="제목을 입력해주세요.">
                    </div>

                    <div class="form-group">
                        <label for="addFieldContent">요약</label>
                        <textarea class="form-control tags" id="addFieldContent" placeholder="요약 내용을 입력해주세요." cols="30" rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="addFieldFile">파일첨부</label>
                        <input multiple="multiple" type="file" class="form-control-file" id="addFieldFile">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addFieldSurvey()">Save</button>
            </div>
        </div>
    </div>
</div>

<!--수정모달 -->
<div class="modal fade" id="editFieldModal" tabindex="-1" role="dialog" aria-labelledby="editFieldModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content fieldSurveyModal">
            <div class="modal-header">
                <h5 class="modal-title" id="addFieldModalTitle">현장조사 수정</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="editFieldSurveyModal" id="editFieldSurveyModal">
                    <div class="form-group ui-widget">
                        <label for="editFieldTitle">제목</label>
                        <input type="text" class="form-control tags" id="editFieldTitle">
                    </div>

                    <div class="form-group">
                        <label for="editFieldContent">요약</label>
                        <textarea class="form-control tags" id="editFieldContent" cols="30" rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="editFieldFile">파일첨부</label> 
                        <button id="btn-addFiles">+</button>
                        <table class="table table-bordered site-docu-all table-responsive">
                            <thead>
                                <tr class="bg-light text-center">
                                    <th class="fs_fileName">첨부파일명</th>
                                    <th class="fs_delete">삭제</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-editFile" style="text-align:center;">
                                <tr class="text-center">
                                    <td class="savedFileName"></td>
                                    <td><input type='button' class='btn btn-danger btn-sm' value="삭제" onclick='deleteFile(this);'></td>
                                </tr>
                            </tbody>
                        </table>  
                        <input multiple="multiple" type="file" class="form-control-file" id="editFieldFile" style="display:none" onchange="changeValue(this)">
                        <!-- <button type="button" class="btn" id="btn-addFiles">파일 추가</button> -->
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="editFieldSurvey()">Save</button>
            </div>
        </div>
    </div>
</div>

<script>
     $(document).ready(function(){
        $('#btn-addFiles').click(function(e){
            e.preventDefault();
            $('#editFieldFile').click();
        });
        var params = new URLSearchParams(location.search);
        var title = params.get('title');
        var content = params.get('content');
        var fileName = params.get('fileName');
        $("#fs_title").text(title);
        $("#fs_content").text(content);
        $("#fs_fileName").text(fileName);
        if(title !=null){
            $("#fs_editBtn").css('display', '');
            $("#fs_deleteBtn").css('display', '');
        }
    });

    function addFieldSurvey() {
        var params = new URLSearchParams(location.search);
        var con_code = params.get('con_code');
       

        if(title==''){
            alert("제목을 입력해주세요.");
        }else if(content==''){
            alert('요약 내용을 입력해주세요.');
        }else if(title!='' && content!=''){
            var result = confirm("현장조사 정보를 추가하시겠습니까?");
            if(result) {
                var title = $('#addFieldTitle').val();
                var content = $('#addFieldContent').val();
                var filePath= $("#addFieldFile").val();
                filePath = filePath.split("\\");
                var fileName = filePath[filePath.length-1];
                // console.log("file: ", f_name);
                alert("추가되었습니다.");
                location.href=".?cat=preConst&page=preConst_addList&con_code="+con_code+"&title="+title+"&content="+content+"&fileName="+fileName+"#icon-fieldSurvey";
                // opener.document.getElementById("fs_title").text = document.getElementById(title);
                // $("#fs_title").text("1");
                // $("#fs_title").text("1");
            }else{
                return;
            }
        }     
    }

    function clickDelete(obj) {
        var result = confirm("해당 리스트 정보를 삭제하시겠습니까?");
        if(result) {
            alert('삭제되었습니다.');
        }else{
            return;
        }
    }

    function modifyFieldSurvey(obj) {
        var title =  $('#fs_title').text();
        var content =  $('#fs_content').text();
        var file =  $('#fs_fileName').text();

        console.log("t: ", title);
        console.log("c: ", content);
        console.log("f: ", file);


        $('#editFieldTitle').val(title);
        $('#editFieldContent').val(content);
        $('.savedFileName').text(file);
    }

    function changeValue(obj){
        // alert(obj.value);
        var str = (obj.value).split("\\");
        var fileName = str[str.length-1];
        console.log("fileName: ", fileName);
        // $('#tbody-editFile').append(
        //     <tr class="text-center">
        //         <td class="selectedFileName">$fileName</td>
        //         <td><button class='btn btn-danger btn-sm' onclick='clickDelete(this);'>삭제</td>
        //     </tr>
        // )
    }

    function deleteFile(obj){
        var result = confirm("해당 파일을 삭제하시겠습니까?");
        if(result) {
            alert('삭제되었습니다.');
        }else{
            return;
        }
    }

    function editFieldSurvey(){
        var result = confirm("수정된 정보를 저장하시겠습니까?");
        if(result) {
            alert('저장되었습니다.');
        }else{
            return;
        }
    }
</script>