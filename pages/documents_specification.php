<style>
    .document-preview-frame {
        width: 100%;
        height: 800px;
    }

    .button-td {
        width: 70px;
    }

    .specificationRow {
        display: flex;
        flex-wrap: wrap;
    }

    .doc-num {
        width: 10%;
    }
    .doc-title {
        width: 60%;
    }
    .doc-detail {
        width: 15%;
    }
    .doc-add {
        width: 15%;
    }
    .doc-download {
        width: 15%;
    }
    .btn-document-list{
        /* display: none; */
        padding-right: 50px;
        text-align: right;
    }
</style>
<script>
    $(document).ready(function () {
        // getSpecificationList();
        $(".btn-document-list").css("display", "none");
        rpc("documents", "conList", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            // var contents = data.contents;
            // var item;
            // var options = "";

            // options += "<option value='all'>전체</option>";

            // for (inx = 0; inx < contents.length; ++inx) {
            //     item = contents[inx];
            //     options += "<option value=" + item.con_code + ">" + item.con_area_name + "</option>";

            // }
            // $("#select-button-by-code").html(options);

            var contents = data.contents;
            var item;
            var doc = "";
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                console.log("code: ", item.con_code);
                doc += "<tr style='text-align:center'>";
                doc += "<td>" + (inx + 1) + "</td>";
                doc += "<td style = 'text-align: left;'>" + item.con_area_name + "</td>";
                // doc += "<td>" + item.doc_classify + "</td>";
                doc += "<td class='button-td'><button class='btn btn-success btn-sm' value=" + item.con_code + " onclick='documentDetailBtn(this);'>자세히</button></td>";
                doc += "<td class='button-td'><button class='btn btn-primary btn-sm' value=" + item.con_code + " onclick='documentAddBtn(this);'>등록</button></td>";
                doc += "</tr>";
            }
            $("#krecon_document_specifications").html(doc);

        });

        // $("#select-button-by-code").change(function () { 
        //     con_code = $(this).val();
        //     console.log(con_code);
        //     if (con_code == 'all') {
        //         getSpecificationList();
        //     } else {
        //         rpc("documents", "getSpecificationListbyConcode", {'con_code' : con_code}, function (data) {
        //             if (data.result != "ok") {
        //                 alert("통신에 문제가 있습니다.");
        //                 return;
        //             }
        //             var contents = data.contents;
        //             var item;
        //             var doc = "";
        //             for (inx = 0; inx < contents.length; ++inx) {
        //                 item = contents[inx];
        //                 doc += "<tr style='text-align:center'>";
        //                 doc += "<td>" + (inx + 1) + "</td>";
        //                 doc += "<td style = 'text-align: left;'>" + item.doc_title + "</td>";
        //                 // doc += "<td>" + item.doc_category + "</td>";
        //                 doc += "<td class='button-td'><button class='btn btn-primary btn-sm' onclick='documentPreviewBtn(this);'data-file='/documents/" + item.doc_file_name + ".pdf'>미리보기</button></td>";
        //                 doc += "<td class='button-td'><a class='btn btn-success btn-sm' onclick='documentDownloadBtn(this);'data-file='/documents/" + item.doc_file_name + ".pdf'>다운로드</a></td>";
        //                 doc += "</tr>";
        //             }
        //             $("#krecon_document_specifications").html(doc);
        //         });
        //     }
        // });
    });

    // function getSpecificationList() {
    //     rpc("documents", "specifications", null, function (data) {
    //         if (data.result != "ok") {
    //             alert("통신에 문제가 있습니다.");
    //             return;
    //         }
    //         var contents = data.contents;
    //         var item;
    //         var doc = "";
    //         for (inx = 0; inx < contents.length; ++inx) {
    //             item = contents[inx];
    //             doc += "<tr style='text-align:center'>";
    //             doc += "<td>" + (inx + 1) + "</td>";
    //             doc += "<td style = 'text-align: left;'>" + item.doc_title + "</td>";
    //             // doc += "<td>" + item.doc_classify + "</td>";
    //             doc += "<td class='button-td'><button class='btn btn-primary btn-sm' onclick='documentPreviewBtn(this);'data-file='/documents/" + item.doc_file_name + ".pdf'>미리보기</button></td>";
    //             doc += "<td class='button-td'><a class='btn btn-success btn-sm' onclick='documentDownloadBtn(this);'data-file='/documents/" + item.doc_file_name + "'>다운로드</a></td>";
    //             doc += "</tr>";
    //         }
    //         $("#krecon_document_specifications").html(doc);

    //     });
    // }

    function documentDetailBtn(obj){
        console.log("obj: ", obj.value);
        var con_code = obj.value;
        rpc("documents", "getConstructionbyConcode", {'con_code' : con_code}, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var area_name = contents[0].con_area_name;

            $("#document_sub_title").text("[" + area_name + "] 시방서 리스트");
            $(".doc-download").css("display", "");
            $(".doc-add").css("display", "none");
            $(".btn-document-list").css("display", "");
        });
        rpc("documents", "getSpecificationListbyConcode", {'con_code' : con_code}, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var doc = "";
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                doc += "<tr style='text-align:center'>";
                doc += "<td>" + (inx + 1) + "</td>";
                doc += "<td style = 'text-align: left;'>" + item.doc_title + "</td>";
                // doc += "<td>" + item.doc_category + "</td>";
                doc += "<td class='button-td'><button class='btn btn-primary btn-sm' onclick='documentPreviewBtn(this);'data-file='/documents/" + item.doc_file_name + ".pdf'>미리보기</button></td>";
                doc += "<td class='button-td'><a class='btn btn-success btn-sm' onclick='documentDownloadBtn(this);'data-file='/documents/" + item.doc_file_name + ".pdf'>다운로드</a></td>";
                doc += "</tr>";
            }
            $("#krecon_document_specifications").html(doc);
        });
    }

    function documentAddBtn(obj) {
        var con_code = obj.value;
        location.href=".?cat=preConst&page=preConst_addList&con_code="+con_code;
    }

    function documentPreviewBtn(obj) {
        var file = $(obj)[0].dataset.file;
        console.log('파일명',file);
        $("#document-preview-div-specification").html('<iframe class="document-preview-frame" src="pdf_viewer/web/viewer.html?file=' + file + '" ></iframe>');
    }

    function documentDownloadBtn(obj) {
        var file = $(obj)[0].dataset.file;
        $(obj).attr({ target: '_blank', href: file });
        $('#download-frame').attr("src", file);
    }

    function goDocumentList(obj) {
        location.href=".?cat=documents&page=documents_specification";
    }

</script>


<div class="card card-default" data-scroll-height="675">
    <div class="card-header">
        <h2 id="document_sub_title">시방서 리스트</h2>   
        <!-- <div class="dropdown d-inline-block mb-1" style="margin-left:10px">
            <label for="select-button-by-code">공사 선택</label>
            <select class="form-control" id="select-button-by-code">
    
            </select>
        </div> -->
    </div>
    <div class='btn-document-list'>
        <button type='button' class='btn btn-secondary' onclick='goDocumentList(this);'>목록</button>
    </div> 
    
    <div class="card-body slim-scroll">
        <table class="table table-bordered table-responsive">
            <thead>
                <tr class="bg-light text-center">
                    <th class="doc-num">번호</th>
                    <th class="doc-title">공사명</th>
                    <!-- <th>구분</th> -->
                    <th class="doc-detail">자세히</th>
                    <th class="doc-add">사전조사 등록</th>
                    <th class="doc-download" style="display: none;">다운로드</th>
                </tr>
            </thead>
            <tbody id="krecon_document_specifications">
                <!-- <tr class="text-center">
                    <td>9</td>
                    <td>지방도619호 용동도로 포장도 보수공사</td>
                    <td>설계설명서</td>
                    <td class="button-td"><button class="btn btn-primary btn-sm document-preview-btn"
                            data-file="/documents/지방도619호용동도로포장도보수공사설계설명서.pdf">미리보기</button></td>
                    <td class="button-td"><a class="btn btn-success btn-sm document-download-btn"
                            data-file="/documents/지방도619호용동도로포장도보수공사설계설명서.pdf">다운로드</a></td>
                </tr> -->
            </tbody>
        </table>
    </div>
</div>
<div id="document-preview-div-specification">
</div>