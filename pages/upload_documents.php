<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: X-PINGOTHER, Content-Type, enctype");

include '../inc/util.php';
include '../inc/db_setting.inc';    

if(isset($_FILES['upfile']) && $_FILES['upfile']['name'] != "") {

    $file = $_FILES['upfile'];
    $title = $_POST['addDocTitle'];
    $classify = $_POST['addDocSelect'];
    $con_code = $_POST['docConCode'];
        
    $upload_directory = '/datas/krecon/pages/uploads/';

    $ext_str = "hwp,xls,doc,xlsx,docx,pdf,jpg,gif,png,txt,ppt,pptx,mp4";

    $allowed_extensions = explode(',', $ext_str);

    $max_file_size = 5242880;

    $ext = substr($file['name'], strrpos($file['name'], '.') + 1);

    // 확장자 체크

    if(!in_array($ext, $allowed_extensions)) {

        echo "업로드할 수 없는 확장자 입니다.";

    }

    // 파일 크기 체크

    if($file['size'] >= $max_file_size) {

        echo "5MB 까지만 업로드 가능합니다.";

    }

    // 파일 이름 (자릿수+파일이름) 
    do{
        $file_name = makeName(10,'doc_').'.'.$ext;
        
        if(!file_exists($file_name)) {

            break;
        }

    }while(true);

    // $path = md5(microtime()) . '.' . $ext;

    if(move_uploaded_file($file['tmp_name'], $upload_directory.$file_name)) {

        $query = sprintf("INSERT INTO krecon_document (
                            doc_category, doc_title, doc_file_name, doc_saved_file_name,
                            con_code, doc_upload_date) VALUES('%s','%s','%s','%s','%s',now())",
                        $classify,$title,$file['name'],$file_name, $con_code);
        mysql_query($query);
 
    }
    
} else {
        

}

mysql_close();

?>
<script>
alert("업로드가 완료 됐습니다.");
location.href="<?php echo $_REQUEST['callBackPage'];?>";
</script>

