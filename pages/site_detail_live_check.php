<?php        
    if(isset($_GET['id'])){
        $site_id = $_GET['id'];
    }else{
        //해당 ID 없음 페이지로 이동
        return;
    }
    include 'inc/db_setting.inc';    


    //krecon_live_scene 얻어오기
    $id=$_GET['id'];
    $query = "select * from krecon_live_scene 
                where con_code = '$id'
                order by regdate desc limit 1";
    
    // 질의 수행
    $result = mysql_query($query);        
    $live_info = null;
    if($row = mysql_fetch_assoc($result)){        
        $live_info = $row;
    }    

    //krecon_construction 얻어오기
    $id=$_GET['id'];
    $query = "select * from krecon_construction
                where con_code = '$id'";
    
    // 질의 수행
    $result = mysql_query($query);        
    $con_info = null;
    if($row = mysql_fetch_assoc($result)){        
        $con_info = $row;
    }
    //krecon_specification 얻어오기
    $id=$_GET['id'];
    $query = "select * from krecon_specification
                where con_code = '$id'";
    
    // 질의 수행
    $result = mysql_query($query);        
    $spec_info = null;
    if($row = mysql_fetch_assoc($result)){        
        $spec_info = $row;
    }

    //krecon_specification 얻어오기
    $id=$_GET['id'];
    $query = "select * from krecon_field_inspection
                where con_code = '$id'";
    
    // 질의 수행
    $result = mysql_query($query);        
    $field_inspec_info = null;
    if($row = mysql_fetch_assoc($result)){        
        $field_inspec_info = $row;
    }

    mysql_free_result($result);
    mysql_close($connect);

?>
<script>
    var latestCheckURL = "api.php?action=latest-live-img";
    var latestFilename = "<?php echo $msx_row['filename'];?>";
    var id = '';
    var gpsisLiveLat;
    var gpsisLiveLong;
    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }
    $.urlParam('id');

    var id_no = $.urlParam('id');
    $(document).ready(function () {
     
               $('#field-check .check-btn').each(function () {
            var checkBtn = $(this).text();
            if (checkBtn == '확인전') {
                $(this).addClass('btn-primary');
            } else if (checkBtn == '확인완료') {
                $(this).addClass('btn-secondary');
                $(this).addClass('cursor-none');
            }
        });

        $('.check-btn').click(function () {
            var checkBtn = $(this).text();
            if (checkBtn == '확인완료') return;

            var result = confirm("현장검침 값을 확인완료하시겠습니까?");
            if (result) {
                alert('확인완료 되었습니다.');
            } else {
                return;
            }
            $.urlParam = function (name) {
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                return results[1] || 0;
            }
            $.urlParam('id');
            id_no = $.urlParam('id');
            var sendData = {
                id: id_no,
                editField: $(this).val(),
                editValue: '확인완료'
            };
            rpc("con-list-all", "editInspection", sendData, function (data) {
                if (data.result != "ok") {
                    alert("통신에 문제가 있습니다.");
                    return;
                }
                location.reload();
            });

        });
    });

 
</script>
<script>
    $(document).ready(function () {

        $(".btn[data-target='#editFieldModal']").click(function () {
            var columnValues = $(this).parent().siblings().map(function () {
                return $(this).text();
            }).get();
            var checkCol = $(this).val();
            var checkVal = columnValues[2];

            $('#editFieldModal #editInspection').val(checkVal);
            $('#editFieldModal #editInspectionField').val(checkCol);

        });
        $('.modal-footer .btn-primary').click(function () {
            $('form[name="modalForm"]').submit();
        });


    });


</script>
<style>
    .image-live {
        /* padding: 5px; */
        width: 100%;
        height: auto;
    }

    .live-card-group h6 {
        margin-left: auto;
        margin-right: auto;
    }

    .live-card-group .card-footer {
        text-align: right;
    }

    .cursor-none {
        cursor: auto !important;
    }

    .drop_down_status {
        margin-left: 0.5rem !important;
    }
</style>

<div class="row card" style="margin-bottom:10px;">
    <div class="card-header">
            현장확인
    </div>
    <div class="card-body">
        <table class="table table-bordered table-responsive" id="field-check">
            <thead>
                <tr class="bg-light text-center">
                    <th>단계</th>
                    <th>지침</th>
                    <th>현장검침</th>
                    <th>공사자</th>
                    <th>현장소장</th>
                    <th>수정</th>
                </tr>
            </thead>
            <tbody>
                <tr class="text-center">
                    <td>현장도착</td>
                    <td><?php echo $spec_info['field_arrival']  == null ? '-' : $spec_info['field_arrival'], '&deg C'; ?>
                    </td>
                    <td><?php echo $field_inspec_info['field_arrival_inspection']  == null ? '-' : $field_inspec_info['field_arrival_inspection']; ?>
                    </td>
                    <td class="button-td"><button value="field_arrival_worker_check"
                            class="btn btn-sm check-btn "><?php echo $field_inspec_info['field_arrival_worker_check']; ?></button>
                    </td>
                    <td class="button-td"><button value="felid_arrival_manager_check"
                            class="btn btn-sm check-btn "><?php echo $field_inspec_info['felid_arrival_manager_check']; ?></button>
                    </td>
                    <td style="text-align:center;">
                        <button class="btn btn-success btn-sm modify-btn" data-toggle="modal"
                            data-target='#editFieldModal' value="field_arrival_inspection">수정</button>
                    </td>
                </tr>
                <tr class="text-center">
                    <td>초기</td>
                    <td><?php echo $spec_info['early']  == null ? '-' : $spec_info['early'], '&deg C'; ?></td>
                    <td><?php echo $field_inspec_info['early_inspection']  == null ? '-' : $field_inspec_info['early_inspection']; ?>
                    </td>
                    <td class="button-td"><button value="early_worker_check"
                            class="btn btn-sm check-btn "><?php echo $field_inspec_info['early_worker_check']; ?></button>
                    </td>
                    <td class="button-td"><button value="early_manager_check"
                            class="btn btn-sm check-btn "><?php echo $field_inspec_info['early_manager_check']; ?></button>
                    </td>

                    <td style="text-align:center;">
                        <button class="btn btn-success btn-sm modify-btn" data-toggle="modal"
                            data-target='#editFieldModal' value="early_inspection">수정</button>
                    </td>
                </tr>
                <tr class="text-center">
                    <td>중간</td>
                    <td><?php echo $spec_info['middle']  == null ? '-' : $spec_info['middle'], '&deg C'; ?></td>
                    <td><?php echo $field_inspec_info['middle_inspection']  == null ? '-' : $field_inspec_info['middle_inspection']; ?>
                    </td>
                    <td class="button-td">
                        <button value="middle_worker_check"
                            class="btn btn-sm check-btn "><?php echo $field_inspec_info['middle_worker_check']; ?></button>
                    </td>
                    <td class="button-td">
                        <button value="middle_manager_check"
                            class="btn btn-sm check-btn "><?php echo $field_inspec_info['middle_manager_check']; ?></button>
                    </td>

                    <td style="text-align:center;">
                        <button class="btn btn-success btn-sm modify-btn" data-toggle="modal"
                            data-target='#editFieldModal' value="middle_inspection">수정</button>
                    </td>
                </tr>
                <tr class="text-center">
                    <td>완성</td>
                    <td><?php echo $spec_info['completion']  == null ? '-' : $spec_info['completion'], '&deg C 이상'; ?>
                    </td>
                    <td>
                        <?php echo $field_inspec_info['completion_inspection'] == null ? '-' : $field_inspec_info['completion_inspection']; ?>
                    </td>
                    <td class="button-td">
                        <button value="completion_worker_check"
                            class="btn btn-sm check-btn "><?php echo $field_inspec_info['completion_worker_check']; ?></button>
                    </td>
                    <td class="button-td">
                        <button value="completion_manager_check"
                            class="btn btn-sm check-btn "><?php echo $field_inspec_info['completion_manager_check']; ?></button>
                    </td>

                    <td style="text-align:center;">
                        <button class="btn btn-success btn-sm modify-btn" data-toggle="modal"
                            data-target='#editFieldModal' value="completion_inspection">수정
                        </button>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>

<!-- 수정모달 -->
<div class="modal fade" id="editFieldModal" tabindex="-1" role="dialog" aria-labelledby="editFieldModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="editFieldModalLabel">수정</h4>
                <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×
                    </span><span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="editInspection">현장 검침 수정</label>
                        <input type="text" class="form-control" id="editInspection">
                        <input type="hidden" class="form-control" id="editInspectionField">
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="editInspection();">Save changes</button>
            </div>
        </div>
    </div>
</div>


<!-- 현장확인추가모달 -->
<div class="modal fade" id="addFieldModal" tabindex="-1" role="dialog" aria-labelledby="addFieldModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addFieldModalTitle">현장확인 추가</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="add-filed-level">단계</label>
                        <select class="form-control" id="add-filed-level">
                            <option>선택</option>
                            <option>현장도착</option>
                            <option>초기</option>
                            <option>중간</option>
                            <option>완성</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="add-field-instruction">지침</label>
                        <select class="form-control" id="add-field-instruction">
                            <option>선택</option>
                            <option>120~160°C</option>
                            <option>110~140°C</option>
                            <option>70~90°C</option>
                            <option>60°C이상</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

<script>

    var id_no = '';
    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }
    $.urlParam('id');
    // console.log($.urlParam('id'));

    id_no = $.urlParam('id');
    function editInspection() {

        var result = confirm("현장검침 값을 수정하시겠습니까?");
        if (result) {
            alert('수정되었습니다.');
        } else {
            return;
        }
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        $.urlParam('id');
        // console.log($.urlParam('id'));

        id_no = $.urlParam('id');
        // console.log(id);
        var sendData = {
            id: id_no,
            editField: $('#editInspectionField').val(),
            editValue: $('#editInspection').val()
        };
        // console.log(sendData);

        rpc("con-list-all", "editInspection", sendData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            location.reload();
        });
    }


</script>