<style>
    .document-preview-frame {
        width: 100%;
        height: 800px;
    }

    .button-td {
        width: 70px;
    }

    input {
        border: unset;
        text-align: center;
        background-color: inherit;
    }
</style>
<script>
    var doc_law_years;
    $(document).ready(function () {
        getLawAllList();

        doc_law_years = $("input[name=doc-law-year]");
        $("#select-button-by-year").change(function () {
            year = $(this).val();
            if (year == 'lawAll') {
                getLawAllList();
            } else {
                // console.log(year);
                rpc("documents", "lawsByYear", { 'year': year }, function (data) {
                    if (data.result != "ok") {
                        alert("통신에 문제가 있습니다.");
                        return;
                    }
                    var contents = data.contents;
                    var item;
                    var doc = "";
                    for (inx = 0; inx < contents.length; ++inx) {
                        item = contents[inx];
                        doc += "<tr style='text-align:center'>";
                        doc += "<td>" + (inx + 1) + "</td>";
                        doc += "<td>" + item.year + "</td>";
                        doc += "<td  style = 'text-align: left;'>" + item.title + "</td>";
                        doc += "<td class='button-td'><button class='btn btn-primary btn-sm' onclick='documentPreviewBtn(this);'data-file='/documents/" + item.url + ".pdf'>미리보기</button></td>";
                doc += "<td class='button-td'><a class='btn btn-success btn-sm' onclick='documentDownloadBtn(this);'data-file='/documents/" + item.url + ".pdf'>다운로드</a></td>";
                        doc += "</tr>";
                    }
                    $("#documents-law-table-tbody").html(doc);
                });
            }
        });

    });

    function documentPreviewBtn(obj) {
        var file = $(obj)[0].dataset.file;
        console.log(file);
        $("#document-preview-div-law").html('<iframe class="document-preview-frame" src="pdf_viewer/web/viewer.html?file=' + file + '" ></iframe>');
    }

    function documentDownloadBtn(obj) {
        var file = $(obj)[0].dataset.file;
        $(obj).attr({ target: '_blank', href: file });
        $('#download-frame').attr("src", file);
    }


    function getLawAllList() {
        rpc("documents", "laws", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var doc = "";
            var options = "";
            options += "<option value='lawAll'>전체</option>";
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                doc += "<tr style='text-align:center'>";
                doc += "<td>" + (inx + 1) + "</td>";
                doc += "<td>" + item.year + "</td>";
                doc += "<td style = 'text-align: left;'>" + item.title + "</td>";
                doc += "<td class='button-td'><button class='btn btn-primary btn-sm' onclick='documentPreviewBtn(this);'data-file='/documents/" + item.url + ".pdf'>미리보기</button></td>";
                doc += "<td class='button-td'><a class='btn btn-success btn-sm' onclick='documentDownloadBtn(this);'data-file='/documents/" + item.url + ".pdf'>다운로드</a></td>";
                doc += "</tr>";

                options += "<option value=" + item.year + ">" + item.year + '년' + "</option>";
            }
            $("#documents-law-table-tbody").html(doc);
            $("#select-button-by-year").html(options);
        });
    }
</script>

<div class="card card-default" data-scroll-height="675">
    <div class="card-header">
        <h2>관련법률 리스트</h2>
        <div class="dropdown d-inline-block mb-1" style="margin-left:10px">
            <label for="select-button-by-year">년도 선택</label>
            <select class="form-control" id="select-button-by-year">

            </select>
        </div>
    </div>
    <div class="card-body slim-scroll">
        <!-- <form id="documents-law-form"> -->
            <table class="table table-bordered table-responsive" id="documents-law-table">
                <thead>
                    <tr class="bg-light text-center">
                        <th>번호</th>
                        <!-- <th>분류</th> -->
                        <th>년도</th>
                        <th>제목</th>
                        <th class="button-td">미리보기</th>
                        <th class="button-td">다운로드</th>
                    </tr>
                </thead>
                <tbody id="documents-law-table-tbody">
                    <!-- <tr class="text-center">
                        <td><input type="text" name="doc-law-num" value="6" /></td>
                        <td><input type="text" name="doc-law-cate" value="법률" /></td>
                        <td><input type="text" name="doc-law-year" value="2001년" /></td>
                        <td><input type="text" name="doc-law-name" value="건설기술관리법" /></td>
                        <td class="button-td"><a class="btn btn-primary btn-sm document-preview-btn"
                                data-file="/documents/건설기술관리법(1).pdf"><input type="text" name="doc-law-preview"
                                    value="미리보기" /></a></td>
                        <td class="button-td"><a class="btn btn-success btn-sm document-download-btn"
                                data-file="/documents/건설기술관리법(1).pdf"><input type="text" name="doc-law-download"
                                    value="다운로드" /></a></td>
                    </tr>  -->
                </tbody>
            </table>
        <!-- </form> -->
    </div>
</div>
<div id="document-preview-div-law">
</div>