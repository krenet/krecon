<style>
        .chat-label{
                font-size: 13pt;
        }
        .font-bold{
                font-weight: bold;
        }
        .chat-date{
                float: right;
        }
</style>
<label class="chat-label font-bold">
        주제 : 보수공자 자문 회의 성암로
</label>


<label>
        [ 참여인원 : 이상염, 권수연, 최준성 (3명) ]
</label>

<label class="chat-date" id="chat-date">
        일자 : 2021-05-00
</label>

<!-- <video controls width="100%" height="auto" id="videoTag" muted="muted" autoplay="true"> -->
<video width="100%" height="auto" id="videoTag" muted="muted" autoplay="true">
<source src="../documents/화상채팅.mp4"
        type="video/mp4">

Sorry, your browser doesn't support embedded videos.
</video>

<!-- <iframe src="http://192.168.0.59:7000/" width="100%" height="800"> -->

<!-- </iframe> -->

<script>

        Date.prototype.format = function(f) {
                if (!this.valueOf()) return " ";
        
                var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
                var d = this;
                
                return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
                switch ($1) {
                        case "yyyy": return d.getFullYear();
                        case "yy": return (d.getFullYear() % 1000).zf(2);
                        case "MM": return (d.getMonth() + 1).zf(2);
                        case "dd": return d.getDate().zf(2);
                        case "E": return weekName[d.getDay()];
                        case "HH": return d.getHours().zf(2);
                        case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
                        case "mm": return d.getMinutes().zf(2);
                        case "ss": return d.getSeconds().zf(2);
                        case "a/p": return d.getHours() < 12 ? "오전" : "오후";
                        default: return $1;
                }
                });
        };
        
        String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
        String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
        Number.prototype.zf = function(len){return this.toString().zf(len);};

        $(document).ready(function(){
                var today = new Date().format("yyyy-MM-dd");
                $("#chat-date").html("일자 : "+today);
        });
</script>