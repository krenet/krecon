<style>
    .table-dark-head th {
        color: white;
        text-align: center;
    }

    #cons-history-table-body td {
        text-align: center;
    }

    #cons-history-table-body tr:hover,
    .cons-history-table-body-hovered {
        background: #7DBCFF;
        color: white;
        cursor: pointer;
    }

    .noselect {
        -webkit-touch-callout: none;
        /* iOS Safari */
        -webkit-user-select: none;
        /* Safari */
        -khtml-user-select: none;
        /* Konqueror HTML */
        -moz-user-select: none;
        /* Firefox */
        -ms-user-select: none;
        /* Internet Explorer/Edge */
        user-select: none;
        /* Non-prefixed version, currently
                                                            supported by Chrome and Opera */
    }
</style>
<!-- <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=8635eb5a7465928c8d747aa7e5f3d6b6"></script> -->
<!-- <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=fb33fb2a39c5c870948bbe64f31deb8e"></script> -->

<script>
    // //<![CDATA[     
    // //공사중 마커 이미지
    // var defaultImageSrc = "http://t1.daumcdn.net/localimg/localimages/07/mapjsapi/default_marker.png";
    // var liveImageSrc = "http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/markerStar.png";
    // var selectedImageSrc = "img/pin-selected.gif";
    // // 마커 이미지의 이미지 크기 입니다
    // var defaultImageSize = new daum.maps.Size(35, 36);
    // var liveImageSize = new daum.maps.Size(24, 35);
    // var selectedImageSize = new daum.maps.Size(35, 35);
    // // 마커 이미지를 생성합니다    
    // var defaultMarkerImage = new daum.maps.MarkerImage(defaultImageSrc, defaultImageSize);
    // var liveMarkerImage = new daum.maps.MarkerImage(liveImageSrc, liveImageSize);
    // var selectedMarkerImage = new daum.maps.MarkerImage(selectedImageSrc, selectedImageSize);

    // //지도 객체를 담을 변수
    // var map;

    // //표시 데이터
    // var cons_id_list = new Array();
    // var cons_list = new Array();
    // cons_list.push({ id: 'i0001', sdate: '2019-04-13', edate: '-', region: '서울시 마포구', name: '성암로(중소기업 DMC 앞) 포장 보수 공사 ', latlng: new daum.maps.LatLng(37.577387, 126.897684), manager: '김그래', mobile: '01086439391', phone: '023033713', issue: "설계도 불일치", islive: true });
    // cons_list.push({ id: 'i0002', sdate: '2018-09-13', edate: '2018-10-13', region: '서울시 마포구', name: '성산대교 포장 보수 공사', latlng: new daum.maps.LatLng(37.552715, 126.891457), manager: '김그래', mobile: '01086439391', phone: '023033713', issue: "기층 약화", islive: false });
    // cons_list.push({ id: 'i0003', sdate: '2017-04-23', edate: '2017-06-25', region: '서울시 영등포구', name: '양화대교 포장 보수 공사', latlng: new daum.maps.LatLng(37.542678, 126.903465), manager: '오세일', mobile: '01086439391', phone: '023033713', issue: "포트홀 상습 발생", islive: false });
    // cons_list.push({ id: 'i0004', sdate: '2016-07-07', edate: '2016-09-15', region: '서울시 마포구', name: '서강대교 포장 보수 공사', latlng: new daum.maps.LatLng(37.537102, 126.925111), manager: '오세일', mobile: '01086439391', phone: '023033713', issue: "포트홀 상습 발생", islive: false });
    // cons_list.push({ id: 'i0005', sdate: '2016-06-02', edate: '2016-09-12', region: '서울시 마포구', name: '가양대교 포장 보수 공사', latlng: new daum.maps.LatLng(37.570082, 126.861502), manager: '오세일', mobile: '01086439391', phone: '023033713', issue: "설계도 불일치", islive: false });
    // cons_list.push({ id: 'i0006', sdate: '2015-05-03', edate: '2015-08-25', region: '경기도 고양시', name: '고양대교 포장 보수 공사', latlng: new daum.maps.LatLng(37.6526522, 126.7179719), manager: '김그래', mobile: '01086439391', phone: '023033713', issue: "보강층 약화", islive: false });
    // cons_list.push({ id: 'i0007', sdate: '2015-05-03', edate: '2015-08-25', region: '충청남도 공주시', name: '천안논산 도로 포장 보수 공사', latlng: new daum.maps.LatLng(36.531912, 127.110622), manager: '김그래', mobile: '01086439391', phone: '023033713', issue: "지반 약화", islive: true });
    // cons_list.push({ id: 'i0008', sdate: '2015-05-03', edate: '2015-08-25', region: '충청남도 공주시', name: '천안논산 도로 포장 보수 공사', latlng: new daum.maps.LatLng(36.637414, 127.116244), manager: '김그래', mobile: '01086439391', phone: '023033713', issue: "지반 약화", islive: true });
    // cons_list.push({ id: 'i0009', sdate: '2015-05-03', edate: '2015-08-25', region: '충청북도 청주시', name: '송천교 포장 보수 공사', latlng: new daum.maps.LatLng(36.667028, 127.464726), manager: '김그래', mobile: '01086439391', phone: '023033713', issue: "주민민원 상습지역", islive: false });


    //초기화


    //     var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    //         mapOption = {
    //             center: new daum.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
    //             level: 3 // 지도의 확대 레벨
    //         };



    //     // 지도를 표시할 div와  지도 옵션으로  지도를 생성합니다
    //     map = new daum.maps.Map(mapContainer, mapOption);

    //     //지도 마커 표시 및 지도 범위 설정
    //     var bounds = new daum.maps.LatLngBounds();
    //     var liveCount = 0;
    //     var consHistoryTableHTML = "";
    //     var item;
    //     var marker;

    //     for (inx = 0; inx < cons_list.length; ++inx) {
    //         item = cons_list[inx];
    //         marker = new daum.maps.Marker({
    //             position: item.latlng,
    //             title: item.name,
    //             map: map
    //         });
    //         cons_list[inx].marker = marker;
    //         cons_id_list[item.id] = item;
    //         marker.id = item.id;
    //         addMarkerActions(marker, item.id);

    //         bounds.extend(item.latlng);
    //         if (item.islive) {
    //             marker.setImage(liveMarkerImage);
    //             ++liveCount;
    //         } else {
    //             marker.setImage(defaultMarkerImage);
    //         }

    //         consHistoryTableHTML += '<tr class="cons-history-table-row" data-cons-id="' + item.id + '">' +
    //             '<td scope=\"row\">' + item.region + '</td>' +
    //             '<td>' + item.name + '</td>' +
    //             '<td>' + item.issue + '</td>' +
    //             '</tr>';

    //     }
    //     map.setBounds(bounds);

    //     //시공 이력및 현재 시공 중 현장 텍스트입력
    //     $(".total-cons-count").text(cons_list.length);
    //     $(".live-cons-count").text(liveCount);

    //     //테이블 완성
    //     $("#cons-history-table-body").html(consHistoryTableHTML);

    //     //테이블에 마우스 오버시 해당 마커 강조
    //     $(".cons-history-table-row").mouseover(function () {
    //         var s_item = cons_id_list[$(this).data('cons-id')];
    //         s_item.marker.setMap(null);
    //         s_item.marker.setImage(selectedMarkerImage);
    //         s_item.marker.setMap(map);
    //     });
    //     $(".cons-history-table-row").mouseout(function () {
    //         var s_item = cons_id_list[$(this).data('cons-id')];
    //         s_item.marker.setMap(null);
    //         if (s_item.islive) {
    //             s_item.marker.setImage(liveMarkerImage);
    //         } else {
    //             s_item.marker.setImage(defaultMarkerImage);
    //         }

    //         s_item.marker.setMap(map);
    //     });

    //     $(".cons-history-table-row").click(function () {
    //         var s_item = cons_id_list[$(this).data('cons-id')];
    //         moveDetail(s_item.id);
    //     });

    // })

    // //마커에 마우스 이벤트 등록                    
    // function addMarkerActions(marker, id) {

    //     daum.maps.event.addListener(marker, 'mouseover', function () {
    //         $(".cons-history-table-row[data-cons-id=" + id + "]").addClass("cons-history-table-body-hovered");
    //     });

    //     daum.maps.event.addListener(marker, 'mouseout', function () {
    //         $(".cons-history-table-row[data-cons-id=" + id + "]").removeClass("cons-history-table-body-hovered");
    //     });

    //     daum.maps.event.addListener(marker, 'click', function () {
    //         moveDetail(id);
    //     });
    // }

    // //상세 페이지로 이동
    // function moveDetail(id) {
    //     /*
    //                         location.href=".?cat=site&page=site_detail&id="+id;
    //                         */
    // }
    // //        ]]>
</script>


<div class="row">
    <div class="col-xl-6 col-md-12">
        <div class="card card-default" data-scroll-height="675">
            <div class="card-header">
                <h2>이슈 발생 지도</h2>
            </div>
            <div class="card-body">
                <div id="mapComIssue" style="width:100%;height:400px;"></div>
            </div>
            <div class="card-footer d-flex flex-wrap bg-white p-0">
                <div class="col-6 px-0">
                    <div class="text-center p-4">
                        <h4 class="total-cons-count"></h4>
                        <p class="mt-2">총 발생 이력</p>
                    </div>
                </div>
                <div class="col-6 px-0">
                    <div class="text-center p-4 border-left">
                        <h4 class="live-cons-count"></h4>
                        <p class="mt-2">현재 시공 중</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-6 col-md-12">
        <div class="card card-default" data-scroll-height="675">
            <div class="card-header">
                <h2>이슈 발생 리스트</h2>
            </div>
            <div class="card-body" style="height:500px;overflow:auto;">
                <table class="table table-bordered noselect table-responsive" id="issue-table" style="text-align:center;">
                    <thead class="table-dark table-dark-head" id="community-site-issue">
                        <tr>
                            <th scope="col" class="">지역명</th>
                            <th scope="col" class="">현장명</th>
                            <th scope="col" class="">이슈</th>
                            <th scope="col" class="">수정</th>
                        </tr>
                    </thead>
                    <tbody id="cons-history-table-body" class=" slim-scroll">
                    </tbody>
                </table>
            </div>
            <div class="card-footer d-flex flex-wrap bg-white p-0">
                <div class="col-6 px-0">
                    <div class="text-center p-4">
                        <h4 class="total-cons-count"></h4>
                        <p class="mt-2">총 발생 이력</p>
                    </div>
                </div>
                <div class="col-6 px-0">
                    <div class="text-center p-4 border-left">
                        <h4 class="live-cons-count"></h4>
                        <p class="mt-2">현재 시공 중</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6"></div>
    <div class="col-md-6 card-text" style="text-align:right; padding: 20px;">
        <button class="btn btn-primary add-btn add-issue" onclick="loadAddIssue()" data-toggle="modal"
            data-target="#makeIssueModal">추가</button>
    </div>
</div>
<!-- Form Modal -->
<div class="modal fade" id="makeIssueModal" tabindex="-1" role="dialog" aria-labelledby="makeIssueModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="makeIssueModalTitle">현장 이슈 추가</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group addOffInputName">
                        <label for="addOffInputName">현장명</label>
                        <select class="form-control" id="addOffInputName">
                            <option selected>선택</option>
                            <option>성암로(중소기업 DMC 앞) 포장 보수 공사</option>
                            <option>성산대교 포장 보수 공사</option>
                            <option>양화대교 포장 보수 공사</option>
                            <option>서강대교 포장 보수 공사</option>
                            <option>가양대교 포장 보수 공사</option>
                            <option>고양대교 포장 보수 공사</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="addIssueName">이슈</label>
                        <input type="text" class="form-control" id="addIssueName" placeholder="이슈를 입력해주세요.">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addIssueOnModal()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- 수정모달 -->
<div class="modal fade" id="editIssueModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="editModalLabel">수정</h4>
                <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×
                    </span><span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modalContent">
                    <form role="form" name="editModalForm" id="editModalForm" action="adviser-edit.php" method="post">
                        <div class="form-group">
                            <label for="지역명">지역명</label>
                            <input class="form-control" name="지역명" id="area" value="" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="현장명">현장명</label>
                            <input class="form-control" name="현장명" id="field" value="" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="이슈">이슈</label>
                            <input class="form-control" name="이슈" id="issue" value="">
                        </div>
                        <input type="hidden" name="dbID" id="dbID" value="">
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick='checkEditValue(this);'>Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="showDetailIssueModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titleArea"> </h4>
                <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×
                    </span><span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modalContent">
                        <div class="form-group">
                            <div id="innerContents" value=""></div>
                        </div>
                        <input type="hidden" name="dbID" id="dbID" value="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary" onclick='checkEditValue(this);'>Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<script>


    // //초록수정 수정모달로 들어가기
    function clickEdit(obj) {
        var tr = $(obj).parent().parent();
        var dbID = $(obj).val();
        var td = tr.children();
        var str = ""
        var tdArr = new Array();
        td.each(function (i) {
            tdArr.push(td.eq(i).text());
        });

        $("#editIssueModal #area").val(tdArr[0]);
        $("#editIssueModal #field").val(tdArr[1]);
        $("#editIssueModal #issue").val(tdArr[2]);
        $('#editIssueModal #dbID').val(dbID);

    }

    //파란수정 수정이 실제로 되는 버튼
    function checkEditValue(obj) {
        var result = confirm("현장이슈를 수정하시겠습니까?");
        if (result) {
            alert('수정되었습니다.');
        } else {
            return;
        }

        var formValue = $(obj).parent().parent();
        // console.log(formValue);
        var formData = $("#editModalForm").serializeObject();
        // console.log(formData);

        rpc("issue", "edit", formData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            //수정하고나서의 프로세스 
            location.reload();
        });
    }

    jQuery.fn.serializeObject = function () {
        var obj = null;
        try {
            if (this[0].tagName && this[0].tagName.toUpperCase() == "FORM") {
                var arr = this.serializeArray();
                // console.log(arr);
                if (arr) {
                    obj = {};
                    jQuery.each(arr, function () {
                        obj[this.name] = this.value;
                    });
                }
            }
        } catch (e) {
            alert(e.message);
        } finally { }
        return obj;
    }

    function loadAddIssue() {
        rpc("issue", "loadAddIssue", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var issue = "";
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                issue += "<option>" + item.con_area_name + "</option>";
            }
            $("#addOffInputName").html(issue);
        });
    }

    function addIssueOnModal() {
        var sendData = {
            con_area_name: $('#addOffInputName').val(),
            title: $('#addIssueName').val(),
        };

        rpc("issue", "addIssueOnModal", sendData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
        });
        location.reload();
    }
    var conGps = new Array();
    $(document).ready(function () {
        // console.log('dddddddddd', $.urlParam);
        if ($.urlParam == undefined) {
            rpc("issue", "list", null, function (data) {
                if (data.result != "ok") {
                    alert("통신에 문제가 있습니다.");
                    return;
                }
                var contents = data.contents;
                var item;
                var offs = "";
                conGps=[];
                var con_lat;
                var con_long;
                for (inx = 0; inx < contents.length; ++inx) {
                    item = contents[inx];
                    offs += "<tr id='" + item.id + "'style='text-align:center;' onclick='clickDetailIssueRow(this);' data-toggle='modal' data-target='#showDetailIssueModal'><td>" + item.con_field_name + "</td>";
                    offs += "<td>" + item.con_area_name + "</td>";
                    offs += "<td>" + item.title + "</td>";
                    offs += "<td> <button type='button' value='" + item.id + "' class='btn btn-success modify-btn btn-sm' data-toggle='modal' onclick='clickEdit(this);' data-target='#editIssueModal'>수정</button></td>";
                    offs += "</tr>";

                    conGps.push({title:item.title, latlng:new kakao.maps.LatLng(item.con_lat,item.con_long)});
                    // conGps += push(new daum.maps.LatLng(item.con_lat,item.con_long));
                    
                }
                console.log(conGps);


                $("#cons-history-table-body").html(offs);
                $(".total-cons-count").text(contents.length);

                if (contents.length == 0) {
                    var none = '';
                    none += '<tr><td colspan=4 style=text-align:center>등록된 현장이슈가 없습니다. </td></tr>';
                    $("#cons-history-table-body").html(none);
                }

                var mapContainer = document.getElementById('mapComIssue'), // 지도를 표시할 div 
                mapOption = {
                    center: new daum.maps.LatLng(37.552715, 126.891457), // 지도의 중심좌표
                    level: 7 // 지도의 확대 레벨
                };
                var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

                var positions = conGps;
                console.log(positions);

                var imageSrc = "http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/markerStar.png"; 
    
                for (var i = 0; i < positions.length; i ++) {
                    
                    // 마커 이미지의 이미지 크기 입니다
                    var imageSize = new kakao.maps.Size(24, 35); 
                    
                    // 마커 이미지를 생성합니다    
                    var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize); 
                    
                    // 마커를 생성합니다
                    var marker = new kakao.maps.Marker({
                        map: map, // 마커를 표시할 지도
                        position: positions[i].latlng, // 마커를 표시할 위치
                        title : positions[i].title, // 마커의 타이틀, 마커에 마우스를 올리면 타이틀이 표시됩니다
                        image : markerImage // 마커 이미지 
                    });
                }
            });

            rpc("issue", "listIsLive", null, function (data) {
                if (data.result != "ok") {
                    alert("통신에 문제가 있습니다.");
                    return;
                }
                var contents = data.contents;
                var item;
                var offs = "";
                for (inx = 0; inx < contents.length; ++inx) {
                    item = contents[inx];

                }

            });

        }
    });
    function clickDetailIssueRow(obj){
            console.log($(obj)[0].attributes.value.value);
            var id = $(obj)[0].attributes.value.value;
            rpc("issue", "showDetailIssueModal", {'id': id}, function (data) {
                if (data.result != "ok") {
                    alert("통신에 문제가 있습니다.");
                    return;
                }
                var contents = data.contents;
                console.log(contents[0].title);
                console.log(contents[0].contents);

                var titleArea = contents[0].title;
                var innerContents = contents[0].contents;
            $('#showDetailIssueModal #titleArea').text(titleArea);
            $('#showDetailIssueModal #innerContents').text(innerContents);
            });
            // $('#showDetailIssueModal #titleArea').html(titleArea);
            // $('#showDetailIssueModal #titleArea').val(titleArea);

        }
</script>