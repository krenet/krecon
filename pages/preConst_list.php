<style>
    .document-preview-frame {
        width: 100%;
        height: 800px;
    }

    .button-td {
        width: 70px;
    }

    .add-cons-btn{
        position: absolute;
        right: 50px;
        font-size: 25px;
        background: #7dbcff;
        color: white;
        border-radius: 50%;
        height: 35px;
        width: 35px;
    }
</style>

<div class="card card-default" data-scroll-height="675">
    <div class="card-header">
        <h2>사전조사 리스트</h2>
        <button class="add-cons-btn">+</button>
    </div>
    <div class="card-body">
		<table class="table table-bordered table-responsive">
			<thead>
				<tr class="bg-light text-center">
					<th name='id'>번호</th>
					<th name='title'>현장명</th>
					<th name='address'>지역명</th>
					<th name='type'>공사개요</th>
					<th name='bid'>금액</th>
					<th>내용</th>
					<th>삭제</th>
				</tr>
			</thead>
			<tbody id="adv-contents">
                <tr class="text-center">
                    <td>1</td>
                    <td>정문 요금소 도로 아스콘 보수공사</td>
                    <td>서울특별시 중랑구 신내로 156</td>
                    <td>도로보수</td>
                    <td>37,730,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='1' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
                <tr class="text-center">
                    <td>2</td>
                    <td>신기2교 보수공사</td>
                    <td>삼척시 신기면 신기리 451-21번지 일원</td>
                    <td>교량보수</td>
                    <td>51,804,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='2' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
                <tr class="text-center">
                    <td>3</td>
                    <td>덕진구 관내 도로 긴급보수공사</td>
                    <td>전주시 덕진구</td>
                    <td>도로유지보수</td>
                    <td>99,500,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='3' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
                <tr class="text-center">
                    <td>4</td>
                    <td>거창읍 도로 소파보수공사</td>
                    <td>거창읍 시가지 일원</td>
                    <td>도로 소파보수</td>
                    <td>35,761,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='4' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
                <tr class="text-center">
                    <td>5</td>
                    <td>지방도604호 대산도로 포장도 보수공사</td>
                    <td>공주시 정안면 대산리 780번지 일원</td>
                    <td>절삭후 아스콘 덧씌우기</td>
                    <td>101,400,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' value='5' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr>
                <!-- <tr class="text-center">
                    <td>6</td>
                    <td>ㅇㅇ교 보수</td>
                    <td>ㅇㅇ시 ㅇㅇ교</td>
                    <td>교량보수</td>
                    <td>10,000,000원</td>
                    <td><button type='button' class='btn btn-success btn-sm' onclick='goDetail(this);'>자세히</button></td>
                    <td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</td>
                </tr> -->
			</tbody>
		</table>
	</div>
</div>

    <script>
        
        $(document).ready(function () {
        $(".add-cons-btn").click((evt)=>{
                    console.log('evt : ', evt);
                    location.href=".?cat=preConst&page=preConst_addList";
                });
        });

        function clickDelete(obj) {
            var result = confirm("해당 리스트 정보를 삭제하시겠습니까?");
            if(result) {
                alert('삭제되었습니다.');
            }else{
                return;
            }
        }

        function goDetail(obj){
            // console.log("this: ",obj.value);
            var t_value = obj.value;
            location.href="http://localhost/?cat=preConst&page=preConst_detail&t_value="+t_value;
        }
    </script>