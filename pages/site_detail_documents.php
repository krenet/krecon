<style>
    .document-preview-frame {
        width: 100%;
        height: 500px;
    }

    .button-td {
        width: 70px;
    }
</style>
<script>
   $(document).ready(function () {
        getList();

        $("#sigong").click(function () {
            getSigongList();
        });
        $("#jungong").click(function () {
            getJungongList();
        });
        $("#getAll").click(function () {
            getList();
        });

    });

    function clickPreview(obj) {
        var file = $(obj)[0].dataset.file;
        $("#document-preview-div").html('<iframe class="document-preview-frame" src="pdf_viewer/web/viewer.html?file=' + file + '" ></iframe>');
    }

    function clickDownload(obj) {
        var file = $(obj)[0].dataset.file;
        $(obj).attr({ target: '_blank', href: file });
        $('#download-frame').attr("src", file);
    }

    function getList() {
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        $.urlParam('id');

        id_no = $.urlParam('id');
        $('#docConCode').val(id_no);
        
        rpc("con-list-all", "siteDetailDocument", { 'id': id_no }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }

            var contents = data.contents;
            var item;
            var doc = '';
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                doc += "<tr>";
                doc += "<td>" + (inx + 1) + "</td>";
                doc += "<td>" + item.doc_category + "</td>";
                doc += "<td>" + item.doc_title + "</td>";
                doc += "<td><button class='btn btn-primary btn-sm document-preview-btn' onclick='clickPreview(this);'";
                doc += "data-file='/pages/uploads/" + item.doc_saved_file_name + "'>미리보기</button></td>";
                doc += "<td><a class='btn btn-success btn-sm document-download-btn' onclick='clickDownload(this);'";
                doc += "data-file='/pages/uploads/" + item.doc_saved_file_name + "'>다운로드</a></td>";
                doc += "</tr>";
            }
            $("#krecon_document").html(doc);
            if (contents.length == 0) {
                var none = '';
                none += '<tr><td colspan=5 style=text-align:center>등록된 문서가 없습니다. </td></tr>';
                $("#krecon_document").html(none);
            }
        });
    }

    function getSigongList() {
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        $.urlParam('id');

        id_no = $.urlParam('id');

        
        rpc("con-list-all", "siteDetailDocumentSigong", { 'id': id_no }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }

            var contents = data.contents;
            var item;
            var doc = '';
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                doc += "<tr>";
                doc += "<td>" + (inx + 1) + "</td>";
                doc += "<td>" + item.doc_category + "</td>";
                doc += "<td>" + item.doc_title + "</td>";
                doc += "<td><button class='btn btn-primary btn-sm document-preview-btn' onclick='clickPreview(this);'";
                doc += "data-file='/pages/uploads/" + item.doc_saved_file_name + ".pdf'>미리보기</button></td>";
                doc += "<td><a class='btn btn-success btn-sm document-download-btn' onclick='clickDownload(this);'";
                doc += "data-file='/pages/uploads/" + item.doc_saved_file_name + ".pdf'>다운로드</a></td>";
                doc += "</tr>";
                //  : "+item.con_code+"<br />\n";
            }
            $("#krecon_document").html(doc);
            // console.log(contents.length);
            if (contents.length == 0) {
                var none = '';
                none += '<tr><td colspan=5 style=text-align:center>등록된 시공 관련 문서가 없습니다. </td></tr>';
                $("#krecon_document").html(none);
            }
        });
    }

    function getJungongList() {
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        $.urlParam('id');

        id_no = $.urlParam('id');
        rpc("con-list-all", "siteDetailDocumentJungong", { 'id': id_no }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }

            var contents = data.contents;
            var item;
            var doc = '';
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                doc += "<tr>";
                doc += "<td>" + (inx + 1) + "</td>";
                doc += "<td>" + item.doc_category + "</td>";
                doc += "<td>" + item.doc_title + "</td>";
                doc += "<td><button class='btn btn-primary btn-sm document-preview-btn' onclick='clickPreview(this);'";
                doc += "data-file='/pages/uploads/" + item.doc_saved_file_name + ".pdf'>미리보기</button></td>";
                doc += "<td><a class='btn btn-success btn-sm document-download-btn' onclick='clickDownload(this);'";
                doc += "data-file='/pages/uploads/" + item.doc_saved_file_name + ".pdf'>다운로드</a></td>";
                doc += "</tr>";
            }
            $("#krecon_document").html(doc);
            if (contents.length == 0) {
                var none = '';
                none += '<tr><td colspan=5 style=text-align:center>등록된 준공 관련 문서가 없습니다. </td></tr>';
                $("#krecon_document").html(none);
            }
        });
    }
    function formSubmit(f) {

        var extArray = new Array('hwp', 'xls', 'doc', 'xlsx', 'docx', 'txt', 'ppt', 'pptx','pdf');
        var path = document.getElementById("upfile").value;
        if (path == "") {
            alert("파일을 선택해 주세요.");
            return false;
        }

        var pos = path.indexOf(".");
        if (pos < 0) {
            alert("확장자가 없는파일 입니다.");
            return false;
        }

        var ext = path.slice(path.indexOf(".") + 1).toLowerCase();
        var checkExt = false;
        for (var i = 0; i < extArray.length; i++) {
            if (ext == extArray[i]) {
                checkExt = true;
                break;
            }
        }

        if (checkExt == false) {
            alert("업로드 할 수 없는 파일 확장자 입니다.");
            return false;
        }

        $('#upload_callback_page').val(location.href);

        return true;
    }


</script>
<div class="row">
    <div class="col-md-6 col-sm-6" style="margin-bottom:10px;display: inline-block;text-align: left;">
        <div class="btn-group btn-group-md btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-primary active" id="getAll">
                <input type="radio" name="options" id="option1" autocomplete="off">전체
            </label>
            <label class="btn btn-primary " id="sigong">
                <input type="radio" name="options" id="option2" autocomplete="off"> 시공
            </label>
            <label class="btn btn-primary" id="jungong">
                <input type="radio" name="options" id="option3" autocomplete="off"> 준공
            </label>
            
        </div>
    </div>
    <div class="col-md-6 col-sm-6" style="margin-bottom:10px;display: inline-block;text-align: right;">
        <button type="button" class="mb-1 btn btn-primary" data-toggle="modal" data-target="#addDocModal">문서추가</button>
    </div>
</div>
<div class="row">
    <table class="table table-bordered site-docu-all table-responsive">
        <thead>
            <tr class="bg-light text-center">
                <th>번호</th>
                <th>분류</th>
                <th>제목</th>
                <th>미리보기</th>
                <th>다운로드</th>
            </tr>
        </thead>
        <tbody id="krecon_document" style="text-align:center;">

        </tbody>
    </table>


</div>
<div id="document-preview-div">
</div>


<!-- Form Modal -->
<div class="modal fade" id="addDocModal" tabindex="-1" role="dialog" aria-labelledby="addDocModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addDocument">문서 추가</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form name="uploadForm" id="uploadForm" method="post" action="./pages/upload_documents.php"
                enctype="multipart/form-data" onsubmit="return formSubmit(this);">
                <input type="hidden" id="upload_callback_page" name="callBackPage" value=""/>


                <!-- <input type="submit" value="업로드" /> -->
                <div class="modal-body">
                    <input type="hidden" name="docConCode" id="docConCode" value=""/>
                    <div class="form-group">
                        <label for="addDocSelect">분류 선택</label>
                        <select class="form-control" id="addDocSelect" name="addDocSelect"placeholder="분류를 선택해 주세요">
                            <option>선택</option>
                            <option>시공</option>
                            <option>준공</option>
                            <option>기타</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="addDocTitle">문서 제목</label>
                        <input type="text" class="form-control"  name="addDocTitle" id="addDocTitle" placeholder="제목을 입력해주세요">
                    </div>
                    <div class="form-group">
                        <label for="upfile">파일 추가</label>
                        <input type="file" name="upfile" id="upfile">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">문서 추가</button>
                </div>
            </form>
        </div>
    </div>
</div>