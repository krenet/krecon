<div class="c">
    <div class="card card-default">
        <div class="card-header card-header-border-bottom">
            <h2>
                <div id="siteDetailId" style="display:inline-block"></div>시공 현장 정보
            </h2>

        </div>
        <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link" id="icon-live-tab" data-toggle="tab" href="#icon-live" role="tab"
                        aria-controls="icon-live-check" aria-selected="true">
                        <i class="mdi mdi-cast mr-1"></i>현장</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link " id="icon-live-check-tab" data-toggle="tab" href="#icon-live-check" role="tab"
                        aria-controls="icon-live-check" aria-selected="false">
                        <i class="mdi mdi-ruler mr-1"></i> 현장확인</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="icon-progress-tab" data-toggle="tab" href="#icon-progress" role="tab"
                        aria-controls="icon-progress" aria-selected="false">
                        <i class="mdi mdi-progress-check mr-1"></i> 공정률</a>
                </li>
                <!--
                        <li class="nav-item">
                            <a class="nav-link" id="icon-map-tab" data-toggle="tab" href="#icon-map" role="tab"
                                aria-controls="icon-map" aria-selected="false">
                                <i class="mdi mdi-map mr-1"></i> 지도</a>
                        </li>
                   <li class="nav-item">
                            <a class="nav-link" id="icon-guide-tab" data-toggle="tab" href="#icon-guide" role="tab"
                                aria-controls="icon-guide" aria-selected="false">
                                <i class="mdi mdi-ruler mr-1"></i> 시공지침</a>
                        </li> -->
                <li class="nav-item">
                    <a class="nav-link" id="icon-task-tab" data-toggle="tab" href="#icon-task" role="tab"
                        aria-controls="icon-task" aria-selected="false">
                        <i class="mdi mdi-ruler mr-1"></i> 과업지시서</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="icon-document-tab" data-toggle="tab" href="#icon-document" role="tab"
                        aria-controls="icon-document" aria-selected="false">
                        <i class="mdi mdi-file-document-outline mr-1"></i> 문서</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="icon-issue-tab" data-toggle="tab" href="#icon-issue" role="tab"
                        aria-controls="icon-issue" aria-selected="false">
                        <i class="mdi mdi-alarm-light mr-1"></i>현장이슈</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="icon-advice-tab" data-toggle="tab" href="#icon-advice" role="tab"
                        aria-controls="icon-advice" aria-selected="false">
                        <i class="mdi mdi-playlist-edit mr-1"></i>자문회의</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent2">
                <div class="tab-pane pt-3 fade" id="icon-live" role="tabpanel" aria-labelledby="icon-live-tab">
                    <?php include "site_detail_live.php"; ?>
                </div>

                <div class="tab-pane pt-3 fade" id="icon-live-check" role="tabpanel"
                    aria-labelledby="icon-live-check-tab">
                    <?php include "site_detail_live_check.php"; ?>
                </div>
                <!--
                        <div class="tab-pane pt-3 fade" id="icon-map" role="tabpanel" aria-labelledby="icon-map-tab">
                            지도
                        </div>
                        -->
                <div class="tab-pane pt-3 fade" id="icon-progress" role="tabpanel" aria-labelledby="icon-progress-tab">
                    <?php include "site_detail_progress.php"; ?>
                </div>
                <!-- <div class="tab-pane pt-3 fade" id="icon-guide" role="tabpanel" aria-labelledby="icon-guide-tab">
                            <?php //include "site_detail_task.php"; ?>
                        </div> -->
                <div class="tab-pane pt-3 fade" id="icon-task" role="tabpanel" aria-labelledby="icon-task-tab">
                    <?php include "site_detail_guideline.php"; ?>
                </div>
                <div class="tab-pane pt-3 fade" id="icon-document" role="tabpanel" aria-labelledby="icon-document-tab">
                    <?php include "site_detail_documents.php"; ?>
                </div>
                <div class="tab-pane pt-3 fade" id="icon-issue" role="tabpanel" aria-labelledby="icon-issue-tab">
                    <?php include "site_detail_issue.php"; ?>
                </div>
                <div class="tab-pane pt-3 fade" id="icon-advice" role="tabpanel" aria-labelledby="icon-advice-tab">
                    <?php include "site_detail_advice.php"; ?>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(".nav-link").click(function () {
            var stateObj = { tab: $(this).attr("href") };
            history.pushState(stateObj, $(this).text(), $(this).attr("href"));
        });


        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        $.urlParam('id');

        var url = window.location.href;
        console.log("url 확인: ", url);
        var hashInx = url.indexOf("#");
        var hash = url.substring(hashInx + 1);
        // console.log('hashhashhashhashhashhashhash',hash);
        // console.log(hashInx);
        if (hashInx == -1) hash = "icon-live";
        // if (hashInx == -1 || hashInx == 61) hash = "icon-live";

        $("#" + hash).addClass('show').addClass('active');
        $("#" + hash + "-tab").addClass("active");

        id_no = $.urlParam('id');

        taskOrder(id);

        //상단 장비버튼 생성
        rpc("con-list-all", "makeEquipmentBtn", { 'id': id }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }

            var contents = data.contents;
            var equipments = new Array();
            var equipmentCnt = new Object();
            for (inx = 0; inx < contents.length; ++inx) {
                if (!equipments.includes(contents[inx]['type'])) {
                    equipments.push(contents[inx]['type']);
                    equipmentCnt[contents[inx]['type']] = 1;
                } else {
                    equipmentCnt[contents[inx]['type']] = equipmentCnt[contents[inx]['type']] + 1;
                }
            }

            var item;
            var task_order = '';
            var task_order2 = '';
            var contentCnt = 0;
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];

                ++contentCnt;
                if (equipments.includes(item['type'])) {
                    task_order2 += "<div class='dropdown d-inline-block mb-1' style='margin-right:0.2rem;'>";
                    task_order2 += "<button class='btn btn-primary dropdown-toggle' type='button' id='equipment_drop" + inx + "'";
                    task_order2 += " data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' data-display='static'>" + item.type + "</button>";
                    task_order2 += "<div class='dropdown-menu' aria-labelledby='equipment_drop" + inx + "'>";
                    equipments.splice(equipments.indexOf(item['type']), 1);

                }
                item.start_time = item.start_time.substring(0, 10);
                if (item.status == 'live') {
                    task_order2 += "<a style='display: inline;'class='dropdown-item' href='#' onclick='selectEquipmentBtn(this);'";
                    task_order2 += "data='" + item.status + "'  id='equipment" + item.equipment_id + "' name =" + item.session_id + ">";
                    task_order2 += item.start_time + "<button class='badge badge-danger drop_down_status'>라이브</button></a>";
                } else if (item.status == 'converting') {
                    task_order2 += "<a style='display: inline;'class='dropdown-item' href='#' onclick='selectEquipmentBtn(this);'";
                    task_order2 += " data='" + item.status + "'  id='equipment" + item.equipment_id + "' name =" + item.session_id + ">";
                    task_order2 += item.start_time + "<button class='badge badge-success drop_down_status'>변환중</button></a>";
                } else if (item.status == 'convert_ready') {
                    task_order2 += "<a style='display: inline;'class='dropdown-item' href='#' onclick='selectEquipmentBtn(this);'";
                    task_order2 += " data='" + item.status + "'  id='equipment" + item.equipment_id + "' name =" + item.session_id + ">";
                    task_order2 += item.start_time + "<button class='badge badge-warning drop_down_status'>대기</button></a>";
                }
                else {
                    task_order2 += "<a class='dropdown-item' href='#' onclick='selectEquipmentBtn(this);' data='" + item.status + "'";
                    task_order2 += " id='equipment" + item.equipment_id + "' name =" + item.session_id + ">" + item.start_time + "</a>";
                }

                if (equipmentCnt[item['type']] == contentCnt) {
                    task_order2 += "</div></div>";
                    contentCnt = 0;
                }

            }
            $("#livebtntest").html(task_order2);

        });

        rpc("con-list-all", "siteDetail", { 'id': id_no }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }

            var contents = data.contents;
            var item;
            var con_sd = '';
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                con_sd += "[" + item.con_area_name + "] ";
            }

            $("#siteDetailId").html(con_sd);
            // if(contents[0].session_id == undefined){
            if (contents.length == 0) {
                console.log('세션정보없음');

                rpc("con-list-all", "forTitle", { 'id': id_no }, function (data) {
                    var title = data.contents;
                    $('#siteDetailId').html("[" + title[0].con_area_name + "] ");
                });
                // $('#mixture-live-div').html('');
                // $('#cons-live-div').html('');
                // $('#surface-live-div').html('');
                // $('.captured-time-text').html('');
                var convertingDiv;
                convertingDiv = "<div class='card' style='width:100%'><div class='card-header'><div class='card-body' style='text-align:center'>시공 전 입니다.</div></div></div>"
                $('#alertDiv').css('display', 'block');
                $('#videoList').css('display', 'none');
                $('#alertDiv').html(convertingDiv);

            } else {
                var session_id = contents[0].session_id;
                console.log(contents[0].session_id);
                console.log('"con-list-all", "checkStatus" = ', session_id);
                rpc("con-list-all", "checkStatus", { 'session_id': session_id }, function (data) {
                    if (data.result != "ok") {
                        alert("통신에 문제가 있습니다.");
                        return;
                    }

                    var contents = data.contents;
                    var session_id = contents[0].id;
                    //비디오 입히기
                    if (contents[0].status != 'live') {
                        console.log('라이브가아니면들어오는곳');
                        if (contents[0].status == 'convert_ready') {
                            setTimeout(checkLiveStatus, 300);
                            var readyDiv;
                            readyDiv = "<div class='card' style='width:100%'><div class='card-header'><div class='card-body' style='text-align:center'>시공영상을 변환대기 중 입니다.</div></div></div>"
                            $('#alertDiv').css('display', 'block');
                            $('#videoList').css('display', 'none');
                            $('#alertDiv').html(readyDiv);
                            // alert('변환대기중입니다.');
                        } else if (contents[0].status == 'converting') {
                            setTimeout(checkLiveStatus, 300);
                            var convertingDiv;
                            convertingDiv = "<div class='card' style='width:100%'><div class='card-header'><div class='card-body' style='text-align:center'>시공영상 변환 중 입니다.</div></div></div>"
                            $('#alertDiv').css('display', 'block');
                            $('#videoList').css('display', 'none');
                            $('#alertDiv').html(convertingDiv);
                        } else if (contents[0].status == 'converted') {
                            console.log('converted');
                            // setTimeout(checkLiveStatus, 300);
                            $('#alertDiv').css('display', 'none');
                            $('#videoList').css('display', 'flex');

                            checkLiveStatus();
                        } else if (contents[0].status == 'live') {
                            console.log('live');
                            setTimeout(checkLatestURL, 400);
                        }
                        getVideoFile(session_id);

                    } else {
                        console.log('라이브들어오는곳');
                        var sendData = {
                            id: id_no,
                            name: session_id
                        };
                        rpc("con-list-all", "selectEquipmentBtn", sendData, function (data) {
                            var contents = data.contents;
                            console.log('라이브일때 현장정보란 채우기');
                            var regdate;
                            var regtime;
                            var nowCrossroad;
                            var extension;
                            var ambient_temp;
                            var equipLocationLat;
                            var equipLocationLong;

                            regdate = contents[0].regdate;
                            console.log(regdate);
                            if (regdate == null) {
                                $('#date-td').html('-');
                            } else {
                                realRegdate = regdate.substring(0, 10);
                                $('#date-td').html(realRegdate);
                            }
                            regtime = regdate.substring(11, 16);
                            console.log(regtime);
                            $('#time-td').html(regtime);
                            console.log(regdate);
                            $('.captured-time-text').text(regdate);

                        });
                        // return;
                        checkLatestURL();
                    }
                });

            }

        });




    });
    function getVideoFile(session_id) {
        console.log($('.dropdown-item'));
        var sendData = {
            'id': id,
            'session_id': session_id
        }
        rpc("con-list-all", "getVideoFile", sendData, function (data) {

            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;

            console.log('겟비디오파일나오', id);
            console.log('겟비디오파일나오', session_id);
            console.log('겟비디오파일나오나', contents.length);
            var filenameV = '';
            var filenameM = '';
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                console.log(item.filename);
                filename = item.filename;
                var filenameFirstC = filename.substr(0, 1);
                // console.log(filenameFirstC);
                if (filenameFirstC == 'm') {
                    filenameM = item.filename;
                } else if (filenameFirstC == 'v') {
                    filenameV = item.filename;
                }

            }
            console.log(contents.length);
            if (contents.length == 0) {
                var sendData = {
                    'session_id': session_id
                }
                 rpc("con-list-all", "checkStatus", sendData, function (data) {
                    console.log(data);
                    if(data.contents[0].status == 'convert_ready'){
                        var mixtureNoneDiv = "<div class='card' style='width:100%'><div class='card-header'><div class='card-body' style='text-align:center'>시공 영상 변환 대기 중입니다.</div></div></div>";
                        $('#alertDiv').css('display', 'block');
                        $('#videoList').css('display', 'none');
                        $('#alertDiv').html(mixtureNoneDiv);
                        console.log(mixtureNoneDiv);
                    }else if(data.contents[0].status == 'converting'){
                        var mixtureNoneDiv = "<div class='card' style='width:100%'><div class='card-header'><div class='card-body' style='text-align:center'>시공 영상 변환 중입니다.</div></div></div>";
                        $('#alertDiv').css('display', 'block');
                        $('#videoList').css('display', 'none');
                        $('#alertDiv').html(mixtureNoneDiv);
                        console.log(mixtureNoneDiv);
                    }
                 });

                var mixtureNoneDiv = "<div class='card' style='width:100%'><div class='card-header'><div class='card-body' style='text-align:center'>등록되어있는 영상이 없습니다.</div></div></div>";
                $('#alertDiv').css('display', 'block');
                $('#videoList').css('display', 'none');
                $('#alertDiv').html(mixtureNoneDiv);
                console.log(mixtureNoneDiv);
            } else {
                var soucePathV = "<source src='video/" + filenameV + "' type='video/mp4'>";
                var soucePathM = "<source src='video/" + filenameM + "' type='video/mp4'>";
                console.log(soucePathV);
                console.log(soucePathM);
                $('#mixture-live-img').html(soucePathM);
                $('#surface-live-img').html(soucePathM);
                $('#cons-live-img').html(soucePathV);
            }
        });


    }
    function taskOrder(id) {
        rpc("con-list-all", "taskOrder", { 'id': id }, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var task_order = '';
            var task_order_btns = '';
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                task_order += "<tr id='summary" + (inx + 1) + "'>";
                task_order += "<td>" + item.layer_name + "</td>";
                task_order += "<td id ='" + item.layer_name + "'></td>";
                task_order += "</tr>";
                task_order_btns += " <label class='btn btn-primary' onclick='guidelineRadioGroup(this);'>";
                task_order_btns += "<input type='radio' name='options' id='option" + (inx + 1) + "'";
                task_order_btns += " autocomplete='off' value='" + item.layer_name + "' >" + item.layer_name + "";
                task_order_btns += "</label>";
            }
            task_order += "<tr><th scope='row'>총진행율</th>";
            task_order += "<th colspan='2' style='text-align:right;' id='total_summary'></th>";
            task_order += "</tr>";
            $("#taskOrder").html(task_order);
            $("#guideLineBtns").html(task_order_btns);


        });
    }


</script>



