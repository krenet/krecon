<style>
    /* #document-preview-div {
        width: 100%;
        height: 500px;
    } */

    #document-preview-img-div {
        /* width: 100%;
        height: 500px; */
        text-align: center;
    }

    .button-td {
        width: 70px;
    }

    #td-fs-num, #td-fs-title, #td-fs-content {
        vertical-align: middle;
    }

    .td-fileName {
        color: #8a909d !important;
    }

</style>

<div class="row">
    <table class="table table-bordered site-docu-all table-responsive">
        <thead>
            <tr class="bg-light text-center">
                <th class="preConst-list-num">번호</th>
                <th class="preConst-list-title">제목</th>
                <th class="preConst-list-content">요약</th>
                <th class="preConst-list-fileName">첨부파일명</th>
                <th class="preConst-list-preview">미리보기</th>
                <th class="preConst-list-download">다운로드</th>
            </tr>
        </thead>
        <tbody id="krecon_document" style="text-align:center;">
            <tr class="text-center">
                <td id="td-fs-num" rowspan="4">1</td>
                <td id="td-fs-title" rowspan="4">현장조사 1차</td>
                <td id="td-fs-content" rowspan="4">2021년 4월 31일 현장 답사 진행 현황: 도로 현상태 파악, 주변환경 파악</td>
                <td id="td-fileName">도로현황1.png</td>
                <!-- <td><button class='btn btn-primary btn-sm document-preview-btn' data-file='/pages/uploads/doc_owfYrR2oaW.pdf' onclick='clickPreview(this);'>미리보기</td> -->
                <td><button class='btn btn-primary btn-sm document-preview-btn' onclick='clickPreview("도로현황1.png");'>미리보기</td>
                <td><a class='btn btn-success btn-sm document-download-btn' href="/pages/uploads/도로현황1.png" download>다운로드</td>
            </tr>
            <tr class="text-center">
                <td class="td-fileName">도로현황2.png</td>
                <td><button class='btn btn-primary btn-sm document-preview-btn' onclick='clickPreview("도로현황2.png");'>미리보기</td>
                <td><a class='btn btn-success btn-sm document-download-btn' href="/pages/uploads/도로현황2.png" download>다운로드</td>
            </tr>
            <tr class="text-center">
                <td class="td-fileName">도로현황3.png</td>
                <td><button class='btn btn-primary btn-sm document-preview-btn' onclick='clickPreview("도로현황3.png");'>미리보기</td>
                <td><a class='btn btn-success btn-sm document-download-btn' href="/pages/uploads/도로현황3.png" download>다운로드</td>
            </tr>
            <tr class="text-center">
                <td class="td-fileName">주변환경1.png</td>
                <td><button class='btn btn-primary btn-sm document-preview-btn' onclick='clickPreview("주변환경1.jpg");'>미리보기</td>
                <td><a class='btn btn-success btn-sm document-download-btn' href="/pages/uploads/주변환경1.jpg" download>다운로드</td>
                <!-- <td><a class='btn btn-success btn-sm document-download-btn' data-file='/pages/uploads/doc_owfYrR2oaW.pdf' onclick='clickDownload("주변환경1.jpg");'>다운로드</td> -->
            </tr>
        </tbody>
    </table>


</div>
<!-- <div id="document-preview-img-div">
</div>
<div id="document-preview-div">
</div> -->
