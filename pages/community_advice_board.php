<style>
    .document-preview-frame {
        width: 100%;
        height: 800px;
    }

    .button-td {
        width: 70px;
    }
</style>


<script>
    //api_server 테스트

    $(document).ready(function () {
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        var id = $.urlParam('id');
        console.log('자세히 아이디 : ', id);
        id=3204;
    
        rpc("board", "contents", {'id':id}, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }

            var contents = data.contents;
            var item;
            var offs = "";
            var board_id="";
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                board_id=item.id;
                offs += "<tr id='off_" + inx + "'style='text-align:center;'>";
                offs += "<td><input type='hidden'value=" + item.id + ">" + (inx + 1) + "</td>";
                offs += "<td>" + (item.classification==null?'-':item.classification)+ "</td>";
                offs += "<td>" +(item.title==null?'-':item.title) + "</td>";
                offs += "<td>" + (item.date==null?'-':item.date) + "</td>";
                offs += "<td><button class='btn btn-primary btn-sm' onclick='documentPreviewBtn(this)'data-file='/documents/" + item.file_path + ".pdf'>미리보기</td>";
                offs += "<td><button class='btn btn-success btn-sm' onclick='documentDownloadBtn(this)' data-file='/documents/" + item.file_path + ".pdf'>다운로드</button></td></tr>";
            }
            $("#board-contents").html(offs);
            // console.log(contents.length);
            if (contents.length == 0) {
                var none = '';
                none += '<tr id=' + board_id + '><td colspan=6 style=text-align:center>등록된 게시물이 없습니다. </td></tr>';
                $("#board-contents").html(none);
            }
        });

    });


    function documentPreviewBtn(obj) {
        var file = $(obj)[0].dataset.file;
        console.log(file);
        $("#document-preview-div-advice-board").html('<iframe class="document-preview-frame" src="pdf_viewer/web/viewer.html?file=' + file + '" ></iframe>');
    }

    function documentDownloadBtn(obj) {
        var file = $(obj)[0].dataset.file;
        console.log(file);
        $(obj).attr({ target: '_blank', href: file });
        $('#download-frame').attr("src", file);
    }
</script>


<div class="card card-default" data-scroll-height="675">
    <div class="card-header">
        <h2>자문 내용</h2>
    </div>
    <div class="card-body">
        <table class="table table-bordered table-responsive">
            <thead>
                <tr class="bg-light text-center">
                    <th>번호</th>
                    <th>분류</th>
                    <th>제목</th>
                    <th>날짜</th>
                    <th>미리보기</th>
                    <th>다운로드</th>
                </tr>
            </thead>
            <tbody id="board-contents">
                <!-- <tr class="text-center">
                        <td>1</td>
                        <td>회의록</td>
                        <td>1차 온라인 회의</td>
                        <td>2019-07-22</td>
                        <td class="button-td"><button class="btn btn-primary btn-sm document-preview-btn"
                                data-file="/documents/시공계획서.pdf">미리보기</button></td>
                        <td class="button-td"><a class="btn btn-success btn-sm document-download-btn"
                                data-file="/documents/시공계획서.pdf">다운로드</a></td>
                    </tr> -->
            </tbody>
        </table>
    </div>
</div>
<div id="document-preview-div-advice-board">
</div>
