<style>
    .site-detail-document-preview-frame {
        width: 100%;
        height: 800px;
    }

    .button-td {
        width: 70px;
    }
</style>
<script>
    $(document).ready(function () {
        $(".site-detail-document-preview-btn").click(function () {
            console.log("들어왔어");
            var file = $(this).data("file");
            $("#site-detail-document-preview-div").html('<iframe class="site-detail-document-preview-frame" src="pdf_viewer/web/viewer.html?file=' + file + '" ></iframe>');
        });

        $(".document-download-btn").click(function () {
            var file = $(this).data("file");
            $(this).attr({ target: '_blank', href: file });
            $('#download-frame').attr("src", file);
        });
    });
</script>
<div class="card card-default" data-scroll-height="675">
    <!-- <div class="card-header">
        <h2>준공서류 리스트</h2>
    </div> -->
    <div class="card-body slim-scroll">
        <table class="table table-bordered table-responsive">
            <tbody>
                <tr class="bg-light text-center">
                    <th>번호</th>
                    <th>공사명</th>
                    <th>분류</th>
                    <th>미리보기</th>
                    <th>다운로드</th>
                </tr>

                <tr class="text-center">
                    <td>1</td>
                    <td>성산대교 포장 보수 공사</td>
                    <td>준공검사원</td>
                    <td class="button-td"><a class="btn btn-primary btn-sm site-detail-document-preview-btn"data-file="/documents/준공검사원.pdf">미리보기</a></td>
                    <td class="button-td"><a class="btn btn-success btn-sm document-download-btn"
                            data-file="/documents/준공검사원.pdf">다운로드</a></td>
                </tr>

                <tr class="text-center">
                    <td>2</td>
                    <td>성산대교 포장 보수 공사</td>
                    <td>준공계</td>
                    <td class="button-td"><a class="btn btn-primary btn-sm site-detail-document-preview-btn"
                            data-file="/documents/광주광역시준공계.pdf">미리보기</a></td>
                    <td class="button-td"><a class="btn btn-success btn-sm document-download-btn"
                            data-file="/documents/광주광역시준공계.pdf">다운로드</a></td>
                </tr>
                <tr class="text-center">
                    <td>3</td>
                    <td>성산대교 포장 보수 공사</td>
                    <td>준공검사조서</td>
                    <td class="button-td"><a class="btn btn-primary btn-sm site-detail-document-preview-btn"
                            data-file="/documents/준공검사조서2009도시대학.pdf">미리보기</a></td>
                    <td class="button-td"><a class="btn btn-success btn-sm document-download-btn"
                            data-file="/documents/준공검사조서2009도시대학.pdf">다운로드</a></td>
                </tr>
                <tr class="text-center">
                    <td>4</td>
                    <td>성산대교 포장 보수 공사</td>
                    <td>사진대지</td>
                    <td class="button-td"><a class="btn btn-primary btn-sm site-detail-document-preview-btn"
                            data-file="/documents/괴산댐.pdf">미리보기</a></td>
                    <td class="button-td"><a class="btn btn-success btn-sm document-download-btn"
                            data-file="/documents/괴산댐.pdf">다운로드</a></td>
                </tr>
                <tr class="text-center">
                    <td>5</td>
                    <td>성산대교 포장 보수 공사</td>
                    <td>준공도면</td>
                    <td class="button-td"><a class="btn btn-primary btn-sm site-detail-document-preview-btn"
                            data-file="/documents/인헌고준공도면.pdf">미리보기</a></td>
                    <td class="button-td"><a class="btn btn-success btn-sm document-download-btn"
                            data-file="/documents/인헌고준공도면.pdf">다운로드</a></td>
                </tr>

            </tbody>
        </table>
        <div class="col-md-6 col-sm-6" style="margin-bottom:10px;display: inline-block;text-align: right;">
        <button type="button" class="mb-1 btn btn-primary" data-toggle="modal" data-target="#siteDetailCompleModal">문서추가</button>
    </div>
    </div>
</div>
<div id="site-detail-document-preview-div">
</div>


<!-- Form Modal -->
<div class="modal fade" id="siteDetailCompleModal" tabindex="-1" role="dialog" aria-labelledby="siteDetailCompleModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="siteDetailCompleument">문서 추가</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="siteDetailCompleSelect">분류 선택</label>
                        <select class="form-control" id="siteDetailCompleSelect" placeholder="분류를 선택해 주세요">
                            <option>선택</option>
                            <option>사진대지</option>
                            <option>준공검사원</option>
                            <option>준공계</option>
                            <option>준공검사조서</option>
                            <option>준공도면</option>
                        </select>
                    </div>
                    <!-- <div class="form-group">
                        <label for="siteDetailCompleTitle">문서 제목</label>
                        <input type="email" class="form-control" id="siteDetailCompleTitle" placeholder="제목을 입력해주세요">
                    </div> -->
                    <div class="form-group">
                        <label for="siteDetailCompleSelectFile">파일 추가</label>
                        <input type="file" class="form-control-file" id="siteDetailCompleSelectFile">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">문서 추가</button>
            </div>
        </div>
    </div>
</div>