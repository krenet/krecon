<style>
    #guideline_viewer{
        width: 100%;
        height: 400px;
    }
</style>
<script>
        function guidelineRadioGroup(obj){
            console.log('과업지시서 들어오는지 확인');
            console.log($(obj)[0].innerText);
            var sval = $(obj)[0].innerText;
            if(sval == "보조기층"){
                document.getElementById('guideline_viewer').contentWindow.PDFViewerApplication.page = 19;
            }else if(sval == "기층"){
                document.getElementById('guideline_viewer').contentWindow.PDFViewerApplication.page = 26;
            }else if(sval == "프라임코트"){
                document.getElementById('guideline_viewer').contentWindow.PDFViewerApplication.page = 34;
            }else if(sval == "텍코트"){
                document.getElementById('guideline_viewer').contentWindow.PDFViewerApplication.page = 48;
            }else if(sval == "표층"){
                document.getElementById('guideline_viewer').contentWindow.PDFViewerApplication.page = 54;
            }
        }
</script>

<div class="row" style="margin-bottom:10px;">
    <div class="btn-group btn-group-md btn-group-toggle col-md-6 col-sm-6" data-toggle="buttons" id="guideLineBtns">
        
    </div>
    <!-- <div class="col-md-6 col-sm-6" style="margin-bottom:10px;display: inline-block;text-align: right;">
    <button class="btn btn-primary add-btn add-issue" data-toggle="modal" data-target="#addGuideModal">추가</button>
</div> -->
</div>

<iframe id="guideline_viewer" src="pdf_viewer/web/viewer.html?page=31&file=/documents/시방서.pdf"> </iframe>

<!-- Form Modal -->
<div class="modal fade" id="addGuideModal" tabindex="-1" role="dialog" aria-labelledby="addGuideModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addGuideModalTitle"> 추가 </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group"><label for="options1"><input type="radio" name="options" id="options1">보조기층</label></div>
                    <div class="form-group"><label for="options2"><input type="radio" name="options" id="options2">기층</label></div>
                    <div class="form-group"><label for="options3"><input type="radio" name="options" id="options3">프라임코트</label></div>
                    <div class="form-group"><label for="options4"><input type="radio" name="options" id="options4">택코트</label></div>
                    <div class="form-group"><label for="options5"><input type="radio" name="options" id="options5">중간층</label></div>
                    <div class="form-group"><label for="options6"><input type="radio" name="options" id="options6">표층</label></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
