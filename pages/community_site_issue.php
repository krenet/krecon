<style>
    .table-dark-head th{
        color: white;
        text-align: center;
    }

    #cons-history-table-body td{
        text-align: center;
    }

    #cons-history-table-body tr:hover, .cons-history-table-body-hovered {
        background: #7DBCFF;
        color: white;
        cursor: pointer;
    }

    .noselect {
        -webkit-touch-callout: none; /* iOS Safari */
        -webkit-user-select: none; /* Safari */
        -khtml-user-select: none; /* Konqueror HTML */
        -moz-user-select: none; /* Firefox */
            -ms-user-select: none; /* Internet Explorer/Edge */
                user-select: none; /* Non-prefixed version, currently
                                    supported by Chrome and Opera */
    }

</style>
<!-- <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=fb33fb2a39c5c870948bbe64f31deb8e"></script> -->
<script>          
//<![CDATA[     
    //공사중 마커 이미지
    var defaultImageSrc = "http://t1.daumcdn.net/localimg/localimages/07/mapjsapi/default_marker.png";
    var liveImageSrc = "http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/markerStar.png";
    var selectedImageSrc = "img/pin-selected.gif";
    // 마커 이미지의 이미지 크기 입니다
    var defaultImageSize = new daum.maps.Size(35,36);
    var liveImageSize = new daum.maps.Size(24, 35);     
    var selectedImageSize = new daum.maps.Size(35, 35);    
    // 마커 이미지를 생성합니다    
    var defaultMarkerImage = new daum.maps.MarkerImage(defaultImageSrc,defaultImageSize);
    var liveMarkerImage = new daum.maps.MarkerImage(liveImageSrc, liveImageSize); 
    var selectedMarkerImage = new daum.maps.MarkerImage(selectedImageSrc, selectedImageSize); 

    //지도 객체를 담을 변수
    var map;

    //표시 데이터
    var cons_id_list = new Array();
    var cons_list = new Array();                                             
                

    $(document).ready(function(){
        rpc("issue", "list", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            console.log(data);
            var contents = data.contents;
            var item;
            for(inx = 0;inx<contents.length;++inx){
                item = contents[inx];
                cons_list.push({id:item.con_code,region:item.con_field_name,name:item.con_area_name,
                latlng:new daum.maps.LatLng(item.con_lat, item.con_long),issue:item.title,islive:item.is_live=='Y', issue_id : item.id
                });
            }
            renderMap();
        }); 

        rpc("advice", "selectConRemoteByCompanyId", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var title ;
            title = "<option>선택</option>"
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
            title += "<option>"+item.con_area_name+"</option>"
            }
            // console.log(contents[0].con_area_name);
            $('#addOffInputName').html(title);
            
        });
    });
                //초기화
                function renderMap(){
                    var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
                    mapOption = { 
                        center: new daum.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
                        level: 3 // 지도의 확대 레벨
                    };

                    

                    // 지도를 표시할 div와  지도 옵션으로  지도를 생성합니다
                    map = new daum.maps.Map(mapContainer, mapOption); 

                    //지도 마커 표시 및 지도 범위 설정
                    var bounds = new daum.maps.LatLngBounds(); 
                    var liveCount = 0;
                    var consHistoryTableHTML = "";
                    var item;
                    var marker;
                    
                    for(inx = 0;inx<cons_list.length;++inx){
                        item = cons_list[inx];
                        marker = new daum.maps.Marker({
                            position: item.latlng,
                            title:item.name,
                            map:map
                        });                                      
                        cons_list[inx].marker = marker;
                        cons_id_list[item.id] = item;                        
                        marker.id = item.id;    
                        addMarkerActions(marker,item.id);                     
                        
                        bounds.extend(item.latlng);
                        if(item.islive){
                            marker.setImage(liveMarkerImage);
                            ++liveCount;
                        }else{
                            marker.setImage(defaultMarkerImage);
                        }
                        
                        consHistoryTableHTML +='<tr class="cons-history-table-row" data-cons-id="' + item.id + '"' +
                                'id="' + item.id + '"style="text-align:center;" value="' + item.issue_id + '" onclick="clickDetailIssueRow(this);">' +
                                '<td scope=\"row\">' + item.region + '</td>' +
                                '<td data-toggle="modal" data-target="#showDetailIssueModal">' + item.name + '</td>' +                             
                                '<td>'+item.issue+'</td>'+
                                '<td> <button type="button" value="' + item.issue_id + '" class="btn btn-success modify-btn btn-sm"' +
                                'data-toggle="modal" onclick="clickEdit(this);" data-target="#editIssueModal">수정</button></td>' +
                            '</tr>';                        
                            
                    }
                    map.setBounds(bounds);                    

                    //시공 이력및 현재 시공 중 현장 텍스트입력
                    $(".total-cons-count").text(cons_list.length);
                    $(".live-cons-count").text(liveCount);

                    //테이블 완성
                    $("#cons-history-table-body").html(consHistoryTableHTML);

                    //테이블에 마우스 오버시 해당 마커 강조
                    $(".cons-history-table-row").mouseover(function(){
                        var s_item = cons_id_list[$(this).data('cons-id')];
                        s_item.marker.setMap(null);
                        s_item.marker.setImage(selectedMarkerImage);
                        s_item.marker.setMap(map);
                    });
                    $(".cons-history-table-row").mouseout(function(){
                        var s_item = cons_id_list[$(this).data('cons-id')];
                        s_item.marker.setMap(null);
                        if(s_item.islive){
                            s_item.marker.setImage(liveMarkerImage);
                        }else{
                            s_item.marker.setImage(defaultMarkerImage);
                        }
                        
                        s_item.marker.setMap(map);
                    });
                    
                    $(".cons-history-table-row").click(function(){
                        var s_item = cons_id_list[$(this).data('cons-id')];
                        moveDetail(s_item.id);
                    });
                    
                }

                //마커에 마우스 이벤트 등록                    
                function addMarkerActions(marker,id){
                        
                        daum.maps.event.addListener(marker, 'mouseover', function() {                                                
                            $(".cons-history-table-row[data-cons-id="+id+"]").addClass("cons-history-table-body-hovered");
                        });
                        
                        daum.maps.event.addListener(marker, 'mouseout', function() {                        
                            $(".cons-history-table-row[data-cons-id="+id+"]").removeClass("cons-history-table-body-hovered");
                        });

                        daum.maps.event.addListener(marker, 'click', function() {                        
                            moveDetail(id);
                        });
                }

                //상세 페이지로 이동
                function moveDetail(id){
/*
                    location.href=".?cat=site&page=site_detail&id="+id;
                    */
                }
//        ]]>
 // //초록수정 수정모달로 들어가기
 function clickEdit(obj) {
        var tr = $(obj).parent().parent();
        var dbID = $(obj).val();
        var td = tr.children();
        var str = ""
        var tdArr = new Array();
        td.each(function (i) {
            tdArr.push(td.eq(i).text());
        });

        $("#editIssueModal #area").val(tdArr[0]);
        $("#editIssueModal #field").val(tdArr[1]);
        $("#editIssueModal #issue").val(tdArr[2]);
        $('#editIssueModal #dbID').val(dbID);

    }

    //파란수정 수정이 실제로 되는 버튼
    function checkEditValue(obj) {
        var result = confirm("현장이슈를 수정하시겠습니까?");
        if (result) {
            alert('수정되었습니다.');
        } else {
            return;
        }

        var formValue = $(obj).parent().parent();
        // console.log(formValue);
        var formData = $("#editModalForm").serializeObject();
        // console.log(formData);

        rpc("issue", "edit", formData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            //수정하고나서의 프로세스 
            location.reload();
        });
    }

    jQuery.fn.serializeObject = function () {
        var obj = null;
        try {
            if (this[0].tagName && this[0].tagName.toUpperCase() == "FORM") {
                var arr = this.serializeArray();
                // console.log(arr);
                if (arr) {
                    obj = {};
                    jQuery.each(arr, function () {
                        obj[this.name] = this.value;
                    });
                }
            }
        } catch (e) {
            alert(e.message);
        } finally { }
        return obj;
    }

    function loadAddIssue() {
        rpc("issue", "loadAddIssue", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var issue = "";
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                issue += "<option>" + item.con_area_name + "</option>";
            }
            $("#addOffInputName").html(issue);
        });
    }

    function addIssueOnModal() {
        var sendData = {
            con_area_name: $('#addOffInputName').val(),
            title: $('#addIssueName').val(),
            contents: $('#addIssueContents').val(),
        };

        rpc("issue", "addIssueOnModal", sendData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
        });
        location.reload();
    }
    function clickDetailIssueRow(obj){
            // console.log($(obj)[0].attributes.value.value);
            var id = $(obj)[0].attributes.value.value;
            rpc("issue", "showDetailIssueModal", {'id': id}, function (data) {
                if (data.result != "ok") {
                    alert("통신에 문제가 있습니다.");
                    return;
                }
                var contents = data.contents;
                // console.log(contents[0].title);
                // console.log(contents[0].contents);

                var titleArea = contents[0].title;
                var innerContents = contents[0].contents;
            $('#showDetailIssueModal #titleArea').text(titleArea);
            $('#showDetailIssueModal #innerContents').text(innerContents);
            });
            // $('#showDetailIssueModal #titleArea').html(titleArea);
            // $('#showDetailIssueModal #titleArea').val(titleArea);

        }
</script>   


<div class="row">
	<div class="col-xl-6 col-md-12">
        <div class="card card-default" data-scroll-height="675">
            <div class="card-header">
                <h2>이슈 발생 지도</h2>
            </div>
            <div class="card-body">
                <div id="map" style="width:100%;height:400px;"></div>             
            </div>
            <div class="card-footer d-flex flex-wrap bg-white p-0">
                <div class="col-6 px-0">
                    <div class="text-center p-4">
                        <h4 class="total-cons-count"></h4>
                        <p class="mt-2">총 발생 이력</p>
                    </div>
                </div>
                <div class="col-6 px-0">
                    <div class="text-center p-4 border-left">
                        <h4 class="live-cons-count"></h4>
                        <p class="mt-2">현재 시공 중</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div class="col-xl-6 col-md-12">
        <div class="card card-default" data-scroll-height="675">
            <div class="card-header">
                <h2>이슈 발생 리스트</h2>
            </div>
            <div class="card-body" style="overflow:auto;    height: 500px;">
                <table class="table table-bordered noselect table-responsive">
                    <thead class="table-dark table-dark-head">
                        <tr>
                            <th scope="col">지역명</th>
                            <th scope="col">현장명</th>                                                        
                            <th scope="col">이슈</th>
                            <th scope="col">수정</th>
                        </tr>
                    </thead>
                    <tbody id="cons-history-table-body">
                        
                    </tbody>
                </table>
            </div>
                <div class="card-footer d-flex flex-wrap bg-white p-0">
                    <div class="col-6 px-0">
                        <div class="text-center p-4">
                            <h4 class="total-cons-count"></h4>
                            <p class="mt-2">총 발생 이력</p>
                        </div>
                    </div>
                    <div class="col-6 px-0">
                        <div class="text-center p-4 border-left">
                            <h4 class="live-cons-count"></h4>
                            <p class="mt-2">현재 시공 중</p>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="col-md-6"></div>
    <div class="col-md-6 card-text" style="text-align:right; padding: 20px;">
        <button class="btn btn-primary add-btn add-issue" onclick="loadAddIssue()" data-toggle="modal"
            data-target="#makeIssueModal">추가</button>
    </div>
</div>
<!-- Form Modal -->
<div class="modal fade" id="makeIssueModal" tabindex="-1" role="dialog" aria-labelledby="makeIssueModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="makeIssueModalTitle">현장 이슈 추가</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group addOffInputName">
                        <label for="addOffInputName">현장명</label>
                        <select class="form-control" id="addOffInputName">
                            <option selected>선택</option>
                        
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="addIssueName">이슈</label>
                        <input type="text" class="form-control" id="addIssueName" placeholder="이슈를 입력해주세요.">
                    </div>
                    <div class="form-group">
                        <label for="addIssueContents">상세내용</label>
                        <input type="text" class="form-control" id="addIssueContents" placeholder="상세내용을 입력해주세요.">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addIssueOnModal()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- 수정모달 -->
<div class="modal fade" id="editIssueModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="editModalLabel">수정</h4>
                <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×
                    </span><span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modalContent">
                    <form role="form" name="editModalForm" id="editModalForm" action="adviser-edit.php" method="post">
                        <div class="form-group">
                            <label for="지역명">지역명</label>
                            <input class="form-control" name="지역명" id="area" value="" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="현장명">현장명</label>
                            <input class="form-control" name="현장명" id="field" value="" readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="이슈">이슈</label>
                            <input class="form-control" name="이슈" id="issue" value="">
                        </div>
                        <input type="hidden" name="dbID" id="dbID" value="">
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick='checkEditValue(this);'>Save changes</button>
            </div>
        </div>
    </div>
    
</div>

<div class="modal fade" id="showDetailIssueModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="titleArea"> </h4>
                <button type="button" class="close" data-dismiss="modal"> <span aria-hidden="true" class="">×
                    </span><span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="modalContent">
                        <div class="form-group">
                            <div id="innerContents" value=""></div>
                        </div>
                        <input type="hidden" name="dbID" id="dbID" value="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary" onclick='checkEditValue(this);'>Save changes</button> -->
            </div>
        </div>
    </div>
</div>