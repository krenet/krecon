<style>
    .h_num {
        width: 5%;
    }

    .h_title {
        width: 25%;
    }

    .h_content {
        width: 35%;
    }
    
    .h_fileName {
        width: 15%;
    }

    .h_edit {
        width: 10%;
    }

    .h_delete {
        width: 10%;
    }
</style>

<table class="table table-bordered site-docu-all table-responsive">
    <thead>
        <tr class="bg-light text-center">
            <th class="h_num">번호</th>
            <th class="h_title">제목</th>
            <th class="h_content">요약</th>
            <th class="h_fileName">첨부파일명</th>
            <th class="h_edit">수정</th>
            <th class="h_delete">삭제</th>
        </tr>
    </thead>
    <tbody id="krecon_document" style="text-align:center;">
        <tr class="text-center">
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>  
<div class="card-text" style="text-align:right;">
    <button class="btn btn-primary" data-toggle="modal" data-target="#addHModal">추가</button>
</div>

<!--추가모달 -->
<div class="modal fade" id="addHModal" tabindex="-1" role="dialog" aria-labelledby="addHistoryModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content historyModal">
            <div class="modal-header">
                <h5 class="modal-title" id="addHistoryModalTitle">이력 추가</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="addHistoryModal" id="addHistoryModal">
                    <div class="form-group ui-widget">
                        <label for="addHistoryTitle">제목</label>
                        <input type="text" class="form-control tags" id="addHistoryTitle" placeholder="제목을 입력해주세요.">
                    </div>

                    <div class="form-group">
                        <label for="addHistoryContent">요약</label>
                        <textarea class="form-control tags" id="addHistoryContent" placeholder="요약 내용을 입력해주세요." cols="30" rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="addHistoryFile">파일첨부</label>
                        <input multiple="multiple" type="file" class="form-control-file" id="addHistoryFile">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addHistory()">Save</button>
            </div>
        </div>
    </div>
</div>

<script>
    function addHistory() {
        var title = $('#addHistoryTitle').val();
        var content = $('#addHistoryContent').val();

        if(title==''){
            alert("제목을 입력해주세요.");
        }else if(content==''){
            alert('요약 내용을 입력해주세요.');
        }else if(title!='' && content!=''){
            var result = confirm("이력 정보를 추가하시겠습니까?");
            if(result) {
                alert("추가되었습니다.");
            }else{
                return;
            }
        }     
    }
</script>