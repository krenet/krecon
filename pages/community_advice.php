<!-- The core Firebase JS SDK is always required and must be listed first
<script src="https://www.gstatic.com/firebasejs/6.0.2/firebase-app.js"></script> -->

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#config-web-app -->

<!-- <script>
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyBECAlmgRRbB3-kPkpon4Id6C-AiEPBq4w",
        authDomain: "krecon-f06b1.firebaseapp.com",
        databaseURL: "https://krecon-f06b1.firebaseio.com",
        projectId: "krecon-f06b1",
        storageBucket: "krecon-f06b1.appspot.com",
        messagingSenderId: "1019913310430",
        appId: "1:1019913310430:web:866a3d65fe4d29d0"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
</script> -->

<style>
    .addAdvTitle {
        cursor: pointer;
    }

    .btn-close {
        background-color: #f5f6fa;
    }

    .sendMail {
        cursor: pointer;
    }

    .chat-ready-btn {
        background-color: #fec400;
        border: #fec400;
    }

    .button-td {
        width: 70px;
    }

    #addAdviceDate {
        margin-bottom: 1rem !important;
    }
    
    .select2-container {
        width: 100% !important;
    }

    /* .clickedDate{
		background-color:#4c84ff;
		color:white;
	} */
</style>

<div class="card card-default">
    <div class="card-header card-header-border-bottom">
        <h2>원격 자문 리스트</h2>
    </div>
    <div class="card-body">
        <table class="table table-bordered table-responsive" style="text-align:center;" id="community_advice_table">
            <thead>
                <tr class="bg-light text-center">
                    <th class="button-td">번호</th>
                    <th>자문제목</th>
                    <th>채팅 예정 일시</th>
                    <th>보고서 마감 일시</th>
                    <th>채팅</th>
                    <th class="button-td">게시판</th>
                    <th>세션 상태</th>
                    <th>삭제</th>
                </tr>
            </thead>
            <tbody id="remote-advice-list-contents">
                <!-- <tr class="text-center">
							<td class="button-td">3</td>
							<td class="addAdvTitle" data-toggle="modal" data-target="#seeAdvModal">구스픽스 아스팔트 물성 개선 기술자문</td>
							<td>종료</td>
							<td>2019-05-25 22:00</td>
							<td>2019-05-25 23:00</td>
							<td><button class="btn btn-sm btn-close" data-id="0001">채팅종료</button>
							</td>
							<td class="button-td"><button class="btn btn-success btn-sm board-btn " data-id="0001">자세히</button>
							</td>
						</tr> -->
            </tbody>
        </table>
    </div>
    <div class="card-text" style="text-align:right; padding: 20px; padding-right: 60px;">
        <button class="btn btn-primary add-btn add-issue" data-toggle="modal" data-target="#addAdvModal">추가</button>
    </div>
</div>
<!-- Form Modal -->
<div class="modal fade" id="addAdvModal" tabindex="-1" role="dialog" aria-labelledby="addAdvModalTitle"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addAdvModalTitle">자문 추가</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="addAdvProject">
                    <div class="form-group addAdvtFieldName">
                        <label for="addAdvtFieldName">현장명</label>
                        <select class="form-control" id="addAdvtFieldName">
                            <option>선택</option>
                            <!-- <option>성암로(중소기업 DMC 앞) 포장 보수 공사</option>
                            <option>성산대교 포장 보수 공사</option>
                            <option>양화대교 포장 보수 공사</option>
                            <option>서강대교 포장 보수 공사</option>
                            <option>가양대교 포장 보수 공사</option>
                            <option>고양대교 포장 보수 공사</option> -->
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="addAdvtFieldTitle">자문 제목</label>
                        <input type="text" class="form-control" id="addAdviceTitle" placeholder="제목을 입력해주세요.">

                    </div>

                    <div class="form-group addAdviceDate">
                        <label sytle="display:inline-block" for="addAdviceDate">채팅예정일시</label>
                        <input type="datetime-local" class="form-control" id="addAdviceDate"
                            placeholder="날짜를 입력해주세요. ex)2019-08-19">
                    </div>
                    <div class="form-group addAdvtFieldDeadLine">
                        <label sytle="display:inline-block" for="addAdvtFieldDeadLine">보고서 제출 기한</label>
                        <input type="date" class="form-control" id="addAdvtFieldDeadLine"
                            placeholder="날짜를 입력해주세요. ex)2019-08-19">
                    </div>
                    <div class="form-group addBoardScheduleOnOff">
                        <label for="addBoardScheduleOnOff">온라인/오프라인</label>
                        <select class="form-control" id="addBoardScheduleOnOff">
                            <option>선택</option>
                            <option value="온라인">온라인 </option>
                            <option value="오프라인">오프라인</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="addAdvtAdviserCheckBox">자문 위원 선택</label>
                        <select class="form-control js-example-basic-multiple" multiple="multiple"
                            name="addAdvtAdviserSelectBox[]" id="addAdvtAdviserSelectBox">
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addRemoteAdvice()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- 자문 제목 눌렀을때 -->
<div class="modal fade" id="seeAdvModal" tabindex="-1" role="dialog" aria-labelledby="seeAdvModalTitle"
    name="seeAdvModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-md-6">
                    <h5 class="modal-title" id="seeAdvModalTitle">이전 자문 목록</h5>
                </div>
                <div class="col-md-6" style="text-align:right;"><button class="btn btn-primary add-btn"
                        data-toggle="modal" data-target="#addScheduleAdvModal">추가</button></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <table class="table table-bordered table-responsive" style="text-align:center">
                    <thead>
                        <tr class="bg-light text-center">
                            <th>번호</th>
                            <th>온라인/오프라인</th>
                            <!-- <th>세션 상태</th> -->
                            <!-- <th>예정 시간</th> -->
                            <th>제목</th>
                            <th>서류마감</th>
                            <th>참석 자문</th>
                            <th>기타</th>
                            <th>등록일자</th>
                            <th>메일</th>
                            <th>삭제</th>
                        <tr>
                    </thead>
                    <tbody id="seeModalValue">
                        <!-- <tr>
							<td>2019-07-09</td>
							<td>온라인</td>
							<td>예정</td>
							<td>2019-07-09 PM14</td>
							<td>2019-07-18</td>
							<td>최준성, 이상염</td>
							<td>-</td>
							<td><span class="mdi mdi-email-outline sendMail" data-toggle="modal"
									data-target="#sendMailAdvModal"></span></span></td>
						</tr> -->
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save</button> -->
            </div>
        </div>
    </div>
</div>



<!-- 메일보내기 -->
<div class="modal fade" id="sendMailAdvModal" tabindex="-1" role="dialog" aria-labelledby="sendMailAdvModalTitle"
    name="sendMailAdvModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="sendMailAdvModalTitle">메일보내기</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-responsive" style="text-align:center">
                    <thead>
                        <tr class="bg-light text-center">
                            <th>번호</th>
                            <th>자문위원</th>
                            <th>메일</th>
                            <th>메일쓰기</th>
                        <tr>
                    </thead>
                    <tbody id="mailLink">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- 일정추가 -->
<div class="modal fade" id="addScheduleAdvModal" tabindex="-1" role="dialog" aria-labelledby="addScheduleAdvModalTitle"
    name="addScheduleAdvModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">일정추가</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group addAdvMeetingTitle">
                    <label sytle="display:inline-block" for="addAdvMeetingTitle">제목</label>
                    <input type="text" class="form-control" id="addAdvMeetingTitle" placeholder="제목을 입력해주세요. ">
                </div>
                <div class="form-group addAdvMeetingOnOff">
                    <label for="addAdvMeetingOnOff">온라인/오프라인</label>
                    <select class="form-control" id="addAdvMeetingOnOff">
                        <option>선택</option>
                        <option value="온라인">온라인 </option>
                        <option value="오프라인">오프라인</option>
                    </select>
                </div>
                <div class="form-group addAdvMeetingDeadline">
                    <label sytle="display:inline-block" for="addAdvMeetingDeadline">보고서 제출 기한</label>
                    <input type="date" class="form-control" id="addAdvMeetingDeadline"
                        placeholder="날짜를 입력해주세요. ex)2019-08-19">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="addAdvMeetingAdvice()">Save</button>
            </div>
        </div>
    </div>
</div>

<script>
    Date.prototype.format = function(f) {
        if (!this.valueOf()) return " ";
    
        var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
        var d = this;
        
        return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
            switch ($1) {
                case "yyyy": return d.getFullYear();
                case "yy": return (d.getFullYear() % 1000).zf(2);
                case "MM": return (d.getMonth() + 1).zf(2);
                case "dd": return d.getDate().zf(2);
                case "E": return weekName[d.getDay()];
                case "HH": return d.getHours().zf(2);
                case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
                case "mm": return d.getMinutes().zf(2);
                case "ss": return d.getSeconds().zf(2);
                case "a/p": return d.getHours() < 12 ? "오전" : "오후";
                default: return $1;
            }
        });
    };
    
    String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
    String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
    Number.prototype.zf = function(len){return this.toString().zf(len);};


    $(document).ready(function () {
        rpc("advice", "remote", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item;
            var offs;
            // var now = new Date();
            // var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            // console.log('오늘 : ', now);
            var today = new Date().format("yyyy-MM-dd");

            for (inx = 0; inx < contents.length; ++inx) {
                // var today = new Date();
                item = contents[inx];
                // var specificDate = new Date(item.scheduled_open_time);
                var specificDate = item.scheduled_open_time.split(' ')[0];

                offs += "<tr id='" + item.id + "'style='text-align:center;' value=" + item.id + ">";
                offs += "<td>" + (inx + 1) + "</td>";
                offs += "<td  data-toggle='modal' data-target='#seeAdvModal'style='cursor:pointer' value=" + item.id + " onclick='seeAdvModal(this);'>" + item.title + "</td>";
                offs += "<td>" + (item.scheduled_open_time == null ? '-' : item.scheduled_open_time) + "</td>";
                offs += "<td>" + item.deadline + "</td>"; //보고서 제출 기한
                if (today > specificDate) {
                    offs += "<td><button class='btn btn-sm btn-close'>채팅종료</td>";
                } else if (today == specificDate) {
                    offs += "<td><button class='btn btn-sm' onclick='goChat();'style='background-color:#fec400'>채팅하기</td>";
                } else if (today < specificDate) {
                    offs += "<td><button class='btn btn-sm btn-close'>채팅준비</td>";
                }
                offs += "<td class='button-td'><button class='btn btn-success btn-sm' onclick='remote_advice(" + item.id + ");'>자세히</button></td>";
                offs += "<td>" + (item.progress == null ? '-' : item.progress) + "</td>";
				offs += "<td><button class='btn btn-danger btn-sm remove-btn' onclick='clickDelete(this);'>삭제</button></td></tr>";

                offs += "<input type='hidden' value=" + item.id + ">";
                offs += "</tr>";
            }

            $("#remote-advice-list-contents").html(offs);
            // console.log(contents.length);
            if (contents.length == 0) {
                var none = '';
                none += '<tr><td colspan=8 style=text-align:center>등록된 원격 자문이 없습니다. </td></tr>';
                $("#remote-advice-list-contents").html(none);
            }


        });

        rpc("advice", "selectConRemoteByCompanyId", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var title ;
            title = "<option>선택</option>"
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
            title += "<option>"+item.con_area_name+"</option>"
            }
            // console.log(contents[0].con_area_name);
            $('#addAdvtFieldName').html(title);
            
        });


        //자문추가시 자문위원선택란
        rpc("adviser", "selectAdviserAll", null, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            // console.log(contents);
            var item;
            var adviser2;

            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                adviser2 += " <option data-value='" + item.id + "'>" + item.name + "</option>"
            }
            $('#addAdvtAdviserSelectBox').html(adviser2);

        });
        $('.js-example-basic-multiple').select2();

    });

    var clicked_advice_id = '';


    function addRemoteAdvice() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;

        // var todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate());
        var adviceDate = $('#addAdviceDate').val();
        var progress = '';
        if (today < adviceDate) {
            progress = '미진행';
        } else if (today == adviceDate) {
            progress = '진행중';
        } else if (today > adviceDate) {
            progress = '채팅종료';
        }
        var sendData = {
            con_area_name: $('#addAdvtFieldName').val(),
            title: $('#addAdviceTitle').val(),
            doc_deadline: $('#addAdvtFieldDeadLine').val(),
            date: $('#addAdviceDate').val(),
            progress: progress
        };
        console.log(sendData);

        rpc("advice", "addRemoteAdvice", sendData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            insert_id = data.insert;
            addAdvicetoAdviceBoard(insert_id);
        });


    }

    function addAdvicetoAdviceBoard(insert_id) {
        var sendData = {
            advice_id: insert_id,
            on_offline: $('#addBoardScheduleOnOff').val(),
            title: $('#addAdviceTitle').val(),
            scheduled_open_time: $('#addAdviceDate').val(),
            doc_deadline: $('#addAdvtFieldDeadLine').val()
        };
        rpc("advice", "addAdvicetoAdviceBoard", sendData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            addRemoteAdviserToAdvise(insert_id);
        });
        // location.reload();
    }

    function addRemoteAdviserToAdvise(insert_id) {
        // function checkEditValue(obj) {
        var result = confirm("자문을 추가하시겠습니까?");
        if (result) {
            alert('추가되었습니다.');
        } else {
            return;
        }
        // }
        $("#addAdvtAdviserSelectBox").find(":selected").each(function (i) {
            // console.log($(this).val());
            // console.log($(this)[0].attributes[0].nodeValue);
            var sendData = {
                advice_id: insert_id,
                adviser_id: $(this)[0].attributes[0].nodeValue
            };
            // console.log(sendData);
            // console.log(insert_id);
            rpc("advice", "addRemoteAdviserToAdvise", sendData, function (data) {
                if (data.result != "ok") {
                    alert("통신에 문제가 있습니다.");
                    return;
                }
            });

            location.reload();

        });

    }

    function seeAdvModal(obj) {
        clicked_advice_id = $(obj).parent().attr("id");
        var sendData = {
            id: $(obj).parent().attr("id")
        };
        console.log('sendData',sendData);
        rpc("advice", "remoteBoard", sendData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
            var contents = data.contents;
            var item = '';
            var offs = '';
            var advice_id = '';
            for (inx = 0; inx < contents.length; ++inx) {
                item = contents[inx];
                advice_id = item.advice_id;
                offs += "<tr id='" + item.advice_id + "'style='text-align:center;'>";
                offs += "<td><input type='hidden'value=" + item.id + ">" + (inx + 1) + "</td>";
                offs += "<td>" + item.on_offline + "</td>";
                // offs += "<td>" + item.session_status + "</td>";
                offs += "<td>" + (item.title == null ? '-' : item.title) + "</td>";
                offs += "<td>" + item.doc_deadline + "</td>";
                offs += "<td>" + item.name + "</td>";
                offs += "<td>" + (item.etc == null ? '-' : item.etc) + "</td>";
                offs += "<td>" + item.regdate + "</td>";
                offs += "<td><span class='mdi mdi-email-outline sendMail' data-toggle='modal' data-target='#sendMailAdvModal'></span></span></td>";
				offs += "<td id ="+item.id+"><button class='btn btn-danger btn-sm remove-btn' onclick='clickDeleteBoardInModal(this);'>삭제</button></td></tr>";

                offs += "</tr>";
            }

            $("#seeModalValue").html(offs);
            console.log(contents.length);
            if (contents.length == 0) {
                var none = '';
                none += '<tr id=' + advice_id + '><td colspan=8>이전 자문이 없습니다. </td></tr>';
                $("#seeModalValue").html(none);
            }
            console.log('contents:',contents);
            adviser = contents[0].name;
            adviser = adviser.split(',');

            rpc("advice", "getMailLink", sendData, function (data) {
                if (data.result != "ok") {
                    alert("통신에 문제가 있습니다.");
                    return;
                }
                var contents = data.contents;
                var item = '';
                var mailLink;
                for (inx = 0; inx < contents.length; ++inx) {
                    item = contents[inx];
                    mailLink += "<tr><td>" + (inx + 1) + "</td><td>" + item.name + "</td><td>" + item.mail + "</td>";
                    mailLink += "<td><a href='mailto:" + item.mail + "'><span class='mdi mdi-email-outline'></span></a></td></tr>";
                }
                $('#mailLink').html(mailLink);
            });
        });
    }

    // 	function addScheduleAdvModal(obj){
    // 		console.log('모달안으로들어옴');

    // }
    function addAdvMeetingAdvice() {
        console.log(clicked_advice_id);
        console.log($('#addAdvMeetingTitle').val());
        var doc_deadline = $('#addScheduleAdvModal #addAdvMeetingDeadline').val();
        var sendData = {
            advice_id: clicked_advice_id,
            doc_deadline: doc_deadline,
            on_offline: $('#addAdvMeetingOnOff').val(),
            title: $('#addScheduleAdvModal #addAdvMeetingTitle').val()
        }
console.log(sendData);
        rpc("advice", "addScheduleAdvModal", sendData, function (data) {
            if (data.result != "ok") {
                alert("통신에 문제가 있습니다.");
                return;
            }
        });
        location.reload();
    }




    function remote_advice(id) {
        location.href = "http://localhost/?cat=community&page=community_advice_board&id=" + id;

    }

    function goChat() {
        location.href = "http://localhost/?cat=community&page=community_advice_chat";
    }

    function clickDelete(obj) {
		var result = confirm("자문을 삭제하시겠습니까?");
		if (result) {
			alert('삭제되었습니다.');
		} else {
			return;
		}
		var tr = $(obj).parent().parent();
		id = $(tr[0]).attr('id');
        console.log(id);
        //백업테이블에 공무원 추가 및 공무원테이블에서 해당 공무원 삭제
        backupAdvice(id);
	}

    function backupAdvice(obj){
        console.log('들어온다백업하러');

        rpc("advice", "adviceBackUp", { 'id': id }, function (data) {
			if (data.result != "ok") {
				alert("통신에 문제가 있습니다.");
				return;
            }
            // return;
			//호출문제로 인해 삭제함수를 여기다 넣었음
            console.log('들어간다삭제하러');
            deleteAdvice(id);
		});
    }

	function deleteAdvice(obj) {
            console.log('들어온다삭제하러');
        console.log($(obj));
        console.log($(id));
        // return;
		rpc("advice", "deleteAdvice", { 'id': id }, function (data) {
			if (data.result != "ok") {
				alert("통신에 문제가 있습니다.");
				return;
			}
        });
        location.reload();
	}

    function clickDeleteBoardInModal(obj){
        console.log($(obj))
        btn = $(obj).parent();
        id = $(btn[0]).attr('id');
        console.log(id);
		rpc("advice", "clickDeleteBoardInModalInCommunity", { 'id': id }, function (data) {
            if (data.result != "ok") {
				alert("통신에 문제가 있습니다.");
				return;
			}
            // return;
            deleteAdvice(id);
        });

    }
    function deleteAdvice(id){
        rpc("advice", "deleteAdviceInModal", { 'id': id }, function (data) {
            if (data.result != "ok") {
				alert("통신에 문제가 있습니다.");
				return;
			}
        });
        return;
        location.reload();
    }
</script>